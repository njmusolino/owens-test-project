<?xml version="1.0"?>
<doc>
    <assembly>
        <name>PTO.Charting.DataContracts</name>
    </assembly>
    <members>
        <member name="T:PTO.Charting.DataContracts.Analytics.MetricRequest">
            <summary>
            Data summarization request to perform an E10 metric calculation
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricRequest.Metric">
            <summary>
            The metric to calculate.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricRequest.EntityFilter">
            <summary>
            The types of entities to include in calculation.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricRequest.Partitions">
            <summary>
            The number of time partitions to divide the timespan into.  Must be greater than or equal to 1.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.Analytics.Metric">
            <summary>
            The metrics supported by the summarization consumer.
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.Metric.OperationalUptime">
            <summary>
            Operational uptime (Uptime / Operations time)
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.Metric.MeanUptimeBetweenFailures">
            <summary>
            Mean uptime between failures (Uptime / # of failures in Uptime)
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.Metric.MeanProductiveTimeBetweenFailures">
            <summary>
            Mean productive time between failures (Productive time / # of failures in Productive time)
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.Metric.MeanTimeOffline">
            <summary>
            Mean productive time between failures (Downtime / # of continuous downtime events)
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.Metric.OperationalUtilization">
            <summary>
            Operational utilization (Productive time / Operations time)
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.Analytics.MetricResultUnit">
            <summary>
            Metric result units
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.MetricResultUnit.Percent">
            <summary>
            Result is returned as a percentage
            </summary>        
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.MetricResultUnit.Hour">
            <summary>
            Result is returned in hours
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.Analytics.MetricResultUnitAttribute">
            <summary>
            Documents the units used for an E10 metric
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResultUnitAttribute.Unit">
            <summary>
            The units for this metric
            </summary>
        </member>
        <member name="M:PTO.Charting.DataContracts.Analytics.MetricResultUnitAttribute.#ctor(PTO.Charting.DataContracts.Analytics.MetricResultUnit)">
            <summary>
            Constructor
            </summary>
            <param name="unit">The units used for an E10 metric</param>
        </member>
        <member name="M:PTO.Charting.DataContracts.Analytics.MetricResultUnitAttribute.GetUnit(PTO.Charting.DataContracts.Analytics.Metric)">
            <summary>
            Gets the units used by a metric based on the MetricResultUnitAttribute applied to the metric.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.Analytics.EntityFilters">
            <summary>
            Entity types to include in metric calculation.
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.EntityFilters.None">
            <summary>
            No entity filter
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.EntityFilters.Tool">
            <summary>
            Include tool level data in calculation
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.EntityFilters.EquipmentModule">
            <summary>
            Include module level data in calculation
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.EntityFilters.IntendedProcessSet">
            <summary>
            Include IPS level data in calculation
            </summary>
        </member>
        <member name="F:PTO.Charting.DataContracts.Analytics.EntityFilters.Mainframe">
            <summary>
            Include mainframe module data in calculation
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.Analytics.MetricResponse">
            <summary>
            Data summarization response for an E10 metric calculation.  
            The response will include an instance of this object for each entity.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResponse.EntityType">
            <summary>
            The type of entity.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResponse.EntityName">
            <summary>
            The name of the entity.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResponse.FirstPartitionStartDateTime">
            <summary>
            The starting time for the first partition
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResponse.LastPartitionEndDateTime">
            <summary>
            The ending time for the last partition
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.Analytics.MetricResponse.Values">
            <summary>
            The metric values for this entity. The size of the array is based on the partitions value in the request.  A null value indicates no data for the partition.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.CountResponse">
            <summary>
            Response to a CountRequest
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.CountResponse.Rows">
            <summary>
            The summary results
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.CountResponse.TooManyRows">
            <summary>
            Indicates if the database contains more rows than the MaxNumberOfRows specified in the request.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.DataItem">
            <summary>
            Identifies a Variable/Event/Alarm in a chart
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.DataItem.SourceId">
            <summary>
            The source ID for the item
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.DataItem.ItemId">
            <summary>
            The Item's ID
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.HistoricalCountRequest">
            <summary>
            Summarization consumer request for historical count data.
            </summary>
            <remarks>In the future this could be extended to support counting alarms or attribute changes.</remarks>
        </member>
        <member name="P:PTO.Charting.DataContracts.HistoricalCountRequest.Event">
            <summary>
            The event occurrence to summarize in the chart.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.HistoricalCountRequest.Attribute">
            <summary>
            The attribute used for grouping.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.HistoricalCountRequest.BucketSize">
            <summary>
            When zero the count is summarized per attribute value.  When greater than zero, the count is summarized 
            into numeric buckets of the specified width.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.MissingHandlerErrorMessageAttribute">
            <summary>
            Attribute applied to summarization requests to provide the error message returned to the client if no query is available to handle the request.
            </summary>
        </member>
        <member name="M:PTO.Charting.DataContracts.MissingHandlerErrorMessageAttribute.#ctor(System.String)">
            <summary>
            Initializes a new MissingHandlerErrorMessageAttribute instance
            </summary>
            <param name="message">The error message to report if no query handles the summarization request</param>
        </member>
        <member name="P:PTO.Charting.DataContracts.MissingHandlerErrorMessageAttribute.Message">
            <summary>
            The error message to report if no query handles the summarization request
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.SummarizationRequestBase">
            <summary>
            Base class for historical summarization requests
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.SummarizationRequestBase.StorageConsumerName">
            <summary>
            The name of the data consumer used to store the data.  
            This is used to find the correct database connection information.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.SummarizationRequestBase.PlanId">
            <summary>
            The plan that contains the items
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.SummarizationRequestBase.MaxNumberOfRows">
            <summary>
            The maximum number of rows to query.  A value of zero returns all rows.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.SummarizationRequestBase.CommandTimeout">
            <summary>
            The maximum timeout for the request.
            </summary>
        </member>
        <member name="T:PTO.Charting.DataContracts.ValueCount">
            <summary>
            Summary data result where the data is counted and then grouped by value.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.ValueCount.Value">
            <summary>
            The grouping value
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.ValueCount.Count">
            <summary>
            The count of items with specified value.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.ValueCount.MinCollectionTime">
            <summary>
            The minimum collection timestamp for the entries in this group.
            </summary>
        </member>
        <member name="P:PTO.Charting.DataContracts.ValueCount.MaxCollectionTime">
            <summary>
            The maximum collection timestamp for the entries in this group.
            </summary>
        </member>
    </members>
</doc>
