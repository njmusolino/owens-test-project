<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Inficon.Hardware</name>
    </assembly>
    <members>
        <member name="T:Inficon.Hardware.InficonCommand">
            <summary>
            Base class for sending commands to the Inficon gauge server
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommand.ResponseExpected">
            <summary>
            Flag indicating if the command is expecting more than just an acknowledgement from the Inficon server
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommand.#ctor(System.Boolean,PTO.EFEM.CommandCompletionHandler,System.String,PTO.EFEM.CommandAlive,PTO.EFEM.Driver)">
            <summary>
            Constructor
            </summary>
            <param name="responseExpected">Flag indicating if the command is expecting more than just an acknowledgement from the Inficon server</param>
        </member>
        <member name="M:Inficon.Hardware.InficonCommand.ParseResposne(System.String)">
            <summary>
            This method should be overridded by derived commands to parse the response from the gauge server.
            </summary>
        </member>
        <member name="T:Inficon.Hardware.InficonDriver">
            <summary>
            Driver for the Inficon gauge server
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriver.ComponentName">
            <summary>
            Component name
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriver.NewLineString">
            <summary>
            The new line string used for communication with the Rorze EFEM
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriver.m_activeCommand">
            <summary>
            The current active command
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriver.m_gauges">
            <summary>
            Dictionary of gauges indexed by channel number
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonDriver.CommLink">
            <summary>
            The inficon communication link
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonDriver.Units">
            <summary>
            The units that the data is read in
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonDriver.AutoClearAlarms">
            <summary>
            Flag indicating if the driver should auto clear alarms received during polling if
            the next poll command doesn't have an error
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.#ctor">
            <summary>
            Constructor
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.AddGauge(Inficon.Hardware.Gauge)">
            <summary>
            Add a gauge to the list of gauges attached to this gauge server
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.GetGauge(System.Int32)">
            <summary>
            Get the gauge that is connected to the gauge server on the specified channel
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.ParseMeasurementData(System.String)">
            <summary>
            Parse the measurement data returned from the gauge server
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.InitializeDriver(PTO.Threading.AsynchronousCallContext,PTO.EFEM.IDriverComponent)">
            <summary>
            Trigger the driver to initialize its connection to the hardware 
            and issue required setup commands
            </summary>
            <param name="context">Context used to track async communication</param>
            <param name="parent">
            Configuration information for the connection.  
            </param>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.SendCommand(PTO.EFEM.Command)">
            <summary>
            Send a command to the hardware
            </summary>
            <param name="commandToSend">The command to send to the hardware</param>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.Receive(System.Exception,System.String)">
            <summary>
            Receive a line of text from the hardware
            </summary>
            <param name="error">An exception describing any errors that occured</param>
            <param name="line">Line of text received</param>
        </member>
        <member name="M:Inficon.Hardware.InficonDriver.ShutdownDriver(PTO.Threading.AsynchronousCallContext)">
            <summary>
            Trigger the driver to disconnect from the hardware
            </summary>
        </member>
        <member name="T:Inficon.Hardware.Gauge">
            <summary>
            Gauge connected to Inficon server
            </summary>
        </member>
        <member name="F:Inficon.Hardware.Gauge.ClassName">
            <summary>
            Class name used for tracing
            </summary>
        </member>
        <member name="P:Inficon.Hardware.Gauge.SuppressGaugeAlarms">
            <summary>
            If this is true then alarms from the gauge will be suppressed.
            </summary>        
        </member>
        <member name="P:Inficon.Hardware.Gauge.IgnoreErrorsBeforeReceivingFirstGoodValue">
            <summary>
            If this is true then any errors received before a good value has been read will be ignored.
            </summary>
            <remarks>Some gauges report measurement out of range when at atmosphere. This allows us to ignore
            this error on startup until the pumps have kicked in</remarks>
        </member>
        <member name="P:Inficon.Hardware.Gauge.ChannelNumber">
            <summary>
            The channel number on the Inficon gauge server
            </summary>
        </member>
        <member name="P:Inficon.Hardware.Gauge.ReceivedGoodValue">
            <summary>
            Flag indicating if this gauge has received a good value since initialization.
            </summary>
        </member>
        <member name="P:Inficon.Hardware.Gauge.CommunicationLinkInstanceId">
            <summary>
            The instance Id of the InficonCommunicationLink to use
            </summary>
        </member>
        <member name="M:Inficon.Hardware.Gauge.#ctor">
            <summary>
            Constructor
            </summary>
        </member>
        <member name="M:Inficon.Hardware.Gauge.ComponentsCreated">
            <summary>
            Called once all the components have been created
            </summary>
        </member>
        <member name="M:Inficon.Hardware.Gauge.ParseSensorReading(System.String,PTO.Core.PressureUnit)">
            <summary>
            Parse a sensor reading from the Inficon gauge server
            </summary>
            <param name="sensorReading">The sensor reading</param>
            <param name="units">The units that the sensors reports data in</param>
        </member>
        <member name="T:Inficon.Hardware.InficonCommunicationLink">
            <summary>
            Communication link to the Inficon Gauge Server 
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonCommunicationLink.ComponentName">
            <summary>
            The component name
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonCommunicationLink.m_gemComponent">
            <summary>
            GEM component used for looking up alarm IDs.
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonCommunicationLink.m_activeAlarms">
            <summary>
            Dictionary of active alarms per gauge, indexed by channel
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonCommunicationLink.m_retriedSetUnits">
            <summary>
            Flag indicating if we've already tried to reset the units.
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonCommunicationLink.m_autoClearAlarms">
            <summary>
            Flag indicating if the driver should auto clear alarms received during polling if
            the next poll command doesn't have an error
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.AckResponse">
            <summary>
            The expected Ack response string for the Inficon gauge server
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.NakResponse">
            <summary>
            The expected Nak response string for the Inficon gauge server
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.GaugeUnits">
            <summary>
            The pressure units that the gauge server should use when reporting gauge measurements
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.PollingPeriod">
            <summary>
            The polling period to use for collecting data from the gauge server
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.AutoClearAlarms">
            <summary>
            Flag indicating if the driver should auto clear alarms received during polling if
            the next poll command doesn't have an error
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.#ctor">
            <summary>
            Constructor
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.SerialConfiguration">
            <summary>
            Configures the serial connection to the hardware
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.DriverConfiguration">
            <summary>
            Gets the driver configuration supported by this link
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.SocketConfiguration">
            <summary>
            Configures the socket settings if the active protocol is Socket
            </summary>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.ActiveProtocol">
            <summary>
            The communication protocol to use.  If Socket, the SocketSettings are used.  If Serial, the SerialSettings are used.
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.SetUnits(PTO.Core.PressureUnit)">
            <summary>
            Send a set units command to the gauge server.
            All future gauge measurements will be given in these units
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.SetUnitsAsync(PTO.Core.PressureUnit,System.Threading.CancellationToken)">
            <summary>
            Send a set units command to the gauge server.
            All future gauge measurements will be given in these units
            </summary>
            <param name="units"></param>
            <param name="token"></param>
            <returns></returns>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.StartContinuousMode(Inficon.Hardware.PollPeriod)">
            <summary>
            Switch the gauge server to continuous mode with the specified polling period
            The gauge server will send PTO gauge measurements at the specified rate without PTO
            sending the gauge server and further commands
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.StartContinuousModeAsync(Inficon.Hardware.PollPeriod,System.Threading.CancellationToken)">
            <summary>
            Switch the gauge server to continuous mode with the specified polling period
            The gauge server will send PTO gauge measurements at the specified rate without PTO
            sending the gauge server and further commands
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.StartContinuousModeComplete(PTO.EFEM.Command,System.Exception)">
            <summary>
            Called when the StartContinuousMode command completes
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.SetUnitsComplete(PTO.EFEM.Command,System.Exception)">
            <summary>
            Called when the SetUnits command completes
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.GetErrorDescription(Inficon.Hardware.InficonGaugeError)">
            <summary>
            Convert the specified Inficon error code into a description
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.RaiseAlarm(System.String,Inficon.Hardware.Gauge)">
            <summary>
            Raise an alarm to the alarm component
            </summary>
            <param name="errorCode">The inficon error code</param>
            <param name="gauge">The gauge that raised the error</param>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.ClearAlarm(Inficon.Hardware.Gauge)">
            <summary>
            Clear the active alarm for the specified gauge.
            </summary>
            <param name="gauge"></param>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.ComponentsCreated">
            <summary>
            Called once all tool components are created
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.CreateDriver">
            <summary>
            Create the EFEM driver
            </summary>
            <returns></returns>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.ChangeToolComponentMode(PTO.Core.ComponentModeChangeRequest)">
            <summary>
            Called when this tool component's mode is changed.  
            </summary>
            <param name="request">Mode change details.</param>
            <returns>A task that tracks method execution</returns>
        </member>
        <member name="M:Inficon.Hardware.InficonCommunicationLink.AlarmStateChange(PTO.Core.AlarmEventArgs)">
            <summary>
            Called when an alarm raised by this tool component changes state.
            </summary>
            <param name="e">The alarm that changed state</param>
        </member>
        <member name="P:Inficon.Hardware.InficonCommunicationLink.ResourceDriver">
            <summary>
            EFEM driver
            </summary>
        </member>
        <member name="T:Inficon.Hardware.PollPeriod">
            <summary>
            The gauge polling period
            </summary>
        </member>
        <member name="T:Inficon.Hardware.InficonGaugeError">
            <summary>
            List of known error code supported by the Inficon gauge server
            </summary>
        </member>
        <member name="T:Inficon.Hardware.InficonAlarmEventArgs">
            <summary>
            Custom Inficon alarm event args class
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonAlarmEventArgs.InficonError">
            <summary>
            The inficon error code that triggered this alarm
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonAlarmEventArgs.InficonGauge">
            <summary>
            The gauge that sent the error code
            </summary>
        </member>
        <member name="M:Inficon.Hardware.InficonAlarmEventArgs.#ctor(Inficon.Hardware.InficonGaugeError,Inficon.Hardware.Gauge,System.String,PTO.Core.ToolComponent)">
            <summary>
            Constructor
            </summary>
            <param name="inficonError">The inficon error code that triggered this alarm</param>
            <param name="gauge">The gauge that sent the error code</param>
            <param name="alid">Alarm id</param>
            <param name="source">The tool component that raised the alarm</param>
        </member>
        <member name="T:Inficon.Hardware.InficonDriverProtocol">
            <summary>
            Protocols supported by the Inficon Driver
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriverProtocol.Socket">
            <summary>
            Socket connection
            </summary>
        </member>
        <member name="F:Inficon.Hardware.InficonDriverProtocol.Serial">
            <summary>
            Serial connection
            </summary>
        </member>
        <member name="T:Inficon.Hardware.SetUnitsCommand">
            <summary>
            Command to set the units that the Inficon uses when reporting pressure measurements
            </summary>
        </member>
        <member name="F:Inficon.Hardware.SetUnitsCommand.CommandName">
            <summary>
            Command name
            </summary>
        </member>
        <member name="F:Inficon.Hardware.SetUnitsCommand.Base">
            <summary>
            The base command text
            </summary>
        </member>
        <member name="M:Inficon.Hardware.SetUnitsCommand.#ctor(PTO.Core.PressureUnit,PTO.EFEM.CommandCompletionHandler,PTO.EFEM.CommandAlive,PTO.EFEM.Driver)">
            <summary>
            Sets the units that will be used when receiving data from the gauges
            </summary>
            <param name="units">The pressure units to use.</param>
        </member>
        <member name="T:Inficon.Hardware.StartContinuousModeCommand">
            <summary>
            Command to start continuous mode on the Inficon gauge server
            </summary>
        </member>
        <member name="F:Inficon.Hardware.StartContinuousModeCommand.CommandName">
            <summary>
            Command name
            </summary>
        </member>
        <member name="F:Inficon.Hardware.StartContinuousModeCommand.Base">
            <summary>
            The base command text
            </summary>
        </member>
        <member name="M:Inficon.Hardware.StartContinuousModeCommand.#ctor(Inficon.Hardware.PollPeriod,PTO.EFEM.CommandCompletionHandler,PTO.EFEM.CommandAlive,PTO.EFEM.Driver)">
            <summary>
            Starts continuous measurement mode
            </summary>
            <param name="period">The period at which to take measurements from the gauges</param>
        </member>
    </members>
</doc>
