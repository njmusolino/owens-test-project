﻿----------------------------------------------------------------------
-- $Workfile: EIBStorageUpdate.sql
-- Summary: Script that updates the EIBStorage database to the current version
-- Project: EIB 7.2
--
-- Description:
-- Script that updates the EIBStorage database to the current version
--
-- $Author: Alex Dlugokecki
-- $Date: 2014-09-15
-- $Revision: 7.2
--
-- Copyright(C) The PEER Group Inc., 2014 - 2016.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- 7.1 - 2014-05-08 - EIB 7.1, Initial Creation
-- 7.2 - 2014-09-15 - EIB 7.2, Performance Indexes, Enabling Snapshot Isolation, Making Requested Exception Fields Nullable, Fix to GetEIBStorageDatabaseInfo stored proc
-- --------------------------------------------------------------------

BEGIN TRY

	-- Enable Snapshot isolation if necessary.  
	-- Note that dynamic SQL is being used here in order to use the current DB_NAME, and that snapshot isolation cannot be enabled within a transaction. (Hence why it's not enabled below.)
	DECLARE @CHECK_SNAPSHOT_ISOLATION_SQL NVARCHAR(300)
	DECLARE @CHECK_SNAPSHOT_ISOLATION NVARCHAR(5)

	-- Build a dynamic query to check if snapshot isolation is enabled
	SET @CHECK_SNAPSHOT_ISOLATION_SQL = 'SELECT @CHECK_SNAPSHOT_ISOLATION=SNAPSHOT_ISOLATION_STATE_DESC from sys.databases WHERE NAME = ''' + 
		REPLACE(REPLACE(QUOTENAME(DB_NAME()), '[', ''), ']', '') + ''''
	EXEC sp_executesql @CHECK_SNAPSHOT_ISOLATION_SQL, N'@CHECK_SNAPSHOT_ISOLATION varchar(5) output', @CHECK_SNAPSHOT_ISOLATION output

	-- if snapshot isolation is not enabled, enable it
	IF (@CHECK_SNAPSHOT_ISOLATION <> 'ON')
	BEGIN
		DECLARE @ALTER_DB_SNAPSHOT_ISOLATION_SQL VARCHAR(300)
		SET @ALTER_DB_SNAPSHOT_ISOLATION_SQL = 'ALTER DATABASE ' + QUOTENAME(DB_NAME()) + 'SET ALLOW_SNAPSHOT_ISOLATION ON'
		EXEC (@ALTER_DB_SNAPSHOT_ISOLATION_SQL)
	END     

	-- BEGIN 7.1
BEGIN TRANSACTION

	DECLARE @TargetVersionNumber NVARCHAR(20) = '7.1.0.0'
	DECLARE @TargetVersionName NVARCHAR(100) = 'EIB 7.1' 
	DECLARE @PreviousVersionNumber NVARCHAR(20) = '7.0.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

	-- Performance Indexes

	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TraceData]') AND name = N'_dta_index_TraceData_5_997578592__K4D_K5D_K2_K1_3_6_7_8_9_10')
	DROP INDEX [_dta_index_TraceData_5_997578592__K4D_K5D_K2_K1_3_6_7_8_9_10] ON [dbo].[TraceData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_TraceData_5_997578592__K4D_K5D_K2_K1_3_6_7_8_9_10] ON [dbo].[TraceData] 
	(
		[ReportTime] DESC,
		[CollectionTime] DESC,
		[PlanDataID] ASC,
		[TraceDataID] ASC
	)
	INCLUDE ( [TraceID],
	[IsPermanent],
	[DateCreated],
	[UserCreatedID],
	[DateModified],
	[UserModifiedID]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TraceValueData]') AND name = N'_dta_index_TraceValueData_5_1125579048__K2_1_3_4_5_6_7_8_9_10_11_12_13_14_15_16')
	DROP INDEX [_dta_index_TraceValueData_5_1125579048__K2_1_3_4_5_6_7_8_9_10_11_12_13_14_15_16] ON [dbo].[TraceValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_TraceValueData_5_1125579048__K2_1_3_4_5_6_7_8_9_10_11_12_13_14_15_16] ON [dbo].[TraceValueData] 
	(
		[TraceDataID] ASC
	)
	INCLUDE ( [TraceValueDataID],
	[ValueDataTypeID],
	[ValueIdentifierDataID],
	[ValueInt],
	[ValueFloat],
	[ValueBit],
	[ValueString],
	[ValueXML],
	[ValueDate],
	[SerializedTypeDataID],
	[IsPermanent],
	[DateCreated],
	[UserCreatedID],
	[DateModified],
	[UserModifiedID]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.1

	-- BEGIN 7.2

	SET @TargetVersionNumber = '7.2.0.0'
	SET @TargetVersionName = 'EIB 7.2' 
	SET @PreviousVersionNumber = '7.1.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN
	-- Make Requested Exception Fields Nullable
	ALTER TABLE AlarmData ALTER COLUMN RequestedExceptionType NVARCHAR(100) NULL

	ALTER TABLE AlarmIdentifierData ALTER COLUMN RequestedSourceID NVARCHAR(4000) NULL

	ALTER TABLE AlarmIdentifierData ALTER COLUMN RequestedExceptionID NVARCHAR(500) NULL

   IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEIBStorageDatabaseInfo]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetEIBStorageDatabaseInfo]

EXEC('----------------------------------------------------------------------
-- $Workfile: EIBStorage.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.0
--
-- Description:
-- Gets the version of the SQL Server database and the amount of available space based on the edition
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------
CREATE PROC GetEIBStorageDatabaseInfo
AS
SELECT SQLServerInstanceName = CAST(SERVERPROPERTY(''ServerName'') AS NVARCHAR)
	,SQLServerVersion = @@VERSION
	,SQLServerEdition = CAST(SERVERPROPERTY(''Edition'') AS NVARCHAR)
	,EIBStorageDatabaseName = a.NAME
	,MaximumMB = CASE 
		WHEN SERVERPROPERTY(''EngineEdition'') = 4 -- Express Edition
			THEN CASE 
					WHEN CAST(SERVERPROPERTY(''ProductVersion'') AS NVARCHAR(128)) LIKE ''10.0%'' -- SQL Server 2008 (pre R2)
						THEN 4096
					ELSE 10240
					END
		ELSE -1
		END
	,FileSizeMB = CONVERT(FLOAT, ROUND(a.size / 128.000, 2))
	,UsedMB = CONVERT(FLOAT, ROUND(FILEPROPERTY(a.NAME, ''SpaceUsed'') / 128.000, 2))
	,AvailableMB = CASE 
		WHEN SERVERPROPERTY(''EngineEdition'') = 4 -- Express Edition
			THEN CASE 
					WHEN CAST(SERVERPROPERTY(''ProductVersion'') AS NVARCHAR(128)) LIKE ''10.0%'' -- SQL Server 2008 (pre R2)
						THEN 4096
					ELSE 10240 --SQL Server 2008 R2 or higher
					END - CONVERT(FLOAT, ROUND(FILEPROPERTY(a.NAME, ''SpaceUsed'') / 128.000, 2))
		ELSE -1
		END 
FROM SYS.sysfiles a
WHERE a.NAME NOT LIKE ''%_log''')

	-- Performance Indexes

	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventData]') AND name = N'_dta_index_EventData_5_885578193__K2_K4_K1_K3')
	DROP INDEX [_dta_index_EventData_5_885578193__K2_K4_K1_K3] ON [dbo].[EventData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventData_5_885578193__K2_K4_K1_K3] ON [dbo].[EventData] 
	(
		  [PlanDataID] ASC,
		  [EventTime] ASC,
		  [EventDataID] ASC,
		  [EventIdentifierDataID] ASC
	)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_6')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_6] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_6] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueInt]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_7')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_7] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_7] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueFloat]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_8')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_8] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_8] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueBit]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_9')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_9] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_9] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueString]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_10')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_10] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_10] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueXML]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K2_K1_K5_11')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_11] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K2_K1_K5_11] ON [dbo].[EventValueData] 
	(
		  [EventDataID] ASC,
		  [EventValueDataID] ASC,
		  [ValueIdentifierDataID] ASC
	)
	INCLUDE ( [ValueDate]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventData]') AND name = N'_dta_index_EventData_5_885578193__K3_K4_K1_K2')
	DROP INDEX [_dta_index_EventData_5_885578193__K3_K4_K1_K2] ON [dbo].[EventData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventData_5_885578193__K3_K4_K1_K2] ON [dbo].[EventData] 
	(
		  [EventIdentifierDataID] ASC,
		  [EventTime] ASC,
		  [EventDataID] ASC,
		  [PlanDataID] ASC
	)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[EventValueData]') AND name = N'_dta_index_EventValueData_5_1333579789__K5_K2_K6')
	DROP INDEX [_dta_index_EventValueData_5_1333579789__K5_K2_K6] ON [dbo].[EventValueData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_EventValueData_5_1333579789__K5_K2_K6] ON [dbo].[EventValueData] 
	(
		  [ValueIdentifierDataID] ASC,
		  [EventDataID] ASC
	)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]


	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TraceData]') AND name = N'_dta_index_TraceData_5_997578592__K2_K4_K1_K5_3_6_7_8_9_10')
	DROP INDEX [_dta_index_TraceData_5_997578592__K2_K4_K1_K5_3_6_7_8_9_10] ON [dbo].[TraceData] WITH ( ONLINE = OFF )

	CREATE NONCLUSTERED INDEX [_dta_index_TraceData_5_997578592__K2_K4_K1_K5_3_6_7_8_9_10] ON [dbo].[TraceData] 
	(
		  [PlanDataID] ASC,
		  [ReportTime] ASC,
		  [TraceDataID] ASC,
		  [CollectionTime] ASC
	)
	INCLUDE ( [TraceID],
	[IsPermanent],
	[DateCreated],
	[UserCreatedID],
	[DateModified],
	[UserModifiedID]) WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.2

	-- BEGIN 7.3

	SET @TargetVersionNumber = '7.3.0.0'
	SET @TargetVersionName = 'EIB 7.3'
	SET @PreviousVersionNumber = '7.2.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN
	
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeEIBStorageData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[PurgeEIBStorageData]

EXEC('----------------------------------------------------------------------
-- $Workfile: EIBStorage.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.0
--
-- Description:
-- Purges data from the EIBStorage database that is older that the specified number of days
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-----------------------------------------------------------------------
CREATE PROC [dbo].[PurgeEIBStorageData] @InterfaceName NVARCHAR(255)
	,@DataConsumerName NVARCHAR(255)
	,@DaysOfDataToKeep INT
AS
SET XACT_ABORT ON;

DECLARE @BatchSize INT = 100000
DECLARE @DelayDuration VARCHAR(10) = ''00:00:00.020''

BEGIN TRY
	DECLARE @PurgeThresholdDate DATETIME = DATEADD(DAY, - @DaysOfDataToKeep, GETDATE())
	DECLARE @RecordsDeleted INT = 0
	DECLARE @Rows INT = 0

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avcid
		FROM AlarmValueContextInstanceData avcid
		INNER JOIN AlarmValueData avd ON avd.AlarmValueDataID = avcid.AlarmValueDataID
		INNER JOIN AlarmData ad ON ad.AlarmDataID = avd.AlarmDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avcid.DateCreated < @PurgeThresholdDate
			AND avcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avcid
		FROM AttributeValueContextInstanceData avcid
		INNER JOIN AttributeValueData avd ON avd.AttributeValueDataID = avcid.AttributeValueDataID
		INNER JOIN AttributeData ad ON ad.AttributeDataID = avd.AttributeDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avcid.DateCreated < @PurgeThresholdDate
			AND avcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) evcid
		FROM EventValueContextInstanceData evcid
		INNER JOIN EventValueData evd ON evd.EventValueDataID = evcid.EventValueDataID
		INNER JOIN [EventData] ed ON ed.EventDataID = evd.EventDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND evcid.DateCreated < @PurgeThresholdDate
			AND evcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avd
		FROM AlarmValueData avd
		INNER JOIN AlarmData ad ON ad.AlarmDataID = avd.AlarmDataID
		LEFT OUTER JOIN AlarmValueContextInstanceData avcid ON avcid.AlarmValueDataID = avd.AlarmValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avd.DateCreated < @PurgeThresholdDate
			AND avd.IsPermanent = 0
			AND NOT EXISTS (SELECT alvcid.AlarmValueDataID
								FROM AlarmValueContextInstanceData alvcid
							WHERE alvcid.AlarmValueDataID = avd.AlarmValueDataID)

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avd
		FROM AttributeValueData avd
		INNER JOIN AttributeData ad ON ad.AttributeDataID = avd.AttributeDataID
		LEFT OUTER JOIN AttributeValueContextInstanceData avcid ON avcid.AttributeValueDataID = avd.AttributeValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avd.DateCreated < @PurgeThresholdDate
			AND avd.IsPermanent = 0
			AND NOT EXISTS (SELECT atvcid.AttributeValueDataID
								FROM AttributeValueContextInstanceData atvcid
							WHERE atvcid.AttributeValueDataID = avd.AttributeValueDataID)

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) evd
		FROM EventValueData evd
		INNER JOIN [EventData] ed ON ed.EventDataID = evd.EventDataID
		LEFT OUTER JOIN EventValueContextInstanceData evcid ON evcid.EventValueDataID = evd.EventValueDataID
			AND evcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND evd.DateCreated < @PurgeThresholdDate
			AND evd.IsPermanent = 0
			AND NOT EXISTS (SELECT evtcid.EventValueDataID
								FROM EventValueContextInstanceData evtcid
							WHERE evtcid.EventValueDataID = evd.EventValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) tvd
		FROM TraceValueData tvd
		INNER JOIN TraceData td ON td.TraceDataID = tvd.TraceDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = td.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND tvd.DateCreated < @PurgeThresholdDate
			AND tvd.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ad
		FROM AlarmData ad
		LEFT OUTER JOIN AlarmValueData avd ON avd.AlarmDataID = ad.AlarmDataID
		LEFT OUTER JOIN AlarmValueContextInstanceData avcid ON avcid.AlarmValueDataID = avd.AlarmValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ad.DateCreated < @PurgeThresholdDate
			AND ad.IsPermanent = 0
			AND NOT EXISTS (SELECT alvd.AlarmDataID
								FROM AlarmValueData alvd
							WHERE alvd.AlarmDataID = ad.AlarmDataID)
			AND NOT EXISTS (SELECT alvcid.AlarmValueDataID
								FROM AlarmValueContextInstanceData alvcid
							WHERE alvcid.AlarmValueDataID = avd.AlarmValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ad
		FROM AttributeData ad
		LEFT OUTER JOIN AttributeValueData avd ON avd.AttributeDataID = ad.AttributeDataID
		LEFT OUTER JOIN AttributeValueContextInstanceData avcid ON avcid.AttributeValueDataID = avd.AttributeValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ad.DateCreated < @PurgeThresholdDate
			AND ad.IsPermanent = 0
			AND NOT EXISTS (SELECT atvd.AttributeDataID
								FROM AttributeValueData atvd
							WHERE atvd.AttributeDataID = ad.AttributeDataID)
			AND NOT EXISTS (SELECT atvcid.AttributeValueDataID
								FROM AttributeValueContextInstanceData atvcid
							WHERE atvcid.AttributeValueDataID = avd.AttributeValueDataID)
		
		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ed
		FROM [EventData] ed
		LEFT OUTER JOIN EventValueData evd ON evd.EventDataID = ed.EventDataID
		LEFT OUTER JOIN EventValueContextInstanceData evcid ON evcid.EventValueDataID = evd.EventValueDataID
			AND evcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ed.DateCreated < @PurgeThresholdDate
			AND ed.IsPermanent = 0
			AND NOT EXISTS (SELECT evtvd.EventDataID
								FROM EventValueData evtvd
							WHERE evtvd.EventDataID = ed.EventDataID)
			AND NOT EXISTS (SELECT evtcid.EventValueDataID
								FROM EventValueContextInstanceData evtcid
							WHERE evtcid.EventValueDataID = evd.EventValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) td
		FROM TraceData td
		INNER JOIN PlanData pd ON pd.PlanDataID = td.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND td.DateCreated < @PurgeThresholdDate
			AND td.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) cid
		FROM ContextInstanceData cid
		INNER JOIN PlanData pd ON pd.PlanDataID = cid.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND cid.DateCreated < @PurgeThresholdDate
			AND NOT EXISTS (
				SELECT avcid.AlarmValueContextInstanceDataID
				FROM AlarmValueContextInstanceData avcid
				WHERE avcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND NOT EXISTS (
				SELECT atvcid.AttributeValueContextInstanceDataID
				FROM AttributeValueContextInstanceData atvcid
				WHERE atvcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND NOT EXISTS (
				SELECT evcid.EventValueContextInstanceDataID
				FROM EventValueContextInstanceData evcid
				WHERE evcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND cid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	SELECT @RecordsDeleted
END TRY

BEGIN CATCH
	DECLARE @ERROR_SEVERITY INT
		,@ERROR_STATE INT
		,@ERROR_NUMBER INT
		,@ERROR_LINE INT
		,@ERROR_MESSAGE NVARCHAR(4000)

	SELECT @ERROR_SEVERITY = ERROR_SEVERITY()
		,@ERROR_STATE = ERROR_STATE()
		,@ERROR_NUMBER = ERROR_NUMBER()
		,@ERROR_LINE = ERROR_LINE()
		,@ERROR_MESSAGE = ERROR_MESSAGE()

	RAISERROR (
			''Msg %d, Line %d, :%s''
			,@ERROR_SEVERITY
			,@ERROR_STATE
			,@ERROR_NUMBER
			,@ERROR_LINE
			,@ERROR_MESSAGE
			)
END CATCH')

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.3

		-- BEGIN 7.4

	SET @TargetVersionNumber = '7.4.0.0'
	SET @TargetVersionName = 'EIB 7.4'
	SET @PreviousVersionNumber = '7.3.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN
	
	IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeEIBStorageData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[PurgeEIBStorageData]

EXEC('----------------------------------------------------------------------
-- $Workfile: EIBStorage.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.0
--
-- Description:
-- Purges data from the EIBStorage database that is older that the specified number of days
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-----------------------------------------------------------------------
CREATE PROC [dbo].[PurgeEIBStorageData] @InterfaceName NVARCHAR(255)
	,@DataConsumerName NVARCHAR(255)
	,@DaysOfDataToKeep INT
AS
SET XACT_ABORT ON;

DECLARE @BatchSize INT = 100000
DECLARE @DelayDuration VARCHAR(10) = ''00:00:00.020''

BEGIN TRY
	DECLARE @PurgeThresholdDate DATETIME = DATEADD(DAY, - @DaysOfDataToKeep, GETDATE())
	DECLARE @RecordsDeleted INT = 0
	DECLARE @Rows INT = 0

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avcid
		FROM AlarmValueContextInstanceData avcid
		INNER JOIN AlarmValueData avd ON avd.AlarmValueDataID = avcid.AlarmValueDataID
		INNER JOIN AlarmData ad ON ad.AlarmDataID = avd.AlarmDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avcid.DateCreated < @PurgeThresholdDate
			AND avcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avcid
		FROM AttributeValueContextInstanceData avcid
		INNER JOIN AttributeValueData avd ON avd.AttributeValueDataID = avcid.AttributeValueDataID
		INNER JOIN AttributeData ad ON ad.AttributeDataID = avd.AttributeDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avcid.DateCreated < @PurgeThresholdDate
			AND avcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) evcid
		FROM EventValueContextInstanceData evcid
		INNER JOIN EventValueData evd ON evd.EventValueDataID = evcid.EventValueDataID
		INNER JOIN [EventData] ed ON ed.EventDataID = evd.EventDataID
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND evcid.DateCreated < @PurgeThresholdDate
			AND evcid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avd
		FROM AlarmValueData avd
		INNER JOIN AlarmData ad ON ad.AlarmDataID = avd.AlarmDataID
		LEFT OUTER JOIN AlarmValueContextInstanceData avcid ON avcid.AlarmValueDataID = avd.AlarmValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avd.DateCreated < @PurgeThresholdDate
			AND avd.IsPermanent = 0
			AND NOT EXISTS (SELECT alvcid.AlarmValueDataID
								FROM AlarmValueContextInstanceData alvcid
							WHERE alvcid.AlarmValueDataID = avd.AlarmValueDataID)

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) avd
		FROM AttributeValueData avd
		INNER JOIN AttributeData ad ON ad.AttributeDataID = avd.AttributeDataID
		LEFT OUTER JOIN AttributeValueContextInstanceData avcid ON avcid.AttributeValueDataID = avd.AttributeValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND avd.DateCreated < @PurgeThresholdDate
			AND avd.IsPermanent = 0
			AND NOT EXISTS (SELECT atvcid.AttributeValueDataID
								FROM AttributeValueContextInstanceData atvcid
							WHERE atvcid.AttributeValueDataID = avd.AttributeValueDataID)

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) evd
		FROM EventValueData evd
		INNER JOIN [EventData] ed ON ed.EventDataID = evd.EventDataID
		LEFT OUTER JOIN EventValueContextInstanceData evcid ON evcid.EventValueDataID = evd.EventValueDataID
			AND evcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND evd.DateCreated < @PurgeThresholdDate
			AND evd.IsPermanent = 0
			AND NOT EXISTS (SELECT evtcid.EventValueDataID
								FROM EventValueContextInstanceData evtcid
							WHERE evtcid.EventValueDataID = evd.EventValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	ALTER TABLE [TraceValueData] NOCHECK CONSTRAINT ALL;
	ALTER INDEX [_dta_index_TraceValueData_5_1125579048__K2_1_3_4_5_6_7_8_9_10_11_12_13_14_15_16] ON [TraceValueData] DISABLE;

	WHILE 1=1
	BEGIN
		BEGIN TRANSACTION TDelete
    
		;WITH CTE AS
		(
			SELECT TOP(@BatchSize) [TraceValueDataID], tvd.[TraceDataID], tvd.[ValueDataTypeID], tvd.[ValueIdentifierDataID], tvd.[ValueInt], tvd.[ValueFloat], tvd.[ValueBit], tvd.[ValueString], tvd.[ValueXML], tvd.[ValueDate], tvd.[SerializedTypeDataID], tvd.[IsPermanent], tvd.[DateCreated], tvd.[UserCreatedID], tvd.[DateModified], tvd.[UserModifiedID]
			FROM [TraceValueData] tvd
			WHERE TraceDataID in 
				(SELECT TraceDataID 
					FROM [TraceData]
					WHERE PlanDataID in 
							(SELECT PlanDataID
								FROM PlanData
								WHERE (InterfaceName = @InterfaceName OR @InterfaceName IS NULL)
								AND
									(DataConsumerName = @DataConsumerName OR @DataConsumerName IS NULL)))
			AND tvd.DateCreated < @PurgeThresholdDate
			AND tvd.IsPermanent = 0		
			ORDER BY [TraceValueDataID]
		)
		DELETE CTE
		SET @Rows = @@ROWCOUNT

		COMMIT TRANSACTION TDelete

		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		CHECKPOINT

		IF (@Rows < @BatchSize)
			BREAK;
	END

	ALTER TABLE [TraceValueData] CHECK CONSTRAINT ALL;
	ALTER INDEX [_dta_index_TraceValueData_5_1125579048__K2_1_3_4_5_6_7_8_9_10_11_12_13_14_15_16] ON [TraceValueData] REBUILD;

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ad
		FROM AlarmData ad
		LEFT OUTER JOIN AlarmValueData avd ON avd.AlarmDataID = ad.AlarmDataID
		LEFT OUTER JOIN AlarmValueContextInstanceData avcid ON avcid.AlarmValueDataID = avd.AlarmValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ad.DateCreated < @PurgeThresholdDate
			AND ad.IsPermanent = 0
			AND NOT EXISTS (SELECT alvd.AlarmDataID
								FROM AlarmValueData alvd
							WHERE alvd.AlarmDataID = ad.AlarmDataID)
			AND NOT EXISTS (SELECT alvcid.AlarmValueDataID
								FROM AlarmValueContextInstanceData alvcid
							WHERE alvcid.AlarmValueDataID = avd.AlarmValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ad
		FROM AttributeData ad
		LEFT OUTER JOIN AttributeValueData avd ON avd.AttributeDataID = ad.AttributeDataID
		LEFT OUTER JOIN AttributeValueContextInstanceData avcid ON avcid.AttributeValueDataID = avd.AttributeValueDataID
			AND avcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ad.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ad.DateCreated < @PurgeThresholdDate
			AND ad.IsPermanent = 0
			AND NOT EXISTS (SELECT atvd.AttributeDataID
								FROM AttributeValueData atvd
							WHERE atvd.AttributeDataID = ad.AttributeDataID)
			AND NOT EXISTS (SELECT atvcid.AttributeValueDataID
								FROM AttributeValueContextInstanceData atvcid
							WHERE atvcid.AttributeValueDataID = avd.AttributeValueDataID)
		
		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) ed
		FROM [EventData] ed
		LEFT OUTER JOIN EventValueData evd ON evd.EventDataID = ed.EventDataID
		LEFT OUTER JOIN EventValueContextInstanceData evcid ON evcid.EventValueDataID = evd.EventValueDataID
			AND evcid.DateCreated < @PurgeThresholdDate
		INNER JOIN PlanData pd ON pd.PlanDataID = ed.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND ed.DateCreated < @PurgeThresholdDate
			AND ed.IsPermanent = 0
			AND NOT EXISTS (SELECT evtvd.EventDataID
								FROM EventValueData evtvd
							WHERE evtvd.EventDataID = ed.EventDataID)
			AND NOT EXISTS (SELECT evtcid.EventValueDataID
								FROM EventValueContextInstanceData evtcid
							WHERE evtcid.EventValueDataID = evd.EventValueDataID)

		SET @Rows = @@ROWCOUNT
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	ALTER TABLE [TraceData] NOCHECK CONSTRAINT ALL;
	ALTER INDEX [_dta_index_TraceData_5_997578592__K2_K4_K1_K5_3_6_7_8_9_10] ON [TraceData] DISABLE;
	ALTER INDEX [_dta_index_TraceData_5_997578592__K4D_K5D_K2_K1_3_6_7_8_9_10] ON [TraceData] DISABLE;

	WHILE 1=1
	BEGIN
	BEGIN TRANSACTION TDelete
    
		;WITH CTE AS
		(
			SELECT TOP(@BatchSize) td.[TraceDataID], td.[PlanDataID], td.[TraceID], td.[ReportTime], td.[CollectionTime], td.[IsPermanent], td.[DateCreated], td.[UserCreatedID], td.[DateModified], td.[UserModifiedID] 
			FROM TraceData td
			WHERE PlanDataID in 
					(SELECT PlanDataID
						FROM PlanData
						WHERE (InterfaceName = @InterfaceName OR @InterfaceName IS NULL)
						AND
							(DataConsumerName = @DataConsumerName OR @DataConsumerName IS NULL))
			AND td.DateCreated < @PurgeThresholdDate
			AND td.IsPermanent = 0
			ORDER BY [TraceDataID]
		)
		DELETE CTE
		SET @Rows = @@ROWCOUNT

		COMMIT TRANSACTION TDelete

		SET @RecordsDeleted = @RecordsDeleted + @Rows

		CHECKPOINT

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	ALTER TABLE [TraceValueData] CHECK CONSTRAINT ALL;
	ALTER INDEX [_dta_index_TraceData_5_997578592__K2_K4_K1_K5_3_6_7_8_9_10] ON [TraceData] REBUILD;
	ALTER INDEX [_dta_index_TraceData_5_997578592__K4D_K5D_K2_K1_3_6_7_8_9_10] ON [TraceData] REBUILD;

	WHILE 1=1
	BEGIN
		DELETE TOP (@BatchSize) cid
		FROM ContextInstanceData cid
		INNER JOIN PlanData pd ON pd.PlanDataID = cid.PlanDataID
		WHERE (
				(pd.InterfaceName = @InterfaceName)
				OR (@InterfaceName IS NULL)
				)
			AND (
				(pd.DataConsumerName = @DataConsumerName)
				OR (@DataConsumerName IS NULL)
				)
			AND cid.DateCreated < @PurgeThresholdDate
			AND NOT EXISTS (
				SELECT avcid.AlarmValueContextInstanceDataID
				FROM AlarmValueContextInstanceData avcid
				WHERE avcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND NOT EXISTS (
				SELECT atvcid.AttributeValueContextInstanceDataID
				FROM AttributeValueContextInstanceData atvcid
				WHERE atvcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND NOT EXISTS (
				SELECT evcid.EventValueContextInstanceDataID
				FROM EventValueContextInstanceData evcid
				WHERE evcid.ContextInstanceDataID = cid.ContextInstanceDataID
				)
			AND cid.IsPermanent = 0

		SET @Rows = @@ROWCOUNT	
		SET @RecordsDeleted = @RecordsDeleted + @Rows

		WAITFOR DELAY @DelayDuration

		IF (@Rows < @BatchSize)
			BREAK;
	END

	SELECT @RecordsDeleted
END TRY

BEGIN CATCH
	DECLARE @ERROR_SEVERITY INT
		,@ERROR_STATE INT
		,@ERROR_NUMBER INT
		,@ERROR_LINE INT
		,@ERROR_MESSAGE NVARCHAR(4000)

	SELECT @ERROR_SEVERITY = ERROR_SEVERITY()
		,@ERROR_STATE = ERROR_STATE()
		,@ERROR_NUMBER = ERROR_NUMBER()
		,@ERROR_LINE = ERROR_LINE()
		,@ERROR_MESSAGE = ERROR_MESSAGE()

	RAISERROR (
			''Msg %d, Line %d, :%s''
			,@ERROR_SEVERITY
			,@ERROR_STATE
			,@ERROR_NUMBER
			,@ERROR_LINE
			,@ERROR_MESSAGE
			)
END CATCH')

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.4

	-- BEGIN 7.5

	SET @TargetVersionNumber = '7.5.0.0'
	SET @TargetVersionName = 'EIB 7.5'
	SET @PreviousVersionNumber = '7.4.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.5

	-- BEGIN 7.6

	SET @TargetVersionNumber = '7.6.0.0'
	SET @TargetVersionName = 'EIB 7.6'
	SET @PreviousVersionNumber = '7.5.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.6
	-- BEGIN 7.6.2

	SET @TargetVersionNumber = '7.6.2.0'
	SET @TargetVersionName = 'EIB 7.6 SP2'
	SET @PreviousVersionNumber = '7.6.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

	-- As part of the 7.7 upgrade, the stored procedure changes made in EIB 7.6 SP2
	-- are no longer applicable and as such have been removed from the upgrade script.
	
	-- appy the new indexes, otherwise exit with an error message.

	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TraceValueData]') AND name = N'NC_IDX_TraceValueData_TraceDataID')
	DROP INDEX [NC_IDX_TraceValueData_TraceDataID] ON [dbo].[TraceValueData] WITH ( ONLINE = OFF )
	CREATE NONCLUSTERED INDEX [NC_IDX_TraceValueData_TraceDataID] ON [dbo].[TraceValueData] 
	(
		[TraceDataID] ASC
	) INCLUDE ([TraceValueDataID])
	WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[AlarmValueData]') AND name = N'NC_IDX_AlarmValueData_AlarmDataID')
	DROP INDEX [NC_IDX_AlarmValueData_AlarmDataID] ON [dbo].[AlarmValueData] WITH ( ONLINE = OFF )
	CREATE NONCLUSTERED INDEX [NC_IDX_AlarmValueData_AlarmDataID] ON [dbo].[AlarmValueData]
	(
		[AlarmDataID]
	) INCLUDE ([AlarmValueDataID])
	WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

	IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[AttributeValueData]') AND name = N'NC_IDX_AttributeValueData_AttributeDataID')
	DROP INDEX [NC_IDX_AttributeValueData_AttributeDataID] ON [dbo].[AttributeValueData] WITH ( ONLINE = OFF )
	CREATE NONCLUSTERED INDEX [NC_IDX_AttributeValueData_AttributeDataID] ON [dbo].[AttributeValueData] 
	(
		[AttributeDataID]
	) INCLUDE (AttributeValueDataID)
	WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	-- END 7.6.2    

	-- BEGIN 7.7


  	SET @TargetVersionNumber = '7.7.0.0'
	SET @TargetVersionName  = 'EIB 7.7' 
	SET @PreviousVersionNumber  = '7.6.2.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN
	
	-- Remove stored procedures	
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertEventValueContextInstanceData]') AND type in (N'P', N'PC'))
		BEGIN
			DROP PROCEDURE InsertEventValueContextInstanceData
			DROP PROCEDURE InsertAttributeValueContextInstanceData
			DROP PROCEDURE InsertAlarmValueContextInstanceData
			DROP PROCEDURE GetEventValueContextInstanceData
			DROP PROCEDURE GetAlarmValueContextInstanceData
			DROP PROCEDURE GetAttributeValueContextInstanceData
			DROP PROCEDURE DeleteEventValueContextInstanceData
			DROP PROCEDURE DeleteAttributeValueContextInstanceData
			DROP PROCEDURE DeleteAlarmValueContextInstanceData
			DROP PROCEDURE GetEIBStorageDatabaseInfo
			DROP PROCEDURE DeleteValueDataType
			DROP PROCEDURE DeleteUserData
			DROP PROCEDURE DeleteVersionData
			DROP PROCEDURE GetValueDataType
			DROP PROCEDURE GetUserData
			DROP PROCEDURE GetVersionData
			DROP PROCEDURE GetEIBStorageVersion
			DROP PROCEDURE UpdateValueDataType
			DROP PROCEDURE UpdateUserData
			DROP PROCEDURE InsertValueDataType
			DROP PROCEDURE InsertUserData
			DROP PROCEDURE InsertVersionData
			DROP PROCEDURE UpdateVersionData
			DROP PROCEDURE UpdateValueIdentifierData
			DROP PROCEDURE UpdateEventIdentifierData
			DROP PROCEDURE UpdateSerializedTypeData
			DROP PROCEDURE UpdatePlanDetailData
			DROP PROCEDURE UpdateAlarmIdentifierData
			DROP PROCEDURE InsertValueIdentifierData
			DROP PROCEDURE InsertSerializedTypeData
			DROP PROCEDURE InsertPlanDetailData
			DROP PROCEDURE GetAlarmIdentifierData
			DROP PROCEDURE InsertEventIdentifierData
			DROP PROCEDURE GetValueIdentifierData
			DROP PROCEDURE InsertAlarmIdentifierData
			DROP PROCEDURE GetEventIdentifierData
			DROP PROCEDURE GetSerializedTypeData
			DROP PROCEDURE GetPlanDetailDataByPlanDetailHash
			DROP PROCEDURE GetPlanDetailData
			DROP PROCEDURE DeleteAlarmIdentifierData
			DROP PROCEDURE DeleteEventIdentifierData
			DROP PROCEDURE DeleteValueIdentifierData
			DROP PROCEDURE DeleteSerializedTypeData
			DROP PROCEDURE DeletePlanDetailData
			DROP PROCEDURE DeletePlanData
			DROP PROCEDURE GetPlanData
			DROP PROCEDURE InsertPlanData
			DROP PROCEDURE UpdatePlanData
			DROP PROCEDURE UpdateTraceData
			DROP PROCEDURE UpdateAttributeData
			DROP PROCEDURE UpdateEventData
			DROP PROCEDURE UpdateContextInstanceData
			DROP PROCEDURE UpdateAlarmData
			DROP PROCEDURE InsertTraceData
			DROP PROCEDURE GetTraceData
			DROP PROCEDURE InsertAttributeData
			DROP PROCEDURE InsertAlarmData
			DROP PROCEDURE InsertEventData
			DROP PROCEDURE InsertContextInstanceData
			DROP PROCEDURE GetEventData
			DROP PROCEDURE DeleteTraceData
			DROP PROCEDURE GetAttributeData
			DROP PROCEDURE GetContextInstanceData
			DROP PROCEDURE GetAlarmData
			DROP PROCEDURE DeleteEventData
			DROP PROCEDURE DeleteContextInstanceData
			DROP PROCEDURE DeleteAlarmData
			DROP PROCEDURE DeleteAttributeData
			DROP PROCEDURE DeleteAlarmValueData
			DROP PROCEDURE DeleteAttributeValueData
			DROP PROCEDURE DeleteTraceValueData
			DROP PROCEDURE DeleteEventValueData
			DROP PROCEDURE GetAttributeValueData
			DROP PROCEDURE GetAlarmValueData
			DROP PROCEDURE GetTraceValueData
			DROP PROCEDURE GetEventValueData
			DROP PROCEDURE InsertAttributeValueData
			DROP PROCEDURE InsertEventValueData
			DROP PROCEDURE InsertAlarmValueData
			DROP PROCEDURE InsertTraceValueData
			DROP PROCEDURE UpdateAttributeValueData
			DROP PROCEDURE UpdateEventValueData
			DROP PROCEDURE UpdateTraceValueData
			DROP PROCEDURE UpdateAlarmValueData
			DROP PROCEDURE UpdateAlarmValueContextInstanceData
			DROP PROCEDURE UpdateAttributeValueContextInstanceData
			DROP PROCEDURE UpdateEventValueContextInstanceData
		END
		--	End droppping of stored procedure
		
		-- add the ContextInstanceDataID columns
		ALTER TABLE [AlarmData] ADD ContextInstanceDataID INT NULL;		
		ALTER TABLE [AttributeData] ADD ContextInstanceDataID INT NULL;
		ALTER TABLE [EventData] ADD ContextInstanceDataID INT NULL;
		--  Currently not supporting ContextInstanceDataID tracking in the TraceData.
		--  TraceData is still retrieved by time.
		--	ALTER TABLE [TraceData] ADD ContextInstanceDataID INT NULL;
		
		-- add the RetentionType to that PlanData table	
		ALTER TABLE [PlanData] ADD RetentionType INT NULL;

		-- Delete the UserData Table and all FK relations 
		DECLARE @foreignKeys VARCHAR(300); 

		-- ALTER TABLE DROP CONSTRAINT  
		DECLARE cur CURSOR FOR
			 SELECT 'ALTER TABLE ' + OBJECT_SCHEMA_NAME(parent_object_id) + '.' + OBJECT_NAME(parent_object_id) +
							' DROP CONSTRAINT ' + name
			 FROM sys.foreign_keys 
			 WHERE OBJECT_SCHEMA_NAME(referenced_object_id) = 'dbo' AND 
						OBJECT_NAME(referenced_object_id) = 'UserData';
 
		OPEN cur;
		FETCH cur INTO @foreignKeys;
 
		-- Drop each foreign key constraint 
		WHILE @@FETCH_STATUS = 0
			BEGIN
			   EXEC (@foreignKeys);
			   FETCH cur INTO @foreignKeys;
			END
 
		CLOSE cur;
		DEALLOCATE cur;
 
		DROP TABLE UserData
		
		-- remove all indexes as they are being updated
		DECLARE @indexName VARCHAR(256)
		DECLARE @tableName VARCHAR(256)

		DECLARE [indexes] CURSOR FOR

		SELECT  i.name as [Index], OBJECT_NAME(i.id) as [Table]
		FROM sysindexes i
		WHERE i.indid between 1 and 249
			and INDEXPROPERTY(i.id, i.name, 'IsClustered') = 0 
			and INDEXPROPERTY(i.id, i.name, 'IsStatistics') = 0
			and INDEXPROPERTY(i.id, i.name, 'IsAutoStatistics') = 0
			and INDEXPROPERTY(i.id, i.name, 'IsHypothetical') = 0
			and OBJECTPROPERTY(i.id,'IsMSShipped') = 0
			and not exists (select * 
				from sysobjects o 
				where xtype = 'UQ' 
				and o.name = i.name )        

		OPEN [indexes]

		FETCH NEXT FROM [indexes] INTO @indexName, @tableName

		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT 'DROP INDEX [' + @indexName + '] ON [' + @tableName + ']'
			Exec ('DROP INDEX [' + @indexName + '] ON [' + @tableName + ']')

			FETCH NEXT FROM [indexes] INTO @indexName, @tableName
		END
	
		CLOSE           [indexes]
		DEALLOCATE      [indexes]
	
		-- remove Audit columns from the database tables		
		ALTER TABLE [AlarmIdentifierData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		ALTER TABLE [AlarmValueContextInstanceData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [AlarmValueData] DROP CONSTRAINT [DF_AlarmValueData_DateCreated];
		ALTER TABLE [AlarmValueData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [AlarmData] DROP CONSTRAINT [DF_AlarmData_DateCreated];
		ALTER TABLE [AlarmData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;			
									
		ALTER TABLE [AttributeValueContextInstanceData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		ALTER TABLE [AttributeValueData] DROP CONSTRAINT [DF_AttributeValueData_DateCreated];
		ALTER TABLE [AttributeValueData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [AttributeData] DROP CONSTRAINT [DF_AttributeData_DateCreated];
		ALTER TABLE [AttributeData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;			

		ALTER TABLE [EventIdentifierData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		ALTER TABLE [EventValueContextInstanceData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [EventValueData] DROP CONSTRAINT [DF_EventValueData_DateCreated];
		ALTER TABLE [EventValueData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [EventData] DROP CONSTRAINT [DF_EventData_DateCreated];
		ALTER TABLE [EventData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
			
		ALTER TABLE [TraceValueData] DROP CONSTRAINT [DF_TraceValueData_DateCreated];
		ALTER TABLE [TraceValueData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [TraceData] DROP CONSTRAINT [DF_TraceData_DateCreated];
		ALTER TABLE [TraceData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [ValueIdentifierData] DROP CONSTRAINT [DF_ValueIdentifierData_DateCreated];
		ALTER TABLE [ValueIdentifierData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		--Adding an index to ParamaterName need to adjust to support 900 bytes index size limit
		UPDATE [ValueIdentifierData] SET ParameterName = LEFT(ParameterName,450)
		ALTER TABLE [ValueIdentifierData] ALTER COLUMN ParameterName NVARCHAR(450)

		
		ALTER TABLE [SerializedTypeData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		ALTER TABLE [PlanDetailData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;
		
		ALTER TABLE [PlanData] DROP CONSTRAINT [DF_PlanData_DateCreated];
		ALTER TABLE [PlanData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;

		ALTER TABLE [ContextInstanceData] DROP CONSTRAINT [DF_ContextData_DateCreated];
		ALTER TABLE [ContextInstanceData] DROP COLUMN IsPermanent, DateCreated, UserCreatedID, DateModified, UserModifiedID;

		--Adding new datatype SByte to supported datatypes table
		INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (10, N'sbyte', N'SByte')
	END
	
	-- END DB Upgrade
	COMMIT
END TRY
BEGIN CATCH
  	IF @@TRANCOUNT > 0
		ROLLBACK

    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, 
               @ErrorSeverity, 
               @ErrorState 
               );
END CATCH;
-- END of the 1st pass upgrade to 7.7 Schema Updates - need to execute the next set in another transaction
GO


BEGIN TRY

	-- Enable Snapshot isolation if necessary.  
	-- Note that dynamic SQL is being used here in order to use the current DB_NAME, and that snapshot isolation cannot be enabled within a transaction. (Hence why it's not enabled below.)
	DECLARE @CHECK_SNAPSHOT_ISOLATION_SQL NVARCHAR(300)
	DECLARE @CHECK_SNAPSHOT_ISOLATION NVARCHAR(5)

	-- Build a dynamic query to check if snapshot isolation is enabled
	SET @CHECK_SNAPSHOT_ISOLATION_SQL = 'SELECT @CHECK_SNAPSHOT_ISOLATION=SNAPSHOT_ISOLATION_STATE_DESC from sys.databases WHERE NAME = ''' + 
		REPLACE(REPLACE(QUOTENAME(DB_NAME()), '[', ''), ']', '') + ''''
	EXEC sp_executesql @CHECK_SNAPSHOT_ISOLATION_SQL, N'@CHECK_SNAPSHOT_ISOLATION varchar(5) output', @CHECK_SNAPSHOT_ISOLATION output

	-- if snapshot isolation is not enabled, enable it
	IF (@CHECK_SNAPSHOT_ISOLATION <> 'ON')
	BEGIN
		DECLARE @ALTER_DB_SNAPSHOT_ISOLATION_SQL VARCHAR(300)
		SET @ALTER_DB_SNAPSHOT_ISOLATION_SQL = 'ALTER DATABASE ' + QUOTENAME(DB_NAME()) + 'SET ALLOW_SNAPSHOT_ISOLATION ON'
		EXEC (@ALTER_DB_SNAPSHOT_ISOLATION_SQL)
	END     

	-- BEGIN 7.7 Final Pass
BEGIN TRANSACTION

	DECLARE @TargetVersionNumber NVARCHAR(20) = '7.7.0.0'
	DECLARE @TargetVersionName NVARCHAR(100) = 'EIB 7.7' 
	DECLARE @PreviousVersionNumber NVARCHAR(20) = '7.6.2.0'

-- BEGIN 7.7 Data Translations
	UPDATE [PlanData] SET RetentionType = 2; --All older plan data have a daily retention type
	 ALTER TABLE [PlanData] ALTER COLUMN RetentionType INT NOT NULL

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

		-- rebuild the performance indexes with new names prior to apply the logic that merges context instance data
		CREATE NONCLUSTERED INDEX [EventData200000000_PD_ET_ED_EID] ON [dbo].[EventData] 
		(
			  [PlanDataID] ASC,
			  [EventTime] ASC,
			  [EventDataID] ASC,
			  [EventIdentifierDataID] ASC
		)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [EventData200000000_EID_ET_ED_PD] ON [dbo].[EventData] 
		(
			  [EventIdentifierDataID] ASC,
			  [EventTime] ASC,
			  [EventDataID] ASC,
			  [PlanDataID] ASC
		)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

				
		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VI]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE ([ValueInt])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
			
		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VF]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE ([ValueFloat])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VB]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE (ValueBit)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VS]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE (ValueString)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
				
		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VX]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE (ValueXml)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_ED_EVD_VID_VD]  ON [dbo].[EventValueData]		
		(	
			[EventDataID] ASC,
			[EventValueDataID] ASC, 
			[ValueIdentifierDataID] ASC
		) INCLUDE (ValueDate)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_EventValueData200000000_VID_ED]  ON [dbo].[EventValueData]		
		(	
			[ValueIdentifierDataID] ASC,
			[EventDataID] ASC
		) 
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [TraceData200000000_PD_RT_TD_CT] ON [dbo].[TraceData] 
		(
			[PlanDataID] ASC,
			[ReportTime] ASC,
			[TraceDataID] ASC,
			[CollectionTime] ASC
		)
		INCLUDE ( [TraceID] ) 
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_TraceData200000000_RT_CT_PD_TD] ON [dbo].[TraceData] 
		(
			[ReportTime] DESC, 
			[CollectionTime] DESC, 
			[PlanDataID] ASC,	
			[TraceDataID] ASC
		)
		INCLUDE ( [TraceID] ) 
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_TraceValueData200000000_TD_VALUES] ON [dbo].[TraceValueData]		
		(
			[TraceDataID] ASC
		) INCLUDE ([TraceValueDataID], [ValueDataTypeID], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_TraceValueData200000000_TD] ON [dbo].[TraceValueData]		
		(
			[TraceDataID] ASC
		) INCLUDE ([TraceValueDataID])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_TraceValueData200000000_VID_TD] ON [dbo].[TraceValueData]		
		(
			[ValueIdentifierDataID] ASC,
			[TraceDataID] ASC
		)
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_AttributeValueData200000000_TD] ON [dbo].[AttributeValueData]		
		(
			[AttributeDataID] ASC
		) INCLUDE ([AttributeValueDataID])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_AlarmValueData200000000_TD] ON [dbo].[AlarmValueData]		
		(
			[AlarmDataID] ASC
		) INCLUDE ([AlarmValueDataID])
		WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
		
		-- 

		-- create new time based indexes which are used by the purge stored procedure.
		CREATE NONCLUSTERED INDEX [NC_IDX_ContextInstanceData200000000_CT] ON [dbo].[ContextInstanceData] 
		(              
		   [ContextTime]
		) INCLUDE ([ContextInstanceDataID],[PlanDataID])
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_TraceData200000000_CT] ON [dbo].[TraceData] 
		(              
		   [CollectionTime]
		) INCLUDE ([TraceDataID],[PlanDataID])
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_AttributeData200000000_AT] ON [dbo].[AttributeData] 
		(              
		   [AttributeTime]
		) INCLUDE ([AttributeDataID],[PlanDataID])
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_AlarmData200000000_ET] ON [dbo].[AlarmData] 
		(              
		   [ExceptionTime]
		) INCLUDE ([AlarmDataID],[PlanDataID])
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [NC_IDX_EventData200000000_ET] ON [dbo].[EventData] 
		(              
		   [EventTime]
		) INCLUDE ([EventDataID],[PlanDataID])
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

		/****** New ValueIdentifier Index to aid in pararmeter based queries   ******/
		CREATE NONCLUSTERED INDEX [NC_IDX_ValueIdentifierData_PN] ON [dbo].[ValueIdentifierData]
		(
			[ParameterName] ASC
		) INCLUDE ([ValueIdentifierDataID]) 
		WITH (SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
		
		-- End of Indexes

		-- update context instance data id's from the legacy "ValueContextInstanceData" tables
		UPDATE [EventData] 
			SET [EventData].ContextInstanceDataID = legev.ContextInstanceDataID
				FROM (SELECT  ed.[EventDataID]      
					,evcid.[ContextInstanceDataID]
					FROM [EventData] ed
					JOIN EventValueData evd on ed.EventDataID = evd.EventDataID
					JOIN EventValueContextInstanceData evcid on evd.EventValueDataID = evcid.EventValueDataID) as legev 
				WHERE EventData.EventDataID = legev.EventDataID

		UPDATE [AlarmData] 
			SET [AlarmData].ContextInstanceDataID = legav.ContextInstanceDataID
				FROM (SELECT  ad.[AlarmDataID]      
					,avcid.[ContextInstanceDataID]
					FROM [AlarmData] ad
					JOIN AlarmValueData avd on ad.AlarmDataID = avd.AlarmDataID
					JOIN AlarmValueContextInstanceData avcid on avd.AlarmValueDataID = avcid.AlarmValueDataID) as legav 
				WHERE AlarmData.AlarmDataID = legav.AlarmDataID

		UPDATE [AttributeData] 
			SET [AttributeData].ContextInstanceDataID = legav.ContextInstanceDataID
				FROM (SELECT  ad.[AttributeDataID]      
					,avcid.[ContextInstanceDataID]
					FROM [AttributeData] ad
					JOIN AttributeValueData avd on ad.AttributeDataID = avd.AttributeDataID
					JOIN AttributeValueContextInstanceData avcid on avd.AttributeValueDataID = avcid.AttributeValueDataID) as legav 
				WHERE [AttributeData].AttributeDataID = legav.AttributeDataID					   

		-- Rename tables
		EXEC sp_rename 'AlarmData', 'AlarmData200000000';					
		EXEC sp_rename 'AlarmValueContextInstanceData', 'AlarmValueContextInstanceData200000000';
		EXEC sp_rename 'AlarmValueData', 'AlarmValueData200000000';
		EXEC sp_rename 'AttributeData', 'AttributeData200000000';			
		EXEC sp_rename 'AttributeValueContextInstanceData', 'AttributeValueContextInstanceData200000000';
		EXEC sp_rename 'AttributeValueData', 'AttributeValueData200000000';
		EXEC sp_rename 'ContextInstanceData', 'ContextInstanceData200000000';			
		EXEC sp_rename 'EventData', 'EventData200000000';			
		EXEC sp_rename 'EventValueContextInstanceData', 'EventValueContextInstanceData200000000';
		EXEC sp_rename 'EventValueData', 'EventValueData200000000';						
		EXEC sp_rename 'TraceData', 'TraceData200000000';
		EXEC sp_rename 'TraceValueData', 'TraceValueData200000000';
		-- complete renaming of existing tables
		
		-- add view for legacy context instance value datas

		-- this view is still up for debate as to whether the data should be migrated into the ContextInstanceValueData2000000
		-- table of whether this view is sufficient.
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContextInstanceValueData200000000]') AND type in (N'V'))
		DROP VIEW [dbo].ContextInstanceValueData200000000
		EXEC('
		CREATE VIEW ContextInstanceValueData200000000 AS
		(SELECT * FROM
		(SELECT avd.[AlarmValueDataID] as ContextInstanceValueDataID
			   ,[ContextInstanceDataID] as ContextInstanceDataID
			  ,avd.[ValueDataTypeID] as ValueDataTypeID
			  ,avd.[ValueIdentifierDataID] as ValueIdentifierDataID
			  ,avd.[ValueInt] as ValueInt
			  ,avd.[ValueFloat] as ValueFloat
			  ,avd.[ValueBit] as ValueBit
			  ,avd.[ValueString] as ValueString
		  FROM  [AlarmValueContextInstanceData200000000] avcid
			 JOIN AlarmValueData200000000 avd ON avcid.AlarmValueDataID = avd.AlarmValueDataID	
		) AV

		UNION ALL
		SELECT * FROM
		(SELECT evd.[EventValueDataID] as ContextInstanceValueDataID
			  ,[ContextInstanceDataID] as ContextInstanceDataID
			  ,evd.[ValueDataTypeID] as ValueDataTypeID
			  ,evd.[ValueIdentifierDataID] as ValueIdentifierDataID
			  ,evd.[ValueInt] as ValueInt
			  ,evd.[ValueFloat] as ValueFloat
			  ,evd.[ValueBit] as ValueBit
			  ,evd.[ValueString] as ValueString
		  FROM  [EventValueContextInstanceData200000000] evcid
			JOIN EventValueData200000000 evd ON evcid.EventValueDataID = evd.EventValueDataID	
		) EV
		UNION ALL 
		SELECT * FROM
		(SELECT avd.[AttributeValueDataID] as ContextInstanceValueDataID
		      ,[ContextInstanceDataID] as ContextInstanceDataID
			  ,avd.[ValueDataTypeID] as ValueDataTypeID
			  ,avd.[ValueIdentifierDataID] as ValueIdentifierDataID
			  ,avd.[ValueInt] as ValueInt
			  ,avd.[ValueFloat] as ValueFloat
			  ,avd.[ValueBit] as ValueBit
			  ,avd.[ValueString] as ValueString
		  FROM  [AttributeValueContextInstanceData200000000] atvcid
			JOIN AttributeValueData200000000 avd ON atvcid.AttributeValueDataID = avd.AttributeValueDataID	
		  ) AT
		)')

		-- creation of views that encompass the old tables is handled by
		-- the ManagePartitions stored procedure.

		-- update Purge Stored Procedure to use the specific data timestamps and not the
		-- DateCreated column
		IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeEIBStorageData]') AND type in (N'P', N'PC'))
		DROP PROCEDURE [dbo].[PurgeEIBStorageData]
		EXEC('----------------------------------------------------------------------
		-- $Workfile: EIBStorage.sql
		-- Summary: EIBStorage Stored Procedures
		-- Project: EIB 
		--
		-- Description:
		-- Purges data from the EIBStorage database that is older that the specified number of days
		--
		-- Copyright(C) The PEER Group Inc., 2013 - 2017.
		--
		-- This software contains confidential and trade secret information
		-- belonging to The PEER Group Inc. All Rights Reserved.
		--
		-- No part of this software may be reproduced or transmitted in any form
		-- or by any means, electronic, mechanical, photocopying, recording or
		-- otherwise, without the prior written consent of The PEER Group Inc.
		--
		-- Revision History:
		--
		-- $Log: 1.0 - Initial Creation
		--       7.6 SP2 - Updated the purge to disable indexes while purge is executing
		--                 and re-worked to improve performance.
		--	     7.7 - Updated the purge to remove the use of DateCreated, updated to use legacy table names
		-----------------------------------------------------------------------
		CREATE PROC [dbo].[PurgeEIBStorageData] @InterfaceName NVARCHAR(255)
			,@DataConsumerName NVARCHAR(255)
			,@DaysOfDataToKeep INT
		AS
		SET XACT_ABORT ON;
		SET DEADLOCK_PRIORITY LOW;

		DECLARE @BatchSize INT = 100000
		DECLARE @DelayDuration VARCHAR(10) = ''00:00:00.020''

		BEGIN TRY
			DECLARE @PurgeThresholdDate DATETIME = DATEADD(DAY, - @DaysOfDataToKeep, GETUTCDATE())
			DECLARE @RecordsDeleted INT = 0
			DECLARE @Rows INT = 0

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) avcid
				FROM AlarmValueContextInstanceData200000000 avcid WITH (ROWLOCK)
				INNER JOIN AlarmValueData200000000 avd WITH (ROWLOCK) ON avd.AlarmValueDataID = avcid.AlarmValueDataID 
				INNER JOIN AlarmData200000000 ad WITH (ROWLOCK) ON ad.AlarmDataID = avd.AlarmDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.ExceptionTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) avcid
				FROM AttributeValueContextInstanceData200000000 avcid WITH (ROWLOCK)
				INNER JOIN AttributeValueData200000000 avd WITH (ROWLOCK) ON avd.AttributeValueDataID = avcid.AttributeValueDataID
				INNER JOIN AttributeData200000000 ad WITH (ROWLOCK) ON ad.AttributeDataID = avd.AttributeDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.AttributeTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) evcid
				FROM EventValueContextInstanceData200000000 evcid WITH (ROWLOCK)
				INNER JOIN EventValueData200000000 evd WITH (ROWLOCK) ON evd.EventValueDataID = evcid.EventValueDataID
				INNER JOIN EventData200000000 ed WITH (ROWLOCK) ON ed.EventDataID = evd.EventDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ed.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ed.EventTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT	
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END
	
			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) avd
				FROM AlarmValueData200000000 avd WITH (ROWLOCK)
				INNER JOIN AlarmData200000000 ad WITH (ROWLOCK) ON ad.AlarmDataID = avd.AlarmDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.ExceptionTime < @PurgeThresholdDate
			
				SET @Rows = @@ROWCOUNT	
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) avd
				FROM AttributeValueData200000000 avd WITH (ROWLOCK)
				INNER JOIN AttributeData200000000 ad WITH (ROWLOCK) ON ad.AttributeDataID = avd.AttributeDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.AttributeTime < @PurgeThresholdDate
	
				SET @Rows = @@ROWCOUNT	
				SET @RecordsDeleted = @RecordsDeleted + @Rows
	
				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) evd
				FROM EventValueData200000000 evd WITH (ROWLOCK)
				INNER JOIN EventData200000000 ed WITH (ROWLOCK) ON ed.EventDataID = evd.EventDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ed.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ed.EventTime < @PurgeThresholdDate
			
				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END	

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) tvd
				FROM TraceValueData200000000 tvd WITH (ROWLOCK)
				INNER JOIN TraceData200000000 td WITH (ROWLOCK) ON td.TraceDataID = tvd.TraceDataID
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = td.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND td.CollectionTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) ad
				FROM AlarmData200000000 ad WITH (ROWLOCK)
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.ExceptionTime < @PurgeThresholdDate
			
				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) ad
				FROM AttributeData200000000 ad WITH (ROWLOCK)
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ad.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ad.AttributeTime < @PurgeThresholdDate
		
				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) ed
				FROM EventData200000000 ed WITH (ROWLOCK)
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = ed.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND ed.EventTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END
	
			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) td
				FROM TraceData200000000 td 
				INNER JOIN PlanData pd WITH (ROWLOCK) ON pd.PlanDataID = td.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND td.CollectionTime < @PurgeThresholdDate

				SET @Rows = @@ROWCOUNT
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			WHILE 1=1
			BEGIN
				DELETE TOP (@BatchSize) cid
				FROM ContextInstanceData200000000 cid WITH (ROWLOCK)
				INNER JOIN PlanData pd  ON pd.PlanDataID = cid.PlanDataID
				WHERE (
						(pd.InterfaceName = @InterfaceName)
						OR (@InterfaceName IS NULL)
						)
					AND (
						(pd.DataConsumerName = @DataConsumerName)
						OR (@DataConsumerName IS NULL)
						)
					AND cid.ContextTime < @PurgeThresholdDate		

				SET @Rows = @@ROWCOUNT	
				SET @RecordsDeleted = @RecordsDeleted + @Rows

				WAITFOR DELAY @DelayDuration

				IF (@Rows < @BatchSize)
					BREAK;
			END

			SELECT @RecordsDeleted
		END TRY

		BEGIN CATCH
			DECLARE @ERROR_SEVERITY INT
				,@ERROR_STATE INT
				,@ERROR_NUMBER INT
				,@ERROR_LINE INT
				,@ERROR_MESSAGE NVARCHAR(4000)

		SELECT @ERROR_SEVERITY = ERROR_SEVERITY()
				,@ERROR_STATE = ERROR_STATE()
				,@ERROR_NUMBER = ERROR_NUMBER()	
				,@ERROR_LINE = ERROR_LINE()
				,@ERROR_MESSAGE = ERROR_MESSAGE()

			RAISERROR (
					''Msg %d, Line %d, :%s''
					,@ERROR_SEVERITY
					,@ERROR_STATE
					,@ERROR_NUMBER
					,@ERROR_LINE
					,@ERROR_MESSAGE
					)
		END CATCH')	

	DECLARE @sql nvarchar(max)
	-- BEGIN CREATE CONTEXTINSTANCEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceData]'))
		DROP VIEW [ContextInstanceData]

	SET @sql = 'CREATE VIEW ContextInstanceData AS SELECT * FROM ContextInstanceData200000000'	
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [ContextInstanceData] TO db_eibstorage
	-- END CREATE CONTEXTINSTANCEDATA VIEW

	-- BEGIN CREATE CONTEXTINSTANCEVALUEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceValueData]'))
		DROP VIEW [ContextInstanceValueData]

	SET @sql = 'CREATE VIEW ContextInstanceValueData AS SELECT * FROM ContextInstanceValueData200000000'	
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [ContextInstanceValueData] TO db_eibstorage
	-- END CREATE CONTEXTINSTANCEVALUEDATA VIEW
		
	-- BEGIN CREATE TRACEVALUEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[TraceValueData]'))
		DROP VIEW [TraceValueData]

	SET @sql = 'CREATE VIEW TraceValueData AS SELECT * FROM TraceValueData200000000'
	EXEC sp_executesql @SQL

	GRANT SELECT ON [TraceValueData] TO db_eibstorage
	-- END CREATE TRACEVALUEDATA VIEW

	-- BEGIN CREATE TRACEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[TraceData]'))
		DROP VIEW [TraceData]

	SET @sql = 'CREATE VIEW TraceData AS  SELECT * FROM TraceData200000000'
	EXEC sp_executesql @SQL

	GRANT SELECT ON [TraceData] TO db_eibstorage
	-- END CREATE TRACEDATA VIEW

	-- BEGIN ALARMVALUEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AlarmValueData]'))
		DROP VIEW [AlarmValueData]

	SET @sql = 'CREATE VIEW AlarmValueData AS SELECT * FROM AlarmValueData200000000'
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [AlarmValueData] TO db_eibstorage
	-- END CREATE ALARMVALUEDATA VIEW

	-- BEGIN ALARMDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AlarmData]'))
		DROP VIEW [AlarmData]

	SET @sql = 'CREATE VIEW AlarmData AS SELECT * FROM AlarmData200000000'
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [AlarmData] TO db_eibstorage
	-- END CREATE ALARMDATA VIEW	

	-- BEGIN ATTRIBUTEVALUEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AttributeValueData]'))
		DROP VIEW [AttributeValueData]

	SET @sql = 'CREATE VIEW AttributeValueData AS SELECT * FROM AttributeValueData200000000'
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [AttributeValueData] TO db_eibstorage
	-- END CREATE ATTRIBUTEVALUEDATA VIEW

	-- BEGIN ATTRIBUTEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AttributeData]'))
		DROP VIEW [AttributeData]

	SET @sql = 'CREATE VIEW AttributeData AS SELECT * FROM AttributeData200000000'
	EXEC sp_executesql @SQL

	GRANT SELECT ON [AttributeData] TO db_eibstorage
	-- END CREATE ATTRIBUTEDATA VIEW				

	-- BEGIN EVENTVALUEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[EventValueData]'))
		DROP VIEW [EventValueData]

	SET @sql = 'CREATE VIEW EventValueData AS SELECT * FROM EventValueData200000000'
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [EventValueData] TO db_eibstorage
	-- END CREATE EVENTVALUEDATA VIEW

	-- BEGIN EVENTDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[EventData]'))
		DROP VIEW [EventData]

	SET @sql = 'CREATE VIEW EventData AS SELECT * FROM EventData200000000'
	EXEC sp_executesql @SQL
	
	GRANT SELECT ON [EventData] TO db_eibstorage
	-- END EVENTDATA VIEW

	-- REBUILD INDEXES

	IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MaintainIndexes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
		DROP PROCEDURE [dbo].[MaintainIndexes]

	EXEC('-- ---------------------------------------------------------------------------------------
--
-- Name:
--         MaintainIndexes
--
-- Description:
--         Defragments fragmented indexes
--
--
--
-- Copyright(C) The PEER Group Inc., 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- PEER®, PEER Group® and PEERFactory® are registered trademarks
-- of The PEER Group Inc. in the United States and Canada.com
--
-- ---------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[MaintainIndexes] 
	@maxfrag FLOAT = 30.0, 	
	@indexCount INT = 20,
	@partitionID INT = 0,	
	@updateStats BIT = 1
AS
BEGIN
	SET NOCOUNT ON
	SET DEADLOCK_PRIORITY -10

	--Declare Variables
	DECLARE @storeTable TABLE (TableName nvarchar(1000), IndexName nvarchar(1000))

	DECLARE	@sql NVARCHAR(4000),
			@tableName NVARCHAR(200),
			@indextName NVARCHAR(200),
			@ret INT = 0,
			@appLock INT = 0,
			@lockName NVARCHAR(200) = ''MaintainIndexes'' + CAST(@partitionID as NVARCHAR(10))

	EXEC @appLock = sp_getapplock
		@Resource = @lockName,
		@LockMode = ''Exclusive'',
		@LockOwner = ''Session'',
		@LockTimeout = 30000

	IF @appLock >= 0
	BEGIN
		BEGIN TRY

		INSERT INTO @storeTable (TableName, IndexName)
		SELECT TOP (@indexCount)
			t.name AS TableName,
			idx.name AS IndexName
		FROM 
			sys.indexes idx 
		INNER JOIN 
			sys.tables t ON idx.object_id = t.object_id 
		INNER JOIN
		sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, ''LIMITED'') ips ON idx.object_id = ips.object_id
		WHERE 
			idx.is_primary_key = 0 AND idx.is_unique = 0 AND idx.is_unique_constraint = 0 AND t.is_ms_shipped = 0 
			AND t.name LIKE ''%'' + (CASE WHEN @partitionID = 0 THEN '''' ELSE CAST(@partitionID AS NVARCHAR) END) AND ips.avg_fragmentation_in_percent > @maxfrag
		ORDER BY 
			ips.avg_fragmentation_in_percent DESC;

		WHILE (SELECT COUNT(*) FROM @storeTable) > 0
		BEGIN
			SELECT TOP 1 @tableName = TableName, @indextName = IndexName
			FROM @storeTable

			SET @sql = N''ALTER INDEX '' + @indextName + N'' ON '' + @tableName + N'' REORGANIZE''
			PRINT @sql;
			BEGIN TRY
				EXEC (@sql)

				SET @sql = N''UPDATE STATISTICS '' + @tableName
				-- Update stats if required
				IF @updateStats = 1
					EXEC (@sql)

				SET @ret = @ret + 1;
			END TRY
			BEGIN CATCH
			
			END CATCH

			DELETE FROM @storeTable
			WHERE TableName = @tableName AND IndexName = @indextName

		END

		EXEC @appLock = sp_releaseapplock @Resource=@lockName, @LockOwner=''Session'';

		--Return the index count successfully rebuilt
		SELECT @ret AS ReturnValue
		
		END TRY
		BEGIN CATCH
			EXEC @appLock = sp_releaseapplock @Resource=@lockName, @LockOwner=''Session'';
			DECLARE @ErrorMessage NVARCHAR(400);
			DECLARE @ErrorNumber INT;
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			DECLARE @ErrorLine INT;

			SELECT @ErrorMessage = ERROR_MESSAGE();
			SELECT @ErrorNumber = ERROR_NUMBER();
			SELECT @ErrorSeverity = ERROR_SEVERITY();
			SELECT @ErrorState = ERROR_STATE();
			SELECT @ErrorLine = ERROR_LINE();
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber)
		END CATCH	
	END
END
')

	-- END REBUILD INDEXES

	END

	COMMIT

	BEGIN TRANSACTION

-- BEGIN 7.7 Index maintenance

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

		-- Update Version to EIB 7.7
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()	
	
	
	EXEC [dbo].[MaintainIndexes] @maxfrag = 0, @indexCount = 200, @partitionID = 200000000, @updateStats = 1

	END

	COMMIT

	-- END 7.7

	-- BEGIN 7.8

	BEGIN TRANSACTION

	SET @TargetVersionNumber = '7.8.0.0'
	SET @TargetVersionName = 'EIB 7.8'
	SET @PreviousVersionNumber = '7.7.0.0'

	IF (SELECT VersionNumber FROM VersionData) = @PreviousVersionNumber
	BEGIN

		INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (11, N'localdate', N'LocalDate')
		INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (12, N'gmtDate', N'GMTDate')

	-- Update Version
	UPDATE VersionData SET
		VersionNumber = @TargetVersionNumber,
		VersionName = @TargetVersionName,
		PreviousVersionNumber = @PreviousVersionNumber,
		DateUpgraded = SYSDATETIMEOFFSET()

	END

	COMMIT

	-- END 7.8

END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK

    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, 
               @ErrorSeverity, 
               @ErrorState 
               );
END CATCH
GO -- do not add/remove the existing "GO" statements, they are used to break the upgrade into multiple steps.