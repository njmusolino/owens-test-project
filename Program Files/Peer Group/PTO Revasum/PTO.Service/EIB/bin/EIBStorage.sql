﻿----------------------------------------------------------------------
-- $Workfile: EIBStorage.sql
-- Summary: Script that creates the EIBStorage database schema
-- Project: EIB 7.1
--
-- Description:
-- Script that creates the EIBStorage database schema
--
-- $Author: Alex Dlugokecki
-- $Date: 2014-05-08
-- $Revision: 1.1
--
-- Copyright(C) The PEER Group Inc., 2014 - 2016.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 7.0 - 2013-07-17 - EIB 7.0, Initial Creation
--       7.1 - 2014-05-08 - EIB 7.1, Trace Data Performance Indexes
--       7.2 - 2014-09-15 - EIB 7.2, Performance Indexes, Enabling Snapshot Isolation, Making Requested Exception Fields Nullable, Fix to GetEIBStorageDatabaseInfo stored proc
-- --------------------------------------------------------------------

-- Auto generated code starts here

/****** Object:  Table [dbo].[ValueDataType]    Script Date: 10/30/2014 13:13:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValueDataType](
    [ValueDataTypeID] int  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(50)  NOT NULL,
 CONSTRAINT [PK_ValueDataType] PRIMARY KEY CLUSTERED 
(
	[ValueDataTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (1, N'null', N'Null')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (2, N'int', N'Integer')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (3, N'byte', N'Byte')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (4, N'float', N'Float')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (5, N'double', N'Double')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (6, N'bit', N'Boolean')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (7, N'string', N'String')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (8, N'xml', N'XML Document')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (9, N'datetime', N'DateTime')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (10, N'sbyte', N'SByte')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (11, N'localdate', N'LocalDate')
INSERT [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description]) VALUES (12, N'gmtDate', N'GMTDate')

/****** Object:  Table [dbo].[VersionData]    Script Date: 10/30/2014 13:13:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VersionData](
    [VersionDataID] int IDENTITY(1,1) NOT NULL,
    [VersionNumber] nvarchar(20)  NOT NULL,
    [VersionName] nvarchar(100)  NOT NULL,
    [PreviousVersionNumber] nvarchar(20)  NULL,
    [DateUpgraded] datetimeoffset  NULL,
 CONSTRAINT [PK_VersionData] PRIMARY KEY CLUSTERED 
(
	[VersionDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[VersionData] ON
INSERT [dbo].[VersionData] ([VersionDataID], [VersionNumber], [VersionName], [PreviousVersionNumber], [DateUpgraded]) VALUES (1, N'7.8.0.0', N'EIB 7.8', NULL, NULL)
SET IDENTITY_INSERT [dbo].[VersionData] OFF

/****** Object:  Table [dbo].[EventIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventIdentifierData](
    [EventIdentifierDataID] int IDENTITY(1,1) NOT NULL,
    [RootID] nvarchar(100)  NOT NULL,
    [SourceID] nvarchar(4000)  NOT NULL,
    [EventID] nvarchar(500)  NOT NULL,
 CONSTRAINT [PK_EventIdentifierData] PRIMARY KEY CLUSTERED 
(
	[EventIdentifierDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[AlarmIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
CREATE TABLE [dbo].[AlarmIdentifierData](
    [AlarmIdentifierDataID] int IDENTITY(1,1) NOT NULL,
    [RootID] nvarchar(100)  NOT NULL,
    [SourceID] nvarchar(4000)  NOT NULL,
    [ExceptionID] nvarchar(500)  NOT NULL,
    [RequestedSourceID] nvarchar(4000)  NULL,
    [RequestedExceptionID] nvarchar(500)  NULL,
 CONSTRAINT [PK_AlarmIdentifierData] PRIMARY KEY CLUSTERED 
(
	[AlarmIdentifierDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SerializedTypeData](
    [SerializedTypeDataID] int IDENTITY(1,1) NOT NULL,
    [SerializedType] nvarchar(255)  NOT NULL,
 CONSTRAINT [PK_SerializedTypeData] PRIMARY KEY CLUSTERED 
(
	[SerializedTypeDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[PlanData]    Script Date: 10/30/2014 13:13:55 ******/
CREATE TABLE [dbo].[PlanData](
    [PlanDataID] int IDENTITY(1,1) NOT NULL,
    [PlanDetailDataID] int  NOT NULL,
    [InterfaceName] nvarchar(255)  NOT NULL,
    [DataConsumerName] nvarchar(255)  NOT NULL,
    [PlanName] nvarchar(100)  NOT NULL,
    [PlanID] uniqueidentifier  NOT NULL,
    [PlanActivationDate] datetimeoffset  NULL,
    [PlanDeactivationDate] datetimeoffset  NULL,
    [DeactivationReason] nvarchar(255)  NULL,
    [Description] nvarchar(255)  NULL,
	[RetentionType] int NOT NULL,
 CONSTRAINT [PK_PlanData] PRIMARY KEY CLUSTERED 
(
	[PlanDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[PlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
CREATE TABLE [dbo].[PlanDetailData](
    [PlanDetailDataID] int IDENTITY(1,1) NOT NULL,
    [PlanDetail] [xml]  NOT NULL,
    [PlanDetailHash] nvarchar(50)  NOT NULL,
 CONSTRAINT [PK_PlanDetailData] PRIMARY KEY CLUSTERED 
(
	[PlanDetailDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_PlanDetailData] UNIQUE NONCLUSTERED 
(
	[PlanDetailHash] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  ForeignKey [FK_PlanData_PlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
ALTER TABLE [dbo].[PlanData]  WITH CHECK ADD  CONSTRAINT [FK_PlanData_PlanDetailData] FOREIGN KEY([PlanDetailDataID])
REFERENCES [dbo].[PlanDetailData] ([PlanDetailDataID])
GO
ALTER TABLE [dbo].[PlanData] CHECK CONSTRAINT [FK_PlanData_PlanDetailData]
GO

/****** Object:  Table [dbo].[ValueIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
CREATE TABLE [dbo].[ValueIdentifierData](
    [ValueIdentifierDataID] int IDENTITY(1,1) NOT NULL,
    [RootID] nvarchar(100)  NOT NULL,
    [SourceID] nvarchar(4000)  NOT NULL,
    [ParameterName] nvarchar(450)  NOT NULL,
 CONSTRAINT [PK_ValueIdentifierData] PRIMARY KEY CLUSTERED 
(
	[ValueIdentifierDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Index [NC_IDX_ValueIdentifierID_PN]    ******/
CREATE NONCLUSTERED INDEX [NC_IDX_ValueIdentifierData_PN] ON [dbo].[ValueIdentifierData]
(
	[ParameterName] ASC
)
INCLUDE ([ValueIdentifierDataID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-- Create db_eibstorage role and grant permissions
CREATE ROLE db_eibstorage
GO
GRANT SELECT, INSERT ON AlarmIdentifierData TO db_eibstorage
GO
GRANT SELECT, INSERT ON EventIdentifierData TO db_eibstorage
GO
GRANT SELECT, INSERT, UPDATE ON PlanData TO db_eibstorage
GO
GRANT SELECT, INSERT ON PlanDetailData TO db_eibstorage
GO
GRANT SELECT, INSERT ON SerializedTypeData TO db_eibstorage
GO
GRANT SELECT ON ValueDataType TO db_eibstorage
GO
GRANT SELECT, INSERT ON ValueIdentifierData TO db_eibstorage
GO
GRANT SELECT ON VersionData TO db_eibstorage
GO

DECLARE @sql nvarchar(max)
--BEGIN CREATE VIEWS - empty skeletons
-- BEGIN CREATE CONTEXTINSTANCEDATA VIEW
SET @sql = 'CREATE VIEW ContextInstanceData AS SELECT CAST(NULL AS bigint) AS ContextInstanceDataID, NULL as PlanDataID, CAST(NULL AS datetimeoffset(7)) AS ContextTime'
EXEC sp_executesql @SQL
	
GRANT SELECT ON [ContextInstanceData] TO db_eibstorage
-- END CREATE CONTEXTINSTANCEDATA VIEW

-- BEGIN CREATE CONTEXTINSTANCEVALUEDATA VIEW
SET @sql = 'CREATE VIEW ContextInstanceValueData AS SELECT NULL AS ContextInstanceValueDataID, CAST(NULL AS bigint) as ContextInstanceDataID, NULL AS ValueDataTypeID, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString'
EXEC sp_executesql @SQL
	
GRANT SELECT ON [ContextInstanceValueData] TO db_eibstorage
-- END CREATE CONTEXTINSTANCEVALUEDATA VIEW
	
-- BEGIN CREATE TRACEVALUEDATA VIEW
SET @sql = 'CREATE VIEW TraceValueData AS SELECT NULL AS TraceValueDataID, CAST(NULL AS bigint) as TraceDataID, NULL AS ValueDataTypeID, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID'
EXEC sp_executesql @SQL
	
GRANT SELECT ON [TraceValueData] TO db_eibstorage
-- END CREATE TRACEVALUEDATA VIEW

-- BEGIN CREATE TRACEDATA VIEW
SET @sql = 'CREATE VIEW TraceData AS SELECT CAST(NULL AS bigint) AS TraceDataID, NULL as PlanDataID, NULL AS TraceID, CAST(NULL AS datetimeoffset(7)) AS ReportTime, CAST(NULL AS datetimeoffset(7)) AS CollectionTime'
EXEC sp_executesql @SQL
		
GRANT SELECT ON [TraceData] TO db_eibstorage
-- END CREATE TRACEDATA VIEW

-- BEGIN ALARMVALUEDATA VIEW
SET @sql = 'CREATE VIEW AlarmValueData AS SELECT NULL AS AlarmValueDataID, CAST(NULL AS bigint) as AlarmDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID '
EXEC sp_executesql @SQL
	
GRANT SELECT ON [AlarmValueData] TO db_eibstorage
-- END CREATE ALARMVALUEDATA VIEW

-- BEGIN ALARMDATA VIEW
SET @sql = 'CREATE VIEW AlarmData AS SELECT CAST(NULL AS bigint) AS AlarmDataID, NULL as PlanDataID, NULL AS AlarmIdentifierDataID, CAST(NULL AS nvarchar(255)) AS Message, CAST(NULL AS datetimeoffset(7)) AS ExceptionTime, CAST(NULL AS nvarchar(50)) AS Severity, CAST(NULL AS nvarchar(50)) AS State, CAST(NULL AS nvarchar(100)) AS ExceptionType, CAST(NULL AS nvarchar(100)) AS RequestedExceptionType, CAST(NULL AS bit) AS Enabled, CAST(NULL AS bigint) AS ContextInstanceDataID'
EXEC sp_executesql @SQL
		
GRANT SELECT ON [AlarmData] TO db_eibstorage
-- END CREATE ALARMDATA VIEW	

-- BEGIN ATTRIBUTEVALUEDATA VIEW
SET @sql = 'CREATE VIEW AttributeValueData AS SELECT NULL AS AttributeValueDataID, CAST(NULL AS bigint) as AttributeDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, CAST(NULL AS bit) AS IsOldPrimaryValue, CAST(NULL AS bit) AS IsNewPrimaryValue, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID '
EXEC sp_executesql @SQL
	
GRANT SELECT ON [AttributeValueData] TO db_eibstorage
-- END CREATE ATTRIBUTEVALUEDATA VIEW

-- BEGIN ATTRIBUTEDATA VIEW
SET @sql = 'CREATE VIEW AttributeData AS SELECT CAST(NULL AS bigint) AS AttributeDataID, NULL as PlanDataID, CAST(NULL AS datetimeoffset(7)) AS AttributeTime, CAST(NULL AS bigint) AS ContextInstanceDataID '
EXEC sp_executesql @SQL
	
GRANT SELECT ON [AttributeData] TO db_eibstorage
-- END CREATE ATTRIBUTEDATA VIEW				

-- BEGIN EVENTVALUEDATA VIEW
SET @sql = 'CREATE VIEW EventValueData AS SELECT NULL AS EventValueDataID,  CAST(NULL AS bigint) as EventDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID '
EXEC sp_executesql @SQL
	
GRANT SELECT ON [EventValueData] TO db_eibstorage
-- END CREATE EVENTVALUEDATA VIEW

-- BEGIN EVENTDATA VIEW
SET @sql = 'CREATE VIEW EventData AS SELECT CAST(NULL AS bigint) AS EventDataID, NULL as PlanDataID, NULL AS EventIdentifierDataID, CAST(NULL AS datetimeoffset(7)) AS EventTime, CAST(NULL AS bigint) AS ContextInstanceDataID '
EXEC sp_executesql @SQL
	
GRANT SELECT ON [EventData] TO db_eibstorage
-- END CREATE EVENTDATA VIEW
--END CREATE VIEWS - empty skeletons

--Enable snapshot isolation
DECLARE @ALTER_DB_SNAPSHOT_ISOLATION_SQL VARCHAR(300)
SET @ALTER_DB_SNAPSHOT_ISOLATION_SQL = 'ALTER DATABASE ' + QUOTENAME(DB_NAME()) + 'SET ALLOW_SNAPSHOT_ISOLATION ON'
EXEC (@ALTER_DB_SNAPSHOT_ISOLATION_SQL)
GO