﻿----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedues.sql
-- Summary: Script that creates the EIBStorage stored procedures
-- Project: EIB 7.7
--
-- Description:
-- Script that creates the EIBStorage stored procedures
--
-- Copyright(C) The PEER Group Inc., 2014 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 7.0 - 2013-07-17 - EIB 7.0, Initial Creation
--       7.1 - 2014-05-08 - EIB 7.1, Trace Data Performance Indexes
--       7.2 - 2014-09-15 - EIB 7.2, Performance Indexes, Enabling Snapshot Isolation, Making Requested Exception Fields Nullable, Fix to GetEIBStorageDatabaseInfo stored proc
--       7.7 - 2018-02-05 - EIB 7.7, Migration to partitioned data model.
-- -- --------------------------------------------------------------------

-- if the ComputePartition already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ComputePartition]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[ComputePartition]
GO

/****** Object:  StoredProcedure [dbo].[ComputePartition] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Computes the partition's name based on the given date and retention type given 
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------
CREATE PROC [dbo].[ComputePartition]	
	@Date datetimeoffset,
	@RetentionType int --1 = hourly, 2= daily, 3 =monthly
AS
BEGIN
	DECLARE @partition int 
	DECLARE @offset int
	DECLARE @startDate datetime = '2017-01-01'
	BEGIN TRY
		IF @RetentionType < 1
			RAISERROR (	'Retention type is not valid. Please enter a value ranging from 1 - 3 where 1 = hourly, 2= daily, 3 =monthly.', -- Message text.
						11, -- Severity.
						1 -- State.				
						);
		--make sure date passed is in UTC time
		SET @offset = datediff(hour, @Date, SYSUTCDATETIME())
		SET @Date = DATEADD(hour, @offset, @Date)

		IF( @RetentionType = 1)
			SET @partition = CASE WHEN DATEDIFF(hh,@startDate,@Date) > 0   THEN DATEDIFF(hh,@startDate,@Date) ELSE 0 END
		ELSE IF ( @RetentionType = 2)
			SET @partition = CASE WHEN DATEDIFF(dd,@startDate,@Date) > 0   THEN DATEDIFF(dd,@startDate,@Date) ELSE 0 END
		ELSE
			SET @partition = CASE WHEN DATEDIFF(mm,@startDate,@Date) > 0   THEN DATEDIFF(mm,@startDate,@Date) ELSE 0 END	

		RETURN @partition
	END TRY
	BEGIN CATCH
		DECLARE @errorMsg NVARCHAR(4000);
		DECLARE @errorSeverity INT;
		DECLARE @errorState INT;
		DECLARE @errorNumber INT;

		SELECT 
			@errorMsg = ERROR_MESSAGE(),
			@errorSeverity = ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorNumber = ERROR_NUMBER();

				BEGIN
					-- Use RAISERROR inside the CATCH block to return error
					-- information about the original error that caused
					-- execution to jump to the CATCH block.
					RAISERROR (@errorMsg, -- Message text.
								@errorSeverity, -- Severity.
								@errorState -- State.
								);
				END
	END CATCH; 
END
GO

-- if the MaintainIndexes already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[MaintainIndexes]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[MaintainIndexes]
GO

/****** Object:  StoredProcedure [dbo].[MaintainIndexes] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Maintains Indexes
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROCEDURE [dbo].[MaintainIndexes] 
	@maxfrag FLOAT = 30.0, 	
	@indexCount INT = 20,
	@partitionID INT = 0,	
	@updateStats BIT = 1
AS
BEGIN	
	SET NOCOUNT ON
	SET DEADLOCK_PRIORITY -10

	--Declare Variables
	DECLARE @storeTable TABLE (TableName nvarchar(1000), IndexName nvarchar(1000))

	DECLARE	@sql NVARCHAR(4000),
			@tableName NVARCHAR(200),
			@indextName NVARCHAR(200),
			@ret INT = 0,
			@appLock INT = 0,
			@lockName NVARCHAR(200) = 'MaintainIndexes' + CAST(@partitionID as NVARCHAR(10))

	EXEC @appLock = sp_getapplock
		@Resource = @lockName,
		@LockMode = 'Exclusive',
		@LockOwner = 'Session',
		@LockTimeout = 30000

	IF @appLock >= 0
	BEGIN
		BEGIN TRY

			INSERT INTO @storeTable (TableName, IndexName)
			SELECT TOP (@indexCount)
				t.name AS TableName,
				idx.name AS IndexName
			FROM 
				sys.indexes idx 
			INNER JOIN 
				sys.tables t ON idx.object_id = t.object_id 
			INNER JOIN
			sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'LIMITED') ips ON idx.object_id = ips.object_id
			WHERE 
				idx.is_primary_key = 0 AND idx.is_unique = 0 AND idx.is_unique_constraint = 0 AND t.is_ms_shipped = 0 
				AND t.name LIKE '%' + (CASE WHEN @partitionID = 0 THEN '' ELSE CAST(@partitionID AS NVARCHAR) END) AND ips.avg_fragmentation_in_percent > @maxfrag
			ORDER BY 
				ips.avg_fragmentation_in_percent DESC;

			WHILE (SELECT COUNT(*) FROM @storeTable) > 0
			BEGIN
				SELECT TOP 1 @tableName = TableName, @indextName = IndexName
				FROM @storeTable
					
				SET @sql = N'ALTER INDEX ' + @indextName + N' ON ' + @tableName + N' REORGANIZE'
							
				PRINT @sql;
				BEGIN TRY
					EXEC (@sql)

					SET @sql = N'UPDATE STATISTICS ' + @tableName
					-- Update stats if required
					IF @updateStats = 1
						EXEC (@sql)

					SET @ret = @ret + 1;
				END TRY
				BEGIN CATCH			
				END CATCH

				DELETE FROM @storeTable
				WHERE TableName = @tableName AND IndexName = @indextName		
			END
			
		EXEC @appLock = sp_releaseapplock @Resource=@lockName, @LockOwner='Session';

		--Return the index count successfully rebuilt
		SELECT @ret AS ReturnValue
		
		END TRY
		BEGIN CATCH
			EXEC @appLock = sp_releaseapplock @Resource=@lockName, @LockOwner='Session';
			DECLARE @ErrorMessage NVARCHAR(400);
			DECLARE @ErrorNumber INT;
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;
			DECLARE @ErrorLine INT;

			SELECT @ErrorMessage = ERROR_MESSAGE();
			SELECT @ErrorNumber = ERROR_NUMBER();
			SELECT @ErrorSeverity = ERROR_SEVERITY();
			SELECT @ErrorState = ERROR_STATE();
			SELECT @ErrorLine = ERROR_LINE();
			RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState, @ErrorNumber)
		END CATCH	
	END
END
GO

-- if the GetEIBStorageDatabaseInfo already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEIBStorageDatabaseInfo]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetEIBStorageDatabaseInfo]
GO

/****** Object:  StoredProcedure [dbo].[GetEIBStorageDatabaseInfo]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Gets the version of the SQL Server database and the amount of available space based on the edition
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------
CREATE PROC [dbo].[GetEIBStorageDatabaseInfo]
AS
DECLARE @maximumMB int
DECLARE @availableMB float
DECLARE @totalFileSizeMB float
DECLARE @totalUsedMB float
DECLARE @sqlServerInstanceName NVARCHAR(500)
DECLARE @sqlServerVersion NVARCHAR(500)
DECLARE @sqlServerEdition NVARCHAR(500)
DECLARE @eibStorageDatabaseName NVARCHAR(500)

SELECT 
		@totalFileSizeMB = SUM( CONVERT(FLOAT, ROUND(a.size / 128.000, 2) )), 
		@totalUsedMB = SUM( CONVERT(FLOAT, ROUND(FILEPROPERTY(a.NAME, 'SpaceUsed') / 128.000, 2) ))
	FROM SYS.sysfiles a
	WHERE a.NAME NOT LIKE '%_log'

SELECT TOP 1 @sqlServerInstanceName = CAST(SERVERPROPERTY('ServerName') AS NVARCHAR)
	,@sqlServerVersion = @@VERSION
	,@sqlServerEdition = CAST(SERVERPROPERTY('Edition') AS NVARCHAR)
	,@eibStorageDatabaseName = a.NAME
	,@maximumMB = CASE 
		WHEN SERVERPROPERTY('EngineEdition') = 4 -- Express Edition
			THEN CASE 
					WHEN CAST(SERVERPROPERTY('ProductVersion') AS NVARCHAR(128)) LIKE '10.0%' -- SQL Server 2008 (pre R2)
						THEN 4096
					ELSE 10240
					END
		ELSE -1
		END
	,@availableMB = CASE 
		WHEN SERVERPROPERTY('EngineEdition') = 4 -- Express Edition
			THEN CASE 
					WHEN CAST(SERVERPROPERTY('ProductVersion') AS NVARCHAR(128)) LIKE '10.0%' -- SQL Server 2008 (pre R2)
						THEN 4096
					ELSE 10240 --SQL Server 2008 R2 or higher
					END - @totalUsedMB
		ELSE -1
		END 
FROM SYS.sysfiles a
WHERE a.NAME NOT LIKE '%_log'

SELECT @sqlServerInstanceName as SQLServerInstanceName
	,@sqlServerVersion as SQLServerVersion 
	,@sqlServerEdition as SQLServerEdition
	,@eibStorageDatabaseName as EIBStorageDatabaseName
	,@maximumMB as MaximumMB 
	,@totalFileSizeMB as FileSizeMB
	,@totalUsedMB as UsedMB 
	,@availableMB as AvailableMB
GO


-- if the DeleteValueDataType already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteValueDataType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteValueDataType]
GO

/****** Object:  StoredProcedure [dbo].[DeleteValueDataType]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the ValueDataType table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteValueDataType] 
	@ValueDataTypeID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[ValueDataType]
	WHERE  [ValueDataTypeID] = @ValueDataTypeID

	COMMIT
GO

-- if the DeleteVersionData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteVersionData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteVersionData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteVersionData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the VersionData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteVersionData] 
	@VersionDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[VersionData]
	WHERE  [VersionDataID] = @VersionDataID

	COMMIT
GO

-- if the GetValueDataType already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetValueDataType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetValueDataType]
GO

/****** Object:  StoredProcedure [dbo].[GetValueDataType]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the ValueDataType table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetValueDataType] 
	@ValueDataTypeID INT
AS 
	SELECT [ValueDataTypeID], [Name], [Description] 
	FROM   [dbo].[ValueDataType] 
	WHERE  ([ValueDataTypeID] = @ValueDataTypeID OR @ValueDataTypeID IS NULL)
GO


-- if the GetVersionData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetVersionData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetVersionData]
GO

/****** Object:  StoredProcedure [dbo].[GetVersionData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the VersionData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetVersionData] 
	@VersionDataID INT
AS 
	SELECT [VersionDataID], [VersionNumber], [VersionName], [PreviousVersionNumber], [DateUpgraded] 
	FROM   [dbo].[VersionData] 
	WHERE  ([VersionDataID] = @VersionDataID OR @VersionDataID IS NULL)
GO


-- if the GetEIBStorageVersion already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEIBStorageVersion]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetEIBStorageVersion]
GO

/****** Object:  StoredProcedure [dbo].[GetEIBStorageVersion]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Gets the current version of the EIBStorage database from the Version table
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------
CREATE PROC [dbo].[GetEIBStorageVersion]
AS
SELECT TOP 1 [VersionNumber], [VersionName]
FROM [VersionData]
ORDER BY [VersionDataID] DESC
GO

-- if the SerializedTypeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SerializedTypeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[SerializedTypeData]
GO

/****** Object:  Table [dbo].[SerializedTypeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- if the UpdateContextInstanceData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateContextInstanceData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateContextInstanceData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateContextInstanceData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the ContextInstanceData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateContextInstanceData] 
	@ContextInstanceDataID int,
	@PlanDataID int,
	@ContextTime datetimeoffset,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'ContextInstanceData'  + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	
	DECLARE @sql nvarchar(max)
	SET @sql = 'UPDATE ' + @tableName + ' SET [PlanDataID] = @PlanDataID, [ContextTime] = @ContextTime WHERE  [ContextInstanceDataID] = @ContextInstanceDataID; SELECT @@ROWCOUNT'
	EXEC sp_executesql @sql, N'@PlanDataID INT , @ContextTime DATETIMEOFFSET,  @ContextInstanceDataID INT',@PlanDataID , @ContextTime , @ContextInstanceDataID
              

	COMMIT
GO

-- if the UpdateValueDataType already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateValueDataType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateValueDataType]
GO

/****** Object:  StoredProcedure [dbo].[UpdateValueDataType]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the ValueDataType table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateValueDataType] 
	@ValueDataTypeID int,
	@Name nvarchar(50),
	@Description nvarchar(50)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[ValueDataType]
	SET    [ValueDataTypeID] = @ValueDataTypeID, [Name] = @Name, [Description] = @Description
	WHERE  [ValueDataTypeID] = @ValueDataTypeID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the InsertValueDataType already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertValueDataType]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertValueDataType]
GO

/****** Object:  StoredProcedure [dbo].[InsertValueDataType]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the ValueDataType table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertValueDataType] 
	@ValueDataTypeID int,
	@Name nvarchar(50),
	@Description nvarchar(50)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[ValueDataType] ([ValueDataTypeID], [Name], [Description])
	SELECT @ValueDataTypeID, @Name, @Description
	
	SELECT SCOPE_IDENTITY() AS ValueDataTypeID
               
	COMMIT
GO

-- if the InsertVersionData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertVersionData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertVersionData]
GO

/****** Object:  StoredProcedure [dbo].[InsertVersionData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the VersionData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertVersionData] 
	@VersionNumber nvarchar(20),
	@VersionName nvarchar(100),
	@PreviousVersionNumber nvarchar(20),
	@DateUpgraded datetimeoffset
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[VersionData] ([VersionNumber], [VersionName], [PreviousVersionNumber], [DateUpgraded])
	SELECT @VersionNumber, @VersionName, @PreviousVersionNumber, @DateUpgraded
	
	SELECT SCOPE_IDENTITY() AS VersionDataID
               
	COMMIT
GO

-- if the UpdateVersionData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateVersionData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateVersionData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateVersionData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the VersionData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateVersionData] 
	@VersionDataID int,
	@VersionNumber nvarchar(20),
	@VersionName nvarchar(100),
	@PreviousVersionNumber nvarchar(20),
	@DateUpgraded datetimeoffset
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[VersionData]
	SET    [VersionNumber] = @VersionNumber, [VersionName] = @VersionName, [PreviousVersionNumber] = @PreviousVersionNumber, [DateUpgraded] = @DateUpgraded
	WHERE  [VersionDataID] = @VersionDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the UpdateValueIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateValueIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateValueIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateValueIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the ValueIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateValueIdentifierData] 
	@ValueIdentifierDataID int,
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@ParameterName nvarchar(500)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[ValueIdentifierData]
	SET    [RootID] = @RootID, [SourceID] = @SourceID, [ParameterName] = @ParameterName
	WHERE  [ValueIdentifierDataID] = @ValueIdentifierDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the UpdateEventIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateEventIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateEventIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateEventIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the EventIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateEventIdentifierData] 
	@EventIdentifierDataID int,
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@EventID nvarchar(500)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[EventIdentifierData]
	SET    [RootID] = @RootID, [SourceID] = @SourceID, [EventID] = @EventID
	WHERE  [EventIdentifierDataID] = @EventIdentifierDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the UpdateSerializedTypeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateSerializedTypeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateSerializedTypeData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateSerializedTypeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the SerializedTypeData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateSerializedTypeData] 
	@SerializedTypeDataID int,
	@SerializedType nvarchar(255)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[SerializedTypeData]
	SET    [SerializedType] = @SerializedType
	WHERE  [SerializedTypeDataID] = @SerializedTypeDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the UpdatePlanDetailData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdatePlanDetailData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdatePlanDetailData]
GO

/****** Object:  StoredProcedure [dbo].[UpdatePlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the PlanDetailData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdatePlanDetailData] 
	@PlanDetailDataID int,
	@PlanDetail xml,
	@PlanDetailHash nvarchar(50)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[PlanDetailData]
	SET    [PlanDetail] = @PlanDetail, [PlanDetailHash] = @PlanDetailHash
	WHERE  [PlanDetailDataID] = @PlanDetailDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the UpdateAlarmIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdateAlarmIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdateAlarmIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[UpdateAlarmIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the AlarmIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdateAlarmIdentifierData] 
	@AlarmIdentifierDataID int,
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@ExceptionID nvarchar(500),
	@RequestedSourceID nvarchar(4000),
	@RequestedExceptionID nvarchar(500)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[AlarmIdentifierData]
	SET    [RootID] = @RootID, [SourceID] = @SourceID, [ExceptionID] = @ExceptionID, [RequestedSourceID] = @RequestedSourceID, [RequestedExceptionID] = @RequestedExceptionID
	WHERE  [AlarmIdentifierDataID] = @AlarmIdentifierDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the InsertValueIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertValueIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertValueIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[InsertValueIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the ValueIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertValueIdentifierData] 
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@ParameterName nvarchar(450)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[ValueIdentifierData] ([RootID], [SourceID], [ParameterName])
	SELECT @RootID, @SourceID, @ParameterName
	
	SELECT SCOPE_IDENTITY() AS ValueIdentifierDataID
               
	COMMIT
GO

-- if the InsertSerializedTypeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertSerializedTypeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertSerializedTypeData]
GO

/****** Object:  StoredProcedure [dbo].[InsertSerializedTypeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the SerializedTypeData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertSerializedTypeData] 
	@SerializedType nvarchar(255)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[SerializedTypeData] ([SerializedType])
	SELECT @SerializedType
	
	SELECT SCOPE_IDENTITY() AS SerializedTypeDataID
               
	COMMIT
GO

-- if the InsertPlanDetailData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertPlanDetailData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertPlanDetailData]
GO

/****** Object:  StoredProcedure [dbo].[InsertPlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the PlanDetailData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertPlanDetailData] 
	@PlanDetail xml,
	@PlanDetailHash nvarchar(50)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[PlanDetailData] ([PlanDetail], [PlanDetailHash])
	SELECT @PlanDetail, @PlanDetailHash
	
	SELECT SCOPE_IDENTITY() AS PlanDetailDataID
               
	COMMIT
GO

-- if the GetAlarmIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetAlarmIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetAlarmIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[GetAlarmIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the AlarmIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetAlarmIdentifierData] 
	@AlarmIdentifierDataID INT
AS 
	SELECT [AlarmIdentifierDataID], [RootID], [SourceID], [ExceptionID], [RequestedSourceID], [RequestedExceptionID]
	FROM   [dbo].[AlarmIdentifierData] 
	WHERE  ([AlarmIdentifierDataID] = @AlarmIdentifierDataID OR @AlarmIdentifierDataID IS NULL)
GO

-- if the InsertEventIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertEventIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertEventIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[InsertEventIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the EventIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertEventIdentifierData] 
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@EventID nvarchar(500)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[EventIdentifierData] ([RootID], [SourceID], [EventID])
	SELECT @RootID, @SourceID, @EventID
	
	SELECT SCOPE_IDENTITY() AS EventIdentifierDataID
               
	COMMIT
GO

-- if the GetValueIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetValueIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetValueIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[GetValueIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the ValueIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetValueIdentifierData] 
	@ValueIdentifierDataID INT
AS 
	SELECT [ValueIdentifierDataID], [RootID], [SourceID], [ParameterName]
	FROM   [dbo].[ValueIdentifierData] 
	WHERE  ([ValueIdentifierDataID] = @ValueIdentifierDataID OR @ValueIdentifierDataID IS NULL)
GO


-- if the InsertAlarmIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertAlarmIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertAlarmIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[InsertAlarmIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the AlarmIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertAlarmIdentifierData] 
	@RootID nvarchar(100),
	@SourceID nvarchar(4000),
	@ExceptionID nvarchar(500),
	@RequestedSourceID nvarchar(4000),
	@RequestedExceptionID nvarchar(500)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[AlarmIdentifierData] ([RootID], [SourceID], [ExceptionID], [RequestedSourceID], [RequestedExceptionID])
	SELECT @RootID, @SourceID, @ExceptionID, @RequestedSourceID, @RequestedExceptionID
	
	SELECT SCOPE_IDENTITY() AS AlarmIdentifierDataID
               
	COMMIT
GO

-- if the GetEventIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEventIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetEventIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[GetEventIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the EventIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetEventIdentifierData] 
	@EventIdentifierDataID INT
AS 
	SELECT [EventIdentifierDataID], [RootID], [SourceID], [EventID]
	FROM   [dbo].[EventIdentifierData] 
	WHERE  ([EventIdentifierDataID] = @EventIdentifierDataID OR @EventIdentifierDataID IS NULL)
GO

-- if the GetSerializedTypeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetSerializedTypeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetSerializedTypeData]
GO

/****** Object:  StoredProcedure [dbo].[GetSerializedTypeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the SerializedTypeData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetSerializedTypeData] 
	@SerializedTypeDataID INT
AS 
	SELECT [SerializedTypeDataID], [SerializedType]
	FROM   [dbo].[SerializedTypeData] 
	WHERE  ([SerializedTypeDataID] = @SerializedTypeDataID OR @SerializedTypeDataID IS NULL)
GO

-- if the GetPlanDetailDataByPlanDetailHash already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetPlanDetailDataByPlanDetailHash]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetPlanDetailDataByPlanDetailHash]
GO

/****** Object:  StoredProcedure [dbo].[GetPlanDetailDataByPlanDetailHash]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Checks the PlanDetailData table to see if a specific PlanDetail XML document hash exists.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetPlanDetailDataByPlanDetailHash] 
	@PlanDetailHash NVARCHAR(50)
AS 
	SELECT [PlanDetailDataID], [PlanDetail], [PlanDetailHash]
	FROM   [dbo].[PlanDetailData] 
	WHERE  PlanDetailHash = @PlanDetailHash
GO

-- if the GetPlanDetailData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetPlanDetailData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetPlanDetailData]
GO

/****** Object:  StoredProcedure [dbo].[GetPlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the PlanDetailData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetPlanDetailData] 
	@PlanDetailDataID INT
AS 
	SELECT [PlanDetailDataID], [PlanDetail], [PlanDetailHash]
	FROM   [dbo].[PlanDetailData] 
	WHERE  ([PlanDetailDataID] = @PlanDetailDataID OR @PlanDetailDataID IS NULL)
GO

-- if the DeleteAlarmIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteAlarmIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteAlarmIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteAlarmIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the AlarmIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteAlarmIdentifierData] 
	@AlarmIdentifierDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[AlarmIdentifierData]
	WHERE  [AlarmIdentifierDataID] = @AlarmIdentifierDataID

	COMMIT
GO

-- if the DeleteEventIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteEventIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteEventIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteEventIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the EventIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteEventIdentifierData] 
	@EventIdentifierDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[EventIdentifierData]
	WHERE  [EventIdentifierDataID] = @EventIdentifierDataID

	COMMIT
GO

-- if the DeleteValueIdentifierData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteValueIdentifierData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteValueIdentifierData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteValueIdentifierData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the ValueIdentifierData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteValueIdentifierData] 
	@ValueIdentifierDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[ValueIdentifierData]
	WHERE  [ValueIdentifierDataID] = @ValueIdentifierDataID

	COMMIT
GO

-- if the DeleteSerializedTypeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeleteSerializedTypeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeleteSerializedTypeData]
GO

/****** Object:  StoredProcedure [dbo].[DeleteSerializedTypeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the SerializedTypeData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeleteSerializedTypeData] 
	@SerializedTypeDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[SerializedTypeData]
	WHERE  [SerializedTypeDataID] = @SerializedTypeDataID

	COMMIT
GO

-- if the DeletePlanDetailData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeletePlanDetailData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeletePlanDetailData]
GO

/****** Object:  StoredProcedure [dbo].[DeletePlanDetailData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the PlanDetailData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeletePlanDetailData] 
	@PlanDetailDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[PlanDetailData]
	WHERE  [PlanDetailDataID] = @PlanDetailDataID

	COMMIT
GO

-- if the DeletePlanData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DeletePlanData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DeletePlanData]
GO

/****** Object:  StoredProcedure [dbo].[DeletePlanData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Delete Stored Procedure for the PlanData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[DeletePlanData] 
	@PlanDataID int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	DELETE
	FROM   [dbo].[PlanData]
	WHERE  [PlanDataID] = @PlanDataID

	COMMIT
GO

-- if the GetPlanData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetPlanData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[GetPlanData]
GO

/****** Object:  StoredProcedure [dbo].[GetPlanData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Get Stored Procedure for the PlanData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[GetPlanData] 
	@PlanDataID INT
AS 
	SELECT [PlanDataID], [PlanDetailDataID], [InterfaceName], [DataConsumerName], [PlanName], [PlanID], [PlanActivationDate], [PlanDeactivationDate], [DeactivationReason], [Description]
	FROM   [dbo].[PlanData] 
	WHERE  ([PlanDataID] = @PlanDataID OR @PlanDataID IS NULL)
GO

-- if the InsertPlanData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertPlanData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertPlanData]
GO

/****** Object:  StoredProcedure [dbo].[InsertPlanData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the PlanData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertPlanData] 
	@PlanDetailDataID int,
	@InterfaceName nvarchar(255),
	@DataConsumerName nvarchar(255),
	@PlanName nvarchar(100),
	@PlanID uniqueidentifier,
	@PlanActivationDate datetimeoffset,
	@PlanDeactivationDate datetimeoffset,
	@DeactivationReason nvarchar(255),
	@Description nvarchar(255),
	@RetentionType int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	
	INSERT INTO [dbo].[PlanData] ([PlanDetailDataID], [InterfaceName], [DataConsumerName], [PlanName], [PlanID], [PlanActivationDate], [PlanDeactivationDate], [DeactivationReason], [Description], [RetentionType])
	SELECT @PlanDetailDataID, @InterfaceName, @DataConsumerName, @PlanName, @PlanID, @PlanActivationDate, @PlanDeactivationDate, @DeactivationReason, @Description, @RetentionType
	
	SELECT SCOPE_IDENTITY() AS PlanDataID
               
	COMMIT
GO

-- if the UpdatePlanData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[UpdatePlanData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[UpdatePlanData]
GO

/****** Object:  StoredProcedure [dbo].[UpdatePlanData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Update Stored Procedure for the PlanData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[UpdatePlanData] 
	@PlanDataID int,
	@PlanDetailDataID int,
	@InterfaceName nvarchar(255),
	@DataConsumerName nvarchar(255),
	@PlanName nvarchar(100),
	@PlanID uniqueidentifier,
	@PlanActivationDate datetimeoffset,
	@PlanDeactivationDate datetimeoffset,
	@DeactivationReason nvarchar(255),
	@Description nvarchar(255)
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN

	UPDATE [dbo].[PlanData]
	SET    [PlanDetailDataID] = @PlanDetailDataID, [InterfaceName] = @InterfaceName, [DataConsumerName] = @DataConsumerName, [PlanName] = @PlanName, [PlanID] = @PlanID, [PlanActivationDate] = @PlanActivationDate, [PlanDeactivationDate] = @PlanDeactivationDate, [DeactivationReason] = @DeactivationReason, [Description] = @Description
	WHERE  [PlanDataID] = @PlanDataID

	SELECT @@ROWCOUNT

	COMMIT
GO

-- if the InsertTraceData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertTraceData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertTraceData]
GO

/****** Object:  StoredProcedure [dbo].[InsertTraceData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the TraceData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertTraceData] 
	@PlanDataID int,
	@TraceID int,
	@ReportTime datetimeoffset,
	@CollectionTime datetimeoffset,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName ='TraceData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)
	
	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([PlanDataID], [TraceID], [ReportTime], [CollectionTime])' +
	'SELECT  @PlanDataID , @TraceID , @ReportTime,  @CollectionTime; SELECT SCOPE_IDENTITY() AS TraceDataID'
	EXEC sp_executesql @sql, N'@PlanDataID INT , @TraceID INT, @ReportTime DATETIMEOFFSET,  @CollectionTime DATETIMEOFFSET',@PlanDataID , @TraceID , @ReportTime,  @CollectionTime
               
	COMMIT
GO

-- if the InsertAttributeData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertAttributeData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertAttributeData]
GO

/****** Object:  StoredProcedure [dbo].[InsertAttributeData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the AttributeData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertAttributeData] 
	@PlanDataID int,
	@AttributeTime datetimeoffset,
	@ContextInstanceDataID bigint,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'AttributeData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([PlanDataID], [AttributeTime], [ContextInstanceDataID])' +
	'SELECT  @PlanDataID, @AttributeTime, @ContextInstanceDataID; SELECT SCOPE_IDENTITY() AS AttributeDataID'
	EXEC sp_executesql @sql,N'@PlanDataID INT, @AttributeTime DATETIMEOFFSET, @ContextInstanceDataID BIGINT', @PlanDataID ,  @AttributeTime, @ContextInstanceDataID	
               
	COMMIT
GO

-- if the InsertAlarmData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertAlarmData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertAlarmData]
GO

/****** Object:  StoredProcedure [dbo].[InsertAlarmData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the AlarmData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertAlarmData] 
	@PlanDataID int,
	@AlarmIdentifierDataID int,
	@Message nvarchar(255),
	@ExceptionTime datetimeoffset,
	@Severity nvarchar(50),
	@State nvarchar(50),
	@ExceptionType nvarchar(100),
	@RequestedExceptionType nvarchar(100),
	@Enabled bit,
	@ContextInstanceDataID bigint,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'AlarmData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([PlanDataID], [AlarmIdentifierDataID], [Message], [ExceptionTime], [Severity], [State], [ExceptionType], [RequestedExceptionType], [Enabled], [ContextInstanceDataID])' +
	'SELECT  @PlanDataID ,  @AlarmIdentifierDataID , @Message , @ExceptionTime, @Severity, @State, @ExceptionType, @RequestedExceptionType, @Enabled, @ContextInstanceDataID; SELECT SCOPE_IDENTITY() AS AlarmDataID'
	EXEC sp_executesql @sql, N'@PlanDataID int,  @AlarmIdentifierDataID int, @Message nvarchar(255), @ExceptionTime datetimeoffset, @Severity nvarchar(50), @State nvarchar(50), 
	@ExceptionType nvarchar(100), @RequestedExceptionType nvarchar(100), @Enabled bit, @ContextInstanceDataID bigint', 
	@PlanDataID ,  @AlarmIdentifierDataID , @Message , @ExceptionTime, @Severity, @State, @ExceptionType, @RequestedExceptionType, @Enabled, @ContextInstanceDataID
               
	COMMIT
GO

-- if the InsertEventData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertEventData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertEventData]
GO

/****** Object:  StoredProcedure [dbo].[InsertEventData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the EventData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertEventData] 
	@PlanDataID int,
	@EventIdentifierDataID int,
	@EventTime datetimeoffset,
	@ContextInstanceDataID bigint,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'EventData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([PlanDataID], [EventIdentifierDataID], [EventTime], [ContextInstanceDataID])' +
	'SELECT  @PlanDataID, @EventIdentifierDataID, @EventTime, @ContextInstanceDataID; SELECT SCOPE_IDENTITY() AS EventDataID'
	EXEC sp_executesql @sql, N' @PlanDataID int, @EventIdentifierDataID int, @EventTime datetimeoffset, @ContextInstanceDataID bigint',
	@PlanDataID, @EventIdentifierDataID, @EventTime, @ContextInstanceDataID
               
	COMMIT
GO

-- if the InsertContextInstanceData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertContextInstanceData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertContextInstanceData]
GO

/****** Object:  StoredProcedure [dbo].[InsertContextInstanceData]    Script Date: 10/30/2014 13:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the ContextInstanceData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertContextInstanceData] 
	@PlanDataID int,
	@ContextTime datetimeoffset,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'ContextInstanceData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([PlanDataID], [ContextTime])' +
	'SELECT  @PlanDataID, @ContextTime; SELECT SCOPE_IDENTITY() AS ContextInstanceDataID'
	EXEC sp_executesql @sql, N' @PlanDataID int, @ContextTime datetimeoffset', @PlanDataID, @ContextTime		
               
	COMMIT
GO

-- if the InsertAttributeValueData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertAttributeValueData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertAttributeValueData]
GO

/****** Object:  StoredProcedure [dbo].[InsertAttributeValueData]    Script Date: 10/30/2014 13:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the AttributeValueData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertAttributeValueData] 
	@AttributeDataID bigint,
	@ValueDataTypeID int,
	@IsContext bit,
	@IsOldPrimaryValue bit,
	@IsNewPrimaryValue bit,
	@ValueIdentifierDataID int,
	@ValueInt int,
	@ValueFloat float,
	@ValueBit bit,
	@ValueString nvarchar(MAX),
	@ValueXML xml,
	@ValueDate datetimeoffset,
	@SerializedTypeDataID int,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'AttributeValueData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([AttributeDataID], [ValueDataTypeID], [IsContext], [IsOldPrimaryValue], [IsNewPrimaryValue], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])' +
	'SELECT  @AttributeDataID, @ValueDataTypeID, @IsContext, @IsOldPrimaryValue, @IsNewPrimaryValue, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML, @ValueDate, @SerializedTypeDataID; SELECT SCOPE_IDENTITY() AS AttributeValueDataID'
	EXEC sp_executesql @sql, N' @AttributeDataID bigint, @ValueDataTypeID int, @IsContext bit, @IsOldPrimaryValue bit, @IsNewPrimaryValue bit, @ValueIdentifierDataID int, @ValueInt int,
	@ValueFloat float, @ValueBit bit, @ValueString nvarchar(MAX), @ValueXML xml, @ValueDate datetimeoffset, @SerializedTypeDataID int',
	@AttributeDataID, @ValueDataTypeID, @IsContext, @IsOldPrimaryValue, @IsNewPrimaryValue, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML,  @ValueDate, @SerializedTypeDataID
               
	COMMIT
GO

-- if the InsertEventValueData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertEventValueData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertEventValueData]
GO

/****** Object:  StoredProcedure [dbo].[InsertEventValueData]    Script Date: 10/30/2014 13:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the EventValueData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertEventValueData] 
	@EventDataID bigint,
	@ValueDataTypeID int,
	@IsContext bit,
	@ValueIdentifierDataID int,
	@ValueInt int,
	@ValueFloat float,
	@ValueBit bit,
	@ValueString nvarchar(MAX),
	@ValueXML xml,
	@ValueDate datetimeoffset,
	@SerializedTypeDataID int,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'EventValueData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([EventDataID], [ValueDataTypeID], [IsContext], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])' +
	'SELECT @EventDataID, @ValueDataTypeID, @IsContext, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML,  @ValueDate, @SerializedTypeDataID; SELECT SCOPE_IDENTITY() AS EventValueDataID'
	EXEC sp_executesql @sql, N'@EventDataID bigint, @ValueDataTypeID int, @IsContext bit, @ValueIdentifierDataID int, @ValueInt int, @ValueFloat float, @ValueBit bit,
	@ValueString nvarchar(MAX), @ValueXML xml, @ValueDate datetimeoffset, @SerializedTypeDataID int',
	@EventDataID, @ValueDataTypeID, @IsContext, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML,  @ValueDate, @SerializedTypeDataID		
               
	COMMIT
GO

-- if the InsertAlarmValueData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertAlarmValueData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertAlarmValueData]
GO

/****** Object:  StoredProcedure [dbo].[InsertAlarmValueData]    Script Date: 10/30/2014 13:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the AlarmValueData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertAlarmValueData] 
	@AlarmDataID bigint,
	@ValueDataTypeID int,
	@IsContext bit,
	@ValueIdentifierDataID int,
	@ValueInt int,
	@ValueFloat float,
	@ValueBit bit,
	@ValueString nvarchar(MAX),
	@ValueXML xml,
	@ValueDate datetimeoffset,
	@SerializedTypeDataID int,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)
	SET @tableName = 'AlarmValueData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([AlarmDataID], [ValueDataTypeID], [IsContext], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])' +
	'SELECT @AlarmDataID, @ValueDataTypeID, @IsContext, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML, @ValueDate, @SerializedTypeDataID; SELECT SCOPE_IDENTITY() AS AlarmValueDataID'
	EXEC sp_executesql @sql, N'@AlarmDataID bigint, @ValueDataTypeID int, @IsContext bit, @ValueIdentifierDataID int, @ValueInt int, @ValueFloat float, @ValueBit bit, @ValueString nvarchar(MAX),
	@ValueXML xml, @ValueDate datetimeoffset, @SerializedTypeDataID int',
	@AlarmDataID, @ValueDataTypeID, @IsContext, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML, @ValueDate, @SerializedTypeDataID	
               
	COMMIT
GO

-- if the InsertTraceValueData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertTraceValueData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertTraceValueData]
GO

/****** Object:  StoredProcedure [dbo].[InsertTraceValueData]    Script Date: 10/30/2014 13:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the TraceValueData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertTraceValueData] 
	@TraceDataID bigint,
	@ValueDataTypeID int,
	@ValueIdentifierDataID int,
	@ValueInt int,
	@ValueFloat float,
	@ValueBit bit,
	@ValueString nvarchar(MAX),
	@ValueXML xml,
	@ValueDate datetimeoffset,
	@SerializedTypeDataID int,
	@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName =  'TraceValueData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([TraceDataID], [ValueDataTypeID], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])' +
	'SELECT  @TraceDataID, @ValueDataTypeID, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML, @ValueDate, @SerializedTypeDataID; SELECT SCOPE_IDENTITY() AS TraceValueDataID'
	EXEC sp_executesql @sql, N' @TraceDataID bigint, @ValueDataTypeID int, @ValueIdentifierDataID int, @ValueInt int, @ValueFloat float, @ValueBit bit,
	@ValueString nvarchar(MAX), @ValueXML xml, @ValueDate datetimeoffset, @SerializedTypeDataID int',
	@TraceDataID, @ValueDataTypeID, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString, @ValueXML, @ValueDate, @SerializedTypeDataID		
               
	COMMIT
GO

-- if the InsertContextInstanceValueData already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertContextInstanceValueData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[InsertContextInstanceValueData]
GO

/****** Object:  StoredProcedure [dbo].[InsertContextInstanceValueData]    Script Date: 10/30/2014 13:13:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Description:
-- Insert Stored Procedure for the InsertContextInstanceValueData table.
--
-- Copyright(C) The PEER Group Inc., 2013 - 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- Revision History:
--
-- $Log: 1.0 - Initial Creation
--
-- --------------------------------------------------------------------

CREATE PROC [dbo].[InsertContextInstanceValueData] 
			@ContextInstanceDataID bigint,
			@ValueDataTypeID int,
			@ValueIdentifierDataID int,
			@ValueInt int,
			@ValueFloat float,
			@ValueBit bit,
			@ValueString nvarchar(MAX),
			@PartitionValue int
AS 
	SET NOCOUNT ON 
	SET XACT_ABORT ON  
	
	BEGIN TRAN
	DECLARE @tableName nvarchar(4000)

	SET @tableName = 'ContextInstanceValueData' + RIGHT('000000000'+CAST(@PartitionValue as varchar), 9)

	DECLARE @sql nvarchar(max)
	SET @sql = 'INSERT INTO ' + @tableName + ' ([ContextInstanceDataID], [ValueDataTypeID], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString])' +
	'SELECT  @ContextInstanceDataID, @ValueDataTypeID, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString; SELECT SCOPE_IDENTITY() AS ContextInstanceValueDataID'
	EXEC sp_executesql @sql, N' @ContextInstanceDataID bigint, @ValueDataTypeID int, @ValueIdentifierDataID int, @ValueInt int, @ValueFloat float, @ValueBit bit,
	@ValueString nvarchar(MAX)',
	@ContextInstanceDataID, @ValueDataTypeID, @ValueIdentifierDataID, @ValueInt, @ValueFloat, @ValueBit, @ValueString
               
	COMMIT
GO

-- if the DropPartitions already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DropPartitions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DropPartitions]
GO

/****** Object:  StoredProcedure [dbo].[DropPartitions]    Script Date: 1/22/2018 10:57:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ---------------------------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Name:
--         DropPartitions
--
-- Description:
--         Drops  Alarm, Attribute, Event and Trace data partition tables, updates corresponding views and files
--         up to the specified date. Specifying a RetentionTerm of 0 will not drop any tables, however the partition
--         table views are updated. 
--
-- Copyright(C) The PEER Group Inc., 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- PEER®, PEER Group® and PEERFactory® are registered trademarks
-- of The PEER Group Inc. in the United States and Canada.com
--
-- ---------------------------------------------------------------------------------------
CREATE PROC [dbo].[DropPartitions]
(
	@RetentionType int, --1 = hourly, 2= daily, 3 =monthly
	@RetentionTerm int
) 
AS
BEGIN TRY

	--return code
	DECLARE @ret int 
	IF @RetentionTerm < 0
		RAISERROR (	'Specified retention term is invalid. Please enter a value greater than or equal to 0.', -- Message text.
					11, -- Severity.
					0 -- State.					
					);

	SET @RetentionTerm += 1 --Increment retention term to make sure there is always at least 1 full hour/day/month worth of data
	DECLARE @tableName nvarchar(4000)
	DECLARE @currentContextInstanceDataTableName nvarchar(4000)
	DECLARE @sql nvarchar(max)
	DECLARE @filename nvarchar(4000)

	DECLARE @contextInstanceDataBaseName varchar(50) =   'ContextInstanceData' 
	DECLARE @contextInstanceValueDataBaseName varchar(50) =  'ContextInstanceValueData' 

	-- trace data table names
	DECLARE @traceDataBaseName varchar(50)  = 'TraceData' 
	DECLARE @traceValueDataBaseName varchar(50) =  'TraceValueData' 
	
	-- alarm data table names
	DECLARE @alarmDataBaseName varchar(50) =  'AlarmData' 
	DECLARE @alarmValueDataBaseName varchar(50) =  'AlarmValueData'

	-- attribute data table names
	DECLARE @attributeDataBaseName varchar(50) =  'AttributeData' 
	DECLARE @attributeValueDataBaseName varchar(50) =  'AttributeValueData' 

	-- event data table names
	DECLARE @eventDataBaseName varchar(50) =  'EventData' 
	DECLARE @eventValueDataBaseName varchar(50) = 'EventValueData'

	DECLARE @p int
	DECLARE @date datetime, @lastday datetime
	DECLARE @lastPartition int
	DECLARE @latestPartition int

	DECLARE @deleteTableList TABLE (TableName nvarchar(200))

	-- REMOVE partitions older than the specified number of days to keep but ignore tables ending in 2000000 
	-- as they should be purged using older purge script (part of update process)
	-- should remove the tables before creating the view.
	SELECT TOP 1 @p = CAST(SUBSTRING(Name,LEN(@traceDataBaseName)+1,99) as int) FROM sys.tables WHERE name not like 'TraceData200000000' and name like @traceDataBaseName+ CAST(@RetentionType as varchar) +'[0-9]%' ORDER BY CAST(SUBSTRING(Name,LEN(@traceDataBaseName)+1,99) as int) ASC
	
	IF @date IS NULL
		SET @date = SYSUTCDATETIME()

	EXEC @latestPartition = dbo.ComputePartition @date, @RetentionType 
	SET @lastPartition = @latestPartition - @RetentionTerm

	INSERT INTO @deleteTableList (TableName)
	SELECT name FROM sys.tables WHERE RTRIM(LTRIM(RIGHT(name, 9))) LIKE CAST(@RetentionType AS nvarchar) + '%' AND name not like '%200000000' AND CASE WHEN ISNUMERIC(RTRIM(LTRIM(RIGHT(name, 9)))) = 1 THEN CONVERT(bigint, RTRIM(LTRIM(RIGHT(name, 9)))) ELSE 9223372036854775807 END <= ((@RetentionType * 100000000) + @lastPartition)

	DECLARE @delimiter nvarchar(40)

	-- BEGIN CREATE CONTEXTINSTANCEDATA VIEW
	IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceData]'))
		SET @sql = 'ALTER VIEW ContextInstanceData AS '
	ELSE
		SET @sql = 'CREATE VIEW ContextInstanceData AS '

	SET @delimiter = ' '
	SET @tableName = NULL

	DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @contextInstanceDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@contextInstanceDataBaseName)+1,99) as int) DESC
	OPEN tableCursor
	FETCH NEXT FROM tableCursor INTO @tableName

	IF( @tableName IS NOT NULL) --Make sure tables exist before creating the views
	BEGIN
		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [ContextInstanceData] TO db_eibstorage
		-- END CREATE CONTEXTINSTANCEDATA VIEW

		-- BEGIN CREATE CONTEXTINSTANCEVALUEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceValueData]'))
			SET @sql = 'ALTER VIEW ContextInstanceValueData AS '
		ELSE
			SET @sql = 'CREATE VIEW ContextInstanceValueData AS '
	
		SET @delimiter = ' '

		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceValueData200000000]'))
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ContextInstanceValueData200000000' 
			SET @delimiter = ' UNION ALL '
		END
	
		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @contextInstanceValueDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@contextInstanceValueDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [ContextInstanceValueData] TO db_eibstorage
		-- END CREATE CONTEXTINSTANCEVALUEDATA VIEW
	
		-- BEGIN CREATE TRACEVALUEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[TraceValueData]'))
			SET @sql = 'ALTER VIEW TraceValueData AS '
		ELSE
			SET @sql = 'CREATE VIEW TraceValueData AS '
	
		SET @delimiter = ' '

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @traceValueDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@traceValueDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [TraceValueData] TO db_eibstorage
		-- END CREATE TRACEVALUEDATA VIEW

		-- BEGIN CREATE TRACEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[TraceData]'))
			SET @sql = 'ALTER VIEW TraceData AS '
		ELSE
			SET @sql = 'CREATE VIEW TraceData AS '

		SET @delimiter = ' '	

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @traceDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@traceDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [TraceData] TO db_eibstorage
		-- END CREATE TRACEDATA VIEW

		-- BEGIN ALARMVALUEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AlarmValueData]'))
			SET @sql = 'ALTER VIEW AlarmValueData AS '
		ELSE
			SET @sql = 'CREATE VIEW AlarmValueData AS '

		SET @delimiter = ' '

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @alarmValueDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@alarmValueDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [AlarmValueData] TO db_eibstorage
		-- END CREATE ALARMVALUEDATA VIEW

		-- BEGIN ALARMDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AlarmData]'))
			SET @sql = 'ALTER VIEW AlarmData AS '
		ELSE
			SET @sql = 'CREATE VIEW AlarmData AS '

		SET @delimiter = ' '

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @alarmDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@alarmDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [AlarmData] TO db_eibstorage
		-- END CREATE ALARMDATA VIEW	

		-- BEGIN ATTRIBUTEVALUEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AttributeValueData]'))
			SET @sql = 'ALTER VIEW AttributeValueData AS '
		ELSE
			SET @sql = 'CREATE VIEW AttributeValueData AS '

		SET @delimiter = ' '	

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @attributeValueDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@attributeValueDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [AttributeValueData] TO db_eibstorage
		-- END CREATE ATTRIBUTEVALUEDATA VIEW

		-- BEGIN ATTRIBUTEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[AttributeData]'))
			SET @sql = 'ALTER VIEW AttributeData AS '
		ELSE
			SET @sql = 'CREATE VIEW AttributeData AS '

		SET @delimiter = ' '	

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @attributeDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@attributeDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [AttributeData] TO db_eibstorage
		-- END CREATE ATTRIBUTEDATA VIEW				

		-- BEGIN EVENTVALUEDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[EventValueData]'))
			SET @sql = 'ALTER VIEW EventValueData AS '
		ELSE
			SET @sql = 'CREATE VIEW EventValueData AS '

		SET @delimiter = ' '	

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @eventValueDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@eventValueDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [EventValueData] TO db_eibstorage
		-- END CREATE EVENTVALUEDATA VIEW

		-- BEGIN EVENTDATA VIEW
		IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[EventData]'))
			SET @sql = 'ALTER VIEW EventData AS '
		ELSE
			SET @sql = 'CREATE VIEW EventData AS '

		SET @delimiter = ' '	

		DECLARE tableCursor CURSOR LOCAL FOR SELECT name FROM sys.tables WHERE name like @eventDataBaseName +'[0-9]%' AND name NOT IN (SELECT TableName FROM @deleteTableList) ORDER BY CAST(SUBSTRING(Name,LEN(@eventDataBaseName)+1,99) as int) DESC
		OPEN tableCursor
		FETCH NEXT FROM tableCursor INTO @tableName

		WHIlE @@FETCH_STATUS=0
		BEGIN
			SET @sql = @sql + @delimiter + ' SELECT * FROM ' + @tableName
			SET @delimiter = ' UNION ALL '
			FETCH NEXT FROM tableCursor INTO @tableName
		END
		EXEC sp_executesql @SQL
	
		CLOSE tableCursor
		DEALLOCATE tableCursor
	
		GRANT SELECT ON [EventData] TO db_eibstorage
		-- END CREATE EVENTDATA VIEW

		IF(@ret IS NULL) 
			SELECT @ret = 1
	END
	ELSE
		BEGIN
			IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[ContextInstanceValueData]'))
				BEGIN			
					-- BEGIN CONTEXTINSTANCEDATA VIEW
					SET @sql = 'ALTER VIEW ContextInstanceData AS SELECT CAST(NULL AS bigint) AS ContextInstanceDataID, NULL as PlanDataID, CAST(NULL AS datetimeoffset(7)) AS ContextTime'
					EXEC sp_executesql @SQL
					-- END CONTEXTINSTANCEDATA VIEW

					-- BEGIN CONTEXTINSTANCEVALUEDATA VIEW
					SET @sql = 'ALTER VIEW ContextInstanceValueData AS SELECT NULL AS ContextInstanceValueDataID, CAST(NULL AS bigint) as ContextInstanceDataID, NULL AS ValueDataTypeID, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString'
					EXEC sp_executesql @SQL
					-- END CONTEXTINSTANCEVALUEDATA VIEW
	
					-- BEGIN TRACEVALUEDATA VIEW
					SET @sql = 'ALTER VIEW TraceValueData AS SELECT NULL AS TraceValueDataID, CAST(NULL AS bigint) as TraceDataID, NULL AS ValueDataTypeID, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID'
					EXEC sp_executesql @SQL
					-- END TRACEVALUEDATA VIEW

					-- BEGIN TRACEDATA VIEW
					SET @sql = 'ALTER VIEW TraceData AS SELECT CAST(NULL AS bigint) AS TraceDataID, NULL as PlanDataID, NULL AS TraceID, CAST(NULL AS datetimeoffset(7)) AS ReportTime, CAST(NULL AS datetimeoffset(7)) AS CollectionTime'
					EXEC sp_executesql @SQL
					-- END TRACEDATA VIEW

					-- BEGIN ALARMVALUEDATA VIEW
					SET @sql = 'ALTER VIEW AlarmValueData AS SELECT NULL AS AlarmValueDataID, CAST(NULL AS bigint) as AlarmDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID'
					EXEC sp_executesql @SQL
					-- END ALARMVALUEDATA VIEW

					-- BEGIN ALARMDATA VIEW
					SET @sql = 'ALTER VIEW AlarmData AS SELECT CAST(NULL AS bigint) AS AlarmDataID, NULL as PlanDataID, NULL AS AlarmIdentifierDataID, CAST(NULL AS nvarchar(255)) AS Message, CAST(NULL AS datetimeoffset(7)) AS ExceptionTime, CAST(NULL AS nvarchar(50)) AS Severity, CAST(NULL AS nvarchar(50)) AS State, CAST(NULL AS nvarchar(100)) AS ExceptionType, CAST(NULL AS nvarchar(100)) AS RequestedExceptionType, CAST(NULL AS bit) AS Enabled, CAST(NULL AS bigint) AS ContextInstanceDataID'
					EXEC sp_executesql @SQL
					-- END ALARMDATA VIEW	

					-- BEGIN ATTRIBUTEVALUEDATA VIEW
					SET @sql = 'ALTER VIEW AttributeValueData AS SELECT NULL AS AttributeValueDataID, CAST(NULL AS bigint) as AttributeDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, CAST(NULL AS bit) AS IsOldPrimaryValue, CAST(NULL AS bit) AS IsNewPrimaryValue, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID'
					EXEC sp_executesql @SQL
					-- END ATTRIBUTEVALUEDATA VIEW

					-- BEGIN ATTRIBUTEDATA VIEW
					SET @sql = 'ALTER VIEW AttributeData AS SELECT CAST(NULL AS bigint) AS AttributeDataID, NULL as PlanDataID, CAST(NULL AS datetimeoffset(7)) AS AttributeTime, CAST(NULL AS bigint) AS ContextInstanceDataID '
					EXEC sp_executesql @SQL
					-- END ATTRIBUTEDATA VIEW				

					-- BEGIN EVENTVALUEDATA VIEW
					SET @sql = 'ALTER VIEW EventValueData AS SELECT NULL AS EventValueDataID,  CAST(NULL AS bigint) as EventDataID, NULL AS ValueDataTypeID, CAST(NULL AS bit) AS IsContext, NULL AS ValueIdentifierDataID, NULL AS ValueInt, CAST(NULL AS float) AS ValueFloat, CAST(NULL AS bit) AS ValueBit, CAST(NULL AS nvarchar(max)) AS ValueString, CAST(NULL AS xml) AS ValueXML, CAST(NULL AS datetimeoffset(7)) AS ValueDate, NULL AS SerializedTypeDataID '
					EXEC sp_executesql @SQL
					-- END EVENTVALUEDATA VIEW

					-- BEGIN EVENTDATA VIEW
					SET @sql = 'ALTER VIEW EventData AS SELECT CAST(NULL AS bigint) AS EventDataID, NULL as PlanDataID, NULL AS EventIdentifierDataID, CAST(NULL AS datetimeoffset(7)) AS EventTime, CAST(NULL AS bigint) AS ContextInstanceDataID'
					EXEC sp_executesql @SQL
					-- END EVENTDATA VIEW

					IF(@ret IS NULL)
						SELECT @ret = 1
				END
			ELSE
				RAISERROR (	'No tables exist to create views in the database.', -- Message text.
						11, -- Severity.
						2 -- State.					
						);


	END

		-- if the retention term is 1 (meaning that 0 was passed in - we skip logic to delete tables and just update the views.
	IF ((@p IS NOT NULL) AND (@RetentionTerm > 1))
		BEGIN
		IF(@lastPartition >= 0 ) --if date has not passed for the defined retention term don't drop any tables
			BEGIN
				SET @lastPartition =  CAST(CAST(@RetentionType as varchar)+ RIGHT('00000000'+ CAST(@lastPartition as varchar), 8) as int)	  

				DECLARE @partitionValue nvarchar(8) = RIGHT('00000000'+CAST(@p as varchar), 8)

				IF(@p > @lastPartition)					
					SELECT @ret = 2 --no tables exist that are older than the specified retention term
				ELSE
					BEGIN
						WHILE @p <= @lastPartition
						BEGIN
							SET @partitionValue = RIGHT('00000000'+CAST(@p as varchar), 8)

							SET @tableName = @attributeValueDataBaseName + CAST(@RetentionType as varchar)+ @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_SerializedTypeData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped SerializedTypeData constraint'
					
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_AttributemData'
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped AttributeData constraint'	
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueDataType'
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueDataType constraint'							

								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueIdentifierData'
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'			
								
								-- Now Drop the attribute value data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
		
							SET @tableName = @attributeDataBaseName + CAST(@RetentionType as varchar)+ @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_PlanData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped PlanData constraint'	
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_ContextInstanceData'+@tableName			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ContextInstanceData constraint'		

								-- Now Drop the attribute data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
			
							SET @tableName = @alarmValueDataBaseName + CAST(@RetentionType as varchar)+ @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_SerializedTypeData'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped SerializedTypeData constraint'
					
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_AlarmData'								
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped AlarmData constraint'	
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueDataType'						
								EXEC sp_executesql @SQL
								--PRINT 'Table ' + @tableName + ' Dropped ValueDataType constraint'							

								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueIdentifierData'					
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueIdentifierData constraint'			
								
								-- Drop the alarm value data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
		
							SET @tableName = @alarmDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_PlanData'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped PlanData constraint'	
								
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_AlarmIdentifierDataID'					
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped AlarmIdentifierDataID constraint'	
							
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_ContextInstanceData'+@tableName 			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ContextInstanceDataID constraint'
								
								-- Drop the alarm data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
		
							SET @tableName = @eventValueDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables							
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_SerializedTypeData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped SerializedTypeData constraint'
					
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_EventData'					
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped EventData constraint'	
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueDataType'					
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueDataType constraint'							

								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueIdentifierData'					
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueIdentifierData constraint'

								-- Drop the event value data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
		
							SET @tableName = @eventDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_PlanData'				
								EXEC sp_executesql @SQL
								--PRINT 'Table ' + @tableName + ' Dropped PlanData constraint'	
			
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_EventIdentifierData'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped EventIdentifierDataID constraint'	
		
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_ContextInstanceData'+@tableName						
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ContextInstanceDataID constraint'	
								
								-- Drop the event data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END


							SET @tableName = @traceValueDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_SerializedTypeData'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped SerializedTypeData constraint'
					
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_TraceData'							
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped TraceData constraint'	
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueDataType'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueDataType constraint'							

								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueIdentifierData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueIdentifierData constraint'	

								-- Drop the trace value data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END

							SET @tableName = @traceDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_PlanData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped PlanData constraint'	

								-- Drop the trace data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END

							SET @tableName = @contextInstanceValueDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							-- compute the parent table name as it's need to delete the FK constraint
							SET @currentContextInstanceDataTableName = @contextInstanceDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- Drop the FK constraints prior  to dropping the table to prevent
								-- any potential deadlocks with the parent referenced tables
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_'+@currentContextInstanceDataTableName
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ContextInstanceDataID constraint'								
						
								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueDataType'				
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueDataType constraint'							

								SET @sql = 'ALTER TABLE ['+@tableName+'] DROP CONSTRAINT FK_'+@tableName+'_ValueIdentifierData'			
								EXEC sp_executesql @SQL
								-- PRINT 'Table ' + @tableName + ' Dropped ValueIdentifierData constraint'	

								-- Drop the context instance value data table
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END
		
							SET @tableName = @contextInstanceDataBaseName + CAST(@RetentionType as varchar) + @partitionValue
							IF EXISTS (SELECT * FROM sys.tables WHERE name = @tableName)
							BEGIN
								-- There are no FK constraints on the ContextInstanceData table 
								SET @SQL = 'DROP TABLE ' + @tableName
								--PRINT @SQL
								EXEC sp_executesql @sql
							END

							SET @p = @p +1;
						END
					END										
			END
		END
	ELSE
		SELECT @ret = 2 --nothing to delete
END TRY
BEGIN CATCH
		DECLARE @errorMsg NVARCHAR(4000);
		DECLARE @errorSeverity INT;
		DECLARE @errorState INT;
		DECLARE @errorNumber INT;

		SELECT 
			@errorMsg = ERROR_MESSAGE(),
			@errorSeverity = ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorNumber = ERROR_NUMBER();

			--exit gracefully as there's nothing to delete
			IF (@errorNumber = 50000 AND @errorState = 0 AND @errorSeverity = 11)
				SELECT @ret = 2
			ELSE
				BEGIN
					-- Use RAISERROR inside the CATCH block to return error
					-- information about the original error that caused
					-- execution to jump to the CATCH block.
					RAISERROR (@errorMsg, -- Message text.
								@errorSeverity, -- Severity.
								@errorState -- State.
								);
				END
END CATCH;   
SELECT @ret AS ReturnValue, CAST(CAST(@RetentionType as varchar)+ RIGHT('00000000'+ CAST(@latestPartition as varchar), 8) as int) as PartitionValue
GO

-- if the ManagePartitions already exist (due to an upgrade) drop it so that the new sproc can be created.
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ManagePartitions]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[ManagePartitions]
GO

/****** Object:  StoredProcedure [dbo].[ManagePartitions]    Script Date: 10/2/2017 9:18:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ---------------------------------------------------------------------------------------
-- $Workfile: EIBStorageStoredProcedures.sql
-- Summary: EIBStorage Stored Procedures
-- Project: EIB 7.8
--
-- Name:
--         ManagePartitions
--
-- Description:
--         Creates Alarm, Attribute, Event and Trace data partition tables, corresponding views and files
--         up to the specified date. Grants access to the appropriate roles
--date
--
-- Notes:
--         Time passed in is in UTC time
--         Number of tables/partitions per data file is taken from the 
--         ConfiguredSetting 'TracesPartitionsPerFile'.
--         FilePath for the data file is taken from the ConfiguredSetting "TracesDataFilePath" 
--         Traces partition tables are TracesNNNNN
--
-- Copyright(C) The PEER Group Inc., 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
--
-- PEER®, PEER Group® and PEERFactory® are registered trademarks
-- of The PEER Group Inc. in the United States and Canada.com
--
-- ---------------------------------------------------------------------------------------
CREATE PROC [dbo].[ManagePartitions]
(
	@RetentionType int, --1 = hourly, 2= daily, 3 =monthly
	@RetentionTerm int
) 
AS
BEGIN
	SET NOCOUNT ON	
	--return code
	DECLARE @ret int 

	-- define how many partitions will be in one file
	DECLARE @firstDay datetime, @date datetime
	DECLARE @partition int

	-- context instance table name
	DECLARE @contextInstanceDataBaseName varchar(50) =  'ContextInstanceData'+ CAST(@RetentionType as varchar)
	DECLARE @contextInstanceValueDataBaseName varchar(50) = 'ContextInstanceValueData'	+ CAST(@RetentionType as varchar)

	-- trace data table names
	DECLARE @traceDataBaseName varchar(50)  =  'TraceData'+ CAST(@RetentionType as varchar)
	DECLARE @traceValueDataBaseName varchar(50) =  'TraceValueData'+ CAST(@RetentionType as varchar)
	
	-- alarm data table names
	DECLARE @alarmDataBaseName varchar(50) = 'AlarmData'+ CAST(@RetentionType as varchar)
	DECLARE @alarmValueDataBaseName varchar(50) =  'AlarmValueData'+ CAST(@RetentionType as varchar)

	-- attribute data table names
	DECLARE @attributeDataBaseName varchar(50) = 'AttributeData'+ CAST(@RetentionType as varchar)
	DECLARE @attributeValueDataBaseName varchar(50) =  'AttributeValueData'+ CAST(@RetentionType as varchar)

	-- event data table names
	DECLARE @eventDataBaseName varchar(50) =  'EventData'+ CAST(@RetentionType as varchar)
	DECLARE @eventValueDataBaseName varchar(50) = 'EventValueData'+ CAST(@RetentionType as varchar)
	
	IF @date IS NULL
		SET @date = SYSUTCDATETIME()

		SELECT @firstDay =  @date

	BEGIN TRY      
	-- determine the last partition we need to create during this run.
	DECLARE @lastPartitionToCreate int
	EXEC @lastPartitionToCreate =  dbo.ComputePartition @firstDay, @RetentionType 

	-- determine last partition that exists
	DECLARE @lastPartition int
	SELECT TOP 1 @lastPartition = CAST(SUBSTRING(Name,LEN(@traceDataBaseName)+1,99) as int)
	FROM sys.tables WHERE name like @traceDataBaseName+'[0-9]%' -- remove clause as it fails on Multi-AZ environments AND @lastPartitionToCreate >= CAST(SUBSTRING(Name,LEN(@tableBaseName)+1,99) as int) 
	ORDER BY CAST(SUBSTRING(Name,LEN(@traceDataBaseName)+1,99) as int) DESC
	
	IF @lastPartitionToCreate = @lastPartition
		RAISERROR (	'Necessary tables already exist.', -- Message text.
					11, -- Severity.
					0 -- State.				
					);
	ELSE IF @lastPartition is NULL
		EXEC @lastPartition = dbo.ComputePartition @firstDay, @RetentionType  -- only creating today's partition

	--To avoid ID collisions between different retention type tables, setup partition identities to start at different values within a bigint - 3 quintillion IDs per table supported
	DECLARE @identityStartValue bigint
	IF( @RetentionType = 1)
		SET @identityStartValue = 3000000000000000001
	ELSE IF ( @RetentionType = 2)
		SET @identityStartValue = 1
	ELSE
		SET @identityStartValue = 6000000000000000001

	DECLARE @sql nvarchar(max)
	DECLARE @tableName nvarchar(4000)
	DECLARE @currentContextInstanceTableName nvarchar(4000)
	DECLARE @currentParentTableName nvarchar(4000)

	DECLARE @p int
	SET @p = @lastPartitionToCreate

	-- create all the partition tables that belong in this file/filegroup
	DECLARE @previousTableValue nvarchar(8)	= RIGHT('00000000'+CAST((@lastPartition) as varchar), 8)
	DECLARE @partitionValue nvarchar(8) = RIGHT('00000000'+CAST(@p as varchar), 8)
	DECLARE @currentIdentityID bigint
	DECLARE @UpdatedTableValue nvarchar(9) = '200000000'; --appended value part of the update script

	-- BEGIN Context Instance Data table	
	DECLARE @previousTableName nvarchar(4000) = @contextInstanceDataBaseName + @previousTableValue
	print @previousTableName	
	--Query for most recent entry
	SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
	EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;

	IF(@currentIdentityID is null)
		BEGIN
		--Check to see if table from an upgraded older DB exists
		IF(@RetentionType = 2)
			BEGIN
				SET @previousTableName = 'ContextInstanceData' + @UpdatedTableValue
				SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
				EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;
			END

		IF(@currentIdentityID is null)
			SET @currentIdentityID = @identityStartValue
		ELSE
			SET @currentIdentityID = @currentIdentityID + 1
		END
	ELSE 
		SET @currentIdentityID = @currentIdentityID + 1

	Set @tableName = @contextInstanceDataBaseName + @partitionValue
	-- store the table name for future reference when building FK constraints from the Attribute, Alarm and Event 
	-- context instance tables
	Set @currentContextInstanceTableName = @tableName

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[ContextInstanceDataID] [bigint] IDENTITY('+CAST(@currentIdentityID as VARCHAR)+',1) NOT NULL,
		[PlanDataID] [int] NOT NULL,
		[ContextTime] [datetimeoffset](7) NOT NULL
	)'

	EXEC sp_executesql @SQL

	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([ContextInstanceDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'
				
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_CT' +
	N' ON ' + @tableName  + ' ([ContextTime] ASC) INCLUDE ([ContextInstanceDataID], [PlanDataID])'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END Context Instance Data table		
			
	-- BEGIN Context Instance Value Data table					
	Set @tableName = @contextInstanceValueDataBaseName + @partitionValue

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[ContextInstanceValueDataID] [int] IDENTITY(1,1) NOT NULL,
		[ContextInstanceDataID] [bigint] NOT NULL,
		[ValueDataTypeID] [int] NOT NULL,
		[ValueIdentifierDataID] [int] NOT NULL,
		[ValueInt] [int] NULL,
		[ValueFloat] [float] NULL,
		[ValueBit] [bit] NULL,
		[ValueString] [nvarchar](max) NULL
	)'

	EXEC sp_executesql @SQL

	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([ContextInstanceValueDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_'+@currentContextInstanceTableName+' FOREIGN KEY([ContextInstanceDataID])
			REFERENCES [dbo].['+@currentContextInstanceTableName+'] ([ContextInstanceDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ContextInstanceDataID constraint'								
						
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueDataType FOREIGN KEY([ValueDataTypeID])
			REFERENCES [dbo].[ValueDataType] ([ValueDataTypeID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueDataType constraint'							

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueIdentifierData FOREIGN KEY([ValueIdentifierDataID])
			REFERENCES [dbo].[ValueIdentifierData] ([ValueIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'	
			
	/****** Object:  Index [NC_IDX_ContextInstanceValueData_ValueIdentifierData]  ******/
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_VID_TD' +		
	N' ON ' + @tableName  + ' ([ValueIdentifierDataID] ASC, [ContextInstanceDataID] ASC)'
	EXEC sp_executesql @SQL			

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_CID' +		
	N' ON ' + @tableName  + ' ([ContextInstanceDataID] ASC) INCLUDE ([ContextInstanceValueDataID])'
	EXEC sp_executesql @SQL	

	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END Context Instance Value Data table		


	-- BEGIN TraceData table
	SET @previousTableName = @traceDataBaseName + @previousTableValue
	print @previousTableName	
	--Query for most recent entry
	SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
	EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;

	IF(@currentIdentityID is null)
		BEGIN
		--Check to see if table from an upgraded older DB exists
		IF(@RetentionType = 2)
			BEGIN
				SET @previousTableName = 'TraceData' + @UpdatedTableValue
				SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
				EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;
			END

		IF(@currentIdentityID is null)
			SET @currentIdentityID = @identityStartValue
		ELSE
			SET @currentIdentityID = @currentIdentityID + 1
		END
	ELSE 
		SET @currentIdentityID = @currentIdentityID + 1
						
	SET @tableName = @traceDataBaseName + @partitionValue
							
	set @sql = N'CREATE TABLE ['+@tableName+'] (
		[TraceDataID] [bigint] IDENTITY('+CAST(@currentIdentityID as VARCHAR)+',1) NOT NULL,
		[PlanDataID] [int] NOT NULL,
		[TraceID] [int] NOT NULL,
		[ReportTime] [datetimeoffset](7) NOT NULL,
		[CollectionTime] [datetimeoffset](7) NOT NULL
										
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([TraceDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_PlanData FOREIGN KEY([PlanDataID])
			REFERENCES [dbo].[PlanData] ([PlanDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PlanData constraint'	
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_CT' +		
	N' ON ' + @tableName  + ' (	[CollectionTime] )
	INCLUDE ( [TraceDataID],[PlanDataID] )'
	EXEC sp_executesql @SQL

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_RT_CT_PD_TD' +		
	N' ON ' + @tableName  + ' (	[ReportTime] DESC, [CollectionTime] DESC, [PlanDataID] ASC,	[TraceDataID] ASC)
	INCLUDE ([TraceID])'
	EXEC sp_executesql @SQL

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_PD_RT_TD_CT' +		
	N' ON ' + @tableName  + ' (	[PlanDataID] ASC, [ReportTime] ASC, [TraceDataID] ASC, [CollectionTime] ASC)
	INCLUDE ([TraceID])'
	EXEC sp_executesql @SQL
			
	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END TraceData table	

	-- BEGIN TraceValueData
	-- cache the current trace data table name, used when creating FK constraints on the trace value data table
	Set @currentParentTableName = @tableName

	Set @tableName = @traceValueDataBaseName + @partitionValue

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[TraceValueDataID] [int] IDENTITY(1,1) NOT NULL,
		[TraceDataID] [bigint] NOT NULL,
		[ValueDataTypeID] [int] NOT NULL,
		[ValueIdentifierDataID] [int] NOT NULL,
		[ValueInt] [int] NULL,
		[ValueFloat] [float] NULL,
		[ValueBit] [bit] NULL,
		[ValueString] [nvarchar](max) NULL,
		[ValueXML] [xml] NULL,
		[ValueDate] [datetimeoffset](7) NULL,
		[SerializedTypeDataID] [int] NULL	
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([TraceValueDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_SerializedTypeData FOREIGN KEY([SerializedTypeDataID])
			REFERENCES [dbo].[SerializedTypeData] ([SerializedTypeDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added SerializedTypeData constraint'
					
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_TraceData FOREIGN KEY([TraceDataID])
			REFERENCES [dbo].['+@currentParentTableName+'] ([TraceDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added TraceData constraint'	
						
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueDataType FOREIGN KEY([ValueDataTypeID])
			REFERENCES [dbo].[ValueDataType] ([ValueDataTypeID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueDataType constraint'							

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueIdentifierData FOREIGN KEY([ValueIdentifierDataID])
			REFERENCES [dbo].[ValueIdentifierData] ([ValueIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'										

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_TD_VALUES' +		
	N' ON ' + @tableName  + ' ([TraceDataID] ASC) INCLUDE ([TraceValueDataID], [ValueDataTypeID], [ValueIdentifierDataID], [ValueInt], [ValueFloat], [ValueBit], [ValueString], [ValueXML], [ValueDate], [SerializedTypeDataID])'
	EXEC sp_executesql @SQL	
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_TD' +		
	N' ON ' + @tableName  + ' ([TraceDataID] ASC) INCLUDE ([TraceValueDataID])'
	EXEC sp_executesql @SQL	

	/****** Object:  Index [NC_IDX_TraceValueData_ValueIdentifierData]  ******/
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_VID_TD' +		
	N' ON ' + @tableName  + ' ([ValueIdentifierDataID] ASC, [TraceDataID] ASC)'
	EXEC sp_executesql @SQL									

	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END TraceValueData

	-- BEGIN EventData table		
	SET @previousTableName = @eventDataBaseName + @previousTableValue
	print @previousTableName	
	--Query for most recent entry
	SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
	EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;

	IF(@currentIdentityID is null)
		BEGIN
		--Check to see if table from an upgraded older DB exists
		IF(@RetentionType = 2)
			BEGIN
				SET @previousTableName = 'EventData' + @UpdatedTableValue
				SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
				EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;
			END

		IF(@currentIdentityID is null)
			SET @currentIdentityID = @identityStartValue
		ELSE
			SET @currentIdentityID = @currentIdentityID + 1
		END
	ELSE 
		SET @currentIdentityID = @currentIdentityID + 1

	SET @tableName = @eventDataBaseName + @partitionValue
						
	set @sql = N'CREATE TABLE ['+@tableName+'] (
		[EventDataID] [bigint] IDENTITY('+CAST(@currentIdentityID as VARCHAR)+',1) NOT NULL,
		[PlanDataID] [int] NOT NULL,
		[EventIdentifierDataID] [int] NOT NULL,
		[EventTime] [datetimeoffset](7) NOT NULL,
		[ContextInstanceDataID] [bigint]  NULL
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([EventDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_PlanData FOREIGN KEY([PlanDataID])
			REFERENCES [dbo].[PlanData] ([PlanDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PlanData constraint'	
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_EventIdentifierData FOREIGN KEY([EventIdentifierDataID])
			REFERENCES [dbo].[EventIdentifierData] ([EventIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added EventIdentifierDataID constraint'	

	-- Creating foreign key on [ContextInstanceDataID] in table 'EventDatas'
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_ContextInstanceData'+@tableName+' FOREIGN KEY([ContextInstanceDataID])
			REFERENCES [dbo].['+@currentContextInstanceTableName+'] ([ContextInstanceDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ContextInstanceDataID constraint'	

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ET' +		
	N' ON ' + @tableName  + ' (	[EventTime] )
	INCLUDE ( [EventDataID],[PlanDataID] )'
	EXEC sp_executesql @SQL
									
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_PD_ET_ED_EID' +
	N' ON ' + @tableName  + ' ([PlanDataID] ASC, [EventTime] ASC, [EventDataID] ASC, [EventIdentifierDataID] ASC)'
	EXEC sp_executesql @SQL		
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_EID_ET_ED_PD' +
	N' ON ' + @tableName  + ' ([EventIdentifierDataID] ASC, [EventTime] ASC, [EventDataID] ASC, [PlanDataID] ASC)'
	EXEC sp_executesql @SQL		
	PRINT 'Table ' + @tableName + ' Added indexes'		

	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END EventData table

	-- BEGIN EventValueData
	-- cache the current event data table name, used when creating FK constraints.
	Set @currentParentTableName = @tableName

	Set @tableName = @eventValueDataBaseName + @partitionValue

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[EventValueDataID] [int] IDENTITY(1,1) NOT NULL,
		[EventDataID] [bigint] NOT NULL,
		[ValueDataTypeID] [int] NOT NULL,
		[IsContext] [bit] NOT NULL,
		[ValueIdentifierDataID] [int] NOT NULL,
		[ValueInt] [int] NULL,
		[ValueFloat] [float] NULL,
		[ValueBit] [bit] NULL,
		[ValueString] [nvarchar](max) NULL,
		[ValueXML] [xml] NULL,
		[ValueDate] [datetimeoffset](7) NULL,
		[SerializedTypeDataID] [int] NULL			
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([EventValueDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_SerializedTypeData FOREIGN KEY([SerializedTypeDataID])
			REFERENCES [dbo].[SerializedTypeData] ([SerializedTypeDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added SerializedTypeData constraint'
					
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_EventData FOREIGN KEY([EventDataID])
			REFERENCES [dbo].['+@currentParentTableName+'] ([EventDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added EventData constraint'	
						
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueDataType FOREIGN KEY([ValueDataTypeID])
			REFERENCES [dbo].[ValueDataType] ([ValueDataTypeID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueDataType constraint'							

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueIdentifierData FOREIGN KEY([ValueIdentifierDataID])
			REFERENCES [dbo].[ValueIdentifierData] ([ValueIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'			
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VI' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueInt])'
	EXEC sp_executesql @SQL	
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VF' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueFloat])'
	EXEC sp_executesql @SQL	
				
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VB' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueBit])'
	EXEC sp_executesql @SQL	

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VS' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueString])'
	EXEC sp_executesql @SQL	

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VX' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueXml])'
	EXEC sp_executesql @SQL	

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ED_EVD_VID_VD' +		
	N' ON ' + @tableName  + ' ([EventDataID] ASC, [EventValueDataID] ASC, [ValueIdentifierDataID] ASC) INCLUDE ([ValueDate])'
	EXEC sp_executesql @SQL	

	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_VID_ED' +		
	N' ON ' + @tableName  + ' ([ValueIdentifierDataID] ASC, [EventDataID] ASC)'
	EXEC sp_executesql @SQL	

	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END EventValueData table			

	-- BEGIN AttributeData table	
	SET @previousTableName = @attributeDataBaseName + @previousTableValue
	print @previousTableName	
	--Query for most recent entry
	SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
	EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;

	IF(@currentIdentityID is null)
		BEGIN
		--Check to see if table from an upgraded older DB exists
		IF(@RetentionType = 2)
			BEGIN
				SET @previousTableName = 'AttributeData' + @UpdatedTableValue
				SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
				EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;
			END

		IF(@currentIdentityID is null)
			SET @currentIdentityID = @identityStartValue
		ELSE
			SET @currentIdentityID = @currentIdentityID + 1
		END
	ELSE 
		SET @currentIdentityID = @currentIdentityID + 1
			
	SET @tableName = @attributeDataBaseName + @partitionValue
						
	set @sql = N'CREATE TABLE ['+@tableName+'] (
		[AttributeDataID] [bigint] IDENTITY('+CAST(@currentIdentityID as VARCHAR)+',1) NOT NULL,
		[PlanDataID] [int] NOT NULL,
		[AttributeTime] [datetimeoffset](7) NOT NULL,
		[ContextInstanceDataID] [bigint]  NULL				
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([AttributeDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_PlanData FOREIGN KEY([PlanDataID])
			REFERENCES [dbo].[PlanData] ([PlanDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PlanData constraint'	

	-- Creating foreign key on [ContextInstanceDataID] in table 'AttributeDatas'
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_ContextInstanceData'+@tableName+' FOREIGN KEY([ContextInstanceDataID])
			REFERENCES [dbo].['+@currentContextInstanceTableName+'] ([ContextInstanceDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PlanData constraint'				
														
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_AT' +
	N' ON ' + @tableName  + ' ([AttributeTime] ASC) INCLUDE ([AttributeDataID], [PlanDataID])'
	EXEC sp_executesql @SQL		
			
	PRINT 'Table ' + @tableName + ' Added indexes'

	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END AttributeData table	

	-- BEGIN AttributeValueData
	-- cache the current trace data table name, used when creating FK constraints.
	Set @currentParentTableName = @tableName

	Set @tableName = @attributeValueDataBaseName + @partitionValue

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[AttributeValueDataID] [int] IDENTITY(1,1) NOT NULL,
		[AttributeDataID] [bigint] NOT NULL,
		[ValueDataTypeID] [int] NOT NULL,
		[IsContext] [bit] NOT NULL,
		[IsOldPrimaryValue] [bit] NOT NULL,
		[IsNewPrimaryValue] [bit] NOT NULL,
		[ValueIdentifierDataID] [int] NOT NULL,
		[ValueInt] [int] NULL,
		[ValueFloat] [float] NULL,
		[ValueBit] [bit] NULL,
		[ValueString] [nvarchar](max) NULL,
		[ValueXML] [xml] NULL,
		[ValueDate] [datetimeoffset](7) NULL,
		[SerializedTypeDataID] [int] NULL				
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([AttributeValueDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_SerializedTypeData FOREIGN KEY([SerializedTypeDataID])
			REFERENCES [dbo].[SerializedTypeData] ([SerializedTypeDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added SerializedTypeData constraint'
					
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_AttributemData FOREIGN KEY([AttributeDataID])
			REFERENCES [dbo].['+@currentParentTableName+'] ([AttributeDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added AttributeData constraint'	
						
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueDataType FOREIGN KEY([ValueDataTypeID])
			REFERENCES [dbo].[ValueDataType] ([ValueDataTypeID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueDataType constraint'							

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueIdentifierData FOREIGN KEY([ValueIdentifierDataID])
			REFERENCES [dbo].[ValueIdentifierData] ([ValueIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'			
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_AD' +		
	N' ON ' + @tableName  + ' ([AttributeDataID] ASC) INCLUDE ([AttributeValueDataID])'
	EXEC sp_executesql @SQL	
			
	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END Attribute Value Data table			
			
	-- BEGIN AlarmData table		
	SET @previousTableName = @alarmDataBaseName + @previousTableValue
	print @previousTableName	
	--Query for most recent entry
	SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
	EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;

	IF(@currentIdentityID is null)
		BEGIN
		--Check to see if table from an upgraded older DB exists
		IF(@RetentionType = 2)
			BEGIN
				SET @previousTableName = 'AlarmData' + @UpdatedTableValue
				SET @sql = 'SELECT @currentIdentityID = IDENT_CURRENT ('''+@previousTableName+''')'
				EXEC sp_executesql @SQL, N'@currentIdentityID bigint OUTPUT', @currentIdentityID OUTPUT ;
			END

		IF(@currentIdentityID is null)
			SET @currentIdentityID = @identityStartValue
		ELSE
			SET @currentIdentityID = @currentIdentityID + 1
		END
	ELSE 
		SET @currentIdentityID = @currentIdentityID + 1

	SET @tableName = @alarmDataBaseName + @partitionValue
						
	set @sql = N'CREATE TABLE ['+@tableName+'] (
		[AlarmDataID] [bigint] IDENTITY('+CAST(@currentIdentityID as VARCHAR)+',1) NOT NULL,
		[PlanDataID] [int] NOT NULL,
		[AlarmIdentifierDataID] [int] NOT NULL,
		[Message] [nvarchar](255) NOT NULL,
		[ExceptionTime] [datetimeoffset](7) NOT NULL,
		[Severity] [nvarchar](50) NOT NULL,
		[State] [nvarchar](50) NOT NULL,
		[ExceptionType] [nvarchar](100) NOT NULL,
		[RequestedExceptionType] [nvarchar](100) NULL,
		[Enabled] [bit] NOT NULL,
		[ContextInstanceDataID] [bigint]  NULL
	)'

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT PK_'+@tableName+' PRIMARY KEY CLUSTERED ([AlarmDataID] ASC)'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_PlanData FOREIGN KEY([PlanDataID])
			REFERENCES [dbo].[PlanData] ([PlanDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PlanData constraint'	
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_AlarmIdentifierDataID FOREIGN KEY([AlarmIdentifierDataID])
			REFERENCES [dbo].[AlarmIdentifierData] ([AlarmIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added AlarmIdentifierDataID constraint'	

	-- Creating foreign key on [ContextInstanceDataID] in table 'AlarmDatas'
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_ContextInstanceData'+@tableName+' FOREIGN KEY([ContextInstanceDataID])
			REFERENCES [dbo].['+@currentContextInstanceTableName+'] ([ContextInstanceDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ContextInstanceDataID constraint'	
								
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName + '_ET' +
	N' ON ' + @tableName  + ' ([ExceptionTime] ASC) INCLUDE ([AlarmDataID], [PlanDataID])'
	EXEC sp_executesql @SQL		

	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END AlarmData table

	-- BEGIN AlarmValueData
	-- cache the current attribute data table name, used when creating FK constraints.
	Set @currentParentTableName = @tableName

	Set @tableName = @alarmValueDataBaseName + @partitionValue

	Set @sql = N'CREATE TABLE ['+@tableName+'] (
		[AlarmValueDataID] [int] IDENTITY(1,1) NOT NULL,
		[AlarmDataID] [bigint] NOT NULL,
		[ValueDataTypeID] [int] NOT NULL,
		[IsContext] [bit] NOT NULL,
		[ValueIdentifierDataID] [int] NOT NULL,
		[ValueInt] [int] NULL,
		[ValueFloat] [float] NULL,
		[ValueBit] [bit] NULL,
		[ValueString] [nvarchar](max) NULL,
		[ValueXML] [xml] NULL,
		[ValueDate] [datetimeoffset](7) NULL,
		[SerializedTypeDataID] [int] NULL				
	) '

	EXEC sp_executesql @SQL
			
	SET @sql = 'ALTER TABLE ['+@tableName+'] ADD CONSTRAINT '+@tableName+'PK PRIMARY KEY CLUSTERED ([AlarmValueDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added PK constraint'

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_SerializedTypeData FOREIGN KEY([SerializedTypeDataID])
			REFERENCES [dbo].[SerializedTypeData] ([SerializedTypeDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added SerializedTypeData constraint'
					
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_AlarmData FOREIGN KEY([AlarmDataID])
			REFERENCES [dbo].['+@currentParentTableName+'] ([AlarmDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added AlarmData constraint'	
						
	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueDataType FOREIGN KEY([ValueDataTypeID])
			REFERENCES [dbo].[ValueDataType] ([ValueDataTypeID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueDataType constraint'							

	SET @sql = 'ALTER TABLE ['+@tableName+'] WITH CHECK ADD CONSTRAINT FK_'+@tableName+'_ValueIdentifierData FOREIGN KEY([ValueIdentifierDataID])
			REFERENCES [dbo].[ValueIdentifierData] ([ValueIdentifierDataID])'
	EXEC sp_executesql @SQL
	PRINT 'Table ' + @tableName + ' Added ValueIdentifierData constraint'			
			
	Set @sql = N'
	SET QUOTED_IDENTIFIER ON
	CREATE NONCLUSTERED INDEX NC_IDX_' + @tableName +	'_AD' +		
	N' ON ' + @tableName  + ' ([AlarmDataID] ASC) INCLUDE ([AlarmValueDataID])'
	EXEC sp_executesql @SQL	
			
	PRINT 'Table ' + @tableName + ' Added indexes'
	SET @sql = 'GRANT SELECT, INSERT, UPDATE, DELETE ON ['+@tableName+'] TO db_eibstorage'
	EXEC sp_executesql @SQL		

	PRINT 'Table ' + @tableName + ' Granted user'
	-- END AlarmValueData				

	IF @RetentionTerm < 0
	RAISERROR (	'Specified retention term is invalid. Please enter a value greater than or equal to 0.', -- Message text.
				11, -- Severity.
				1 -- State.				
				);
	EXEC  dbo.DropPartitions @RetentionType, @RetentionTerm
	SET @ret = 1

END TRY
BEGIN CATCH
	DECLARE @errorMsg NVARCHAR(4000);
	DECLARE @errorSeverity INT;
	DECLARE @errorState INT;
	DECLARE @errorNumber INT;

	SELECT 
		@errorMsg = ERROR_MESSAGE(),
		@errorSeverity = ERROR_SEVERITY(),
		@errorState = ERROR_STATE(),
		@errorNumber = ERROR_NUMBER();

		--exit gracefully as necessary tables were already created
		IF (@errorNumber = 50000 AND @errorState = 0 AND @errorSeverity = 11)
			SELECT @ret = 2 --necessary tables already exist
		ELSE IF (@errorNumber = 50000 AND @errorState = 1 AND @errorSeverity = 11)
			SELECT @ret = 3 --ManagePartitions executed but will not be performing DropPartitions
		ELSE
			BEGIN
				-- Use RAISERROR inside the CATCH block to return error
				-- information about the original error that caused
				-- execution to jump to the CATCH block.
				RAISERROR (@errorMsg, -- Message text.
							@errorSeverity, -- Severity.
							@errorState -- State.
							);
			END
END CATCH; 
SELECT @ret AS ReturnValue, CAST(CAST(@RetentionType as varchar)+ RIGHT('00000000'+ CAST(@lastPartitionToCreate as varchar), 8) as int) as PartitionValue
END

GO


-- Autogenerated code ends here
--GRANT SELECT, INSERT ON AlarmData TO db_eibstorage
GO
GRANT SELECT, INSERT ON AlarmIdentifierData TO db_eibstorage
GO
GRANT SELECT, INSERT ON EventIdentifierData TO db_eibstorage
GO
GRANT SELECT, INSERT, UPDATE ON PlanData TO db_eibstorage
GO
GRANT SELECT, INSERT ON PlanDetailData TO db_eibstorage
GO
GRANT SELECT, INSERT ON SerializedTypeData TO db_eibstorage
GO
GRANT SELECT ON ValueDataType TO db_eibstorage
GO
GRANT SELECT, INSERT ON ValueIdentifierData TO db_eibstorage
GO
GRANT SELECT ON VersionData TO db_eibstorage
GO

GRANT EXECUTE ON GetEIBStorageDatabaseInfo TO db_eibstorage
GO
GRANT EXECUTE ON GetEIBStorageVersion TO db_eibstorage
GO
GRANT EXECUTE ON GetPlanDetailDataByPlanDetailHash TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAttributeData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAttributeValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertContextInstanceData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertContextInstanceValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertPlanData TO db_eibstorage
GO
GRANT EXECUTE ON InsertPlanDetailData TO db_eibstorage
GO
GRANT EXECUTE ON InsertTraceData TO db_eibstorage
GO
GRANT EXECUTE ON InsertTraceValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertValueDataType TO db_eibstorage
GO
GRANT EXECUTE ON InsertValueIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertVersionData TO db_eibstorage
GO
GRANT EXECUTE ON UpdatePlanData TO db_eibstorage
GO
GRANT EXECUTE ON GetEIBStorageDatabaseInfo TO db_eibstorage
GO
GRANT EXECUTE ON GetEIBStorageVersion TO db_eibstorage
GO
GRANT EXECUTE ON GetPlanDetailDataByPlanDetailHash TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAlarmValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAttributeData TO db_eibstorage
GO
GRANT EXECUTE ON InsertAttributeValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertContextInstanceData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertEventValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertPlanData TO db_eibstorage
GO
GRANT EXECUTE ON InsertPlanDetailData TO db_eibstorage
GO
GRANT EXECUTE ON InsertTraceData TO db_eibstorage
GO
GRANT EXECUTE ON InsertTraceValueData TO db_eibstorage
GO
GRANT EXECUTE ON InsertValueDataType TO db_eibstorage
GO
GRANT EXECUTE ON InsertValueIdentifierData TO db_eibstorage
GO
GRANT EXECUTE ON InsertVersionData TO db_eibstorage
GO
GRANT EXECUTE ON UpdatePlanData TO db_eibstorage
GO
GRANT EXECUTE ON ManagePartitions TO db_eibstorage
GO
GRANT EXECUTE ON DropPartitions TO db_eibstorage
GO
GRANT EXECUTE ON MaintainIndexes TO db_eibstorage
GO