----------------------------------------------------------------------
-- Copyright(C) The PEER Group Inc., 2018.
--
-- This software contains confidential and trade secret information
-- belonging to The PEER Group Inc. All Rights Reserved.
--
-- No part of this software may be reproduced or transmitted in any form
-- or by any means, electronic, mechanical, photocopying, recording or
-- otherwise, without the prior written consent of The PEER Group Inc.
-- --------------------------------------------------------------------

-- ---
-- Add system overrides here, and then reload the config.
-- ---
ALTER SYSTEM set timezone to 'UTC';
ALTER SYSTEM set log_timezone to 'UTC';
SELECT pg_reload_conf();
-- ---

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;

--
-- plan_detail_data
--
CREATE SEQUENCE pdd_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE pdd_sequence
    OWNER TO postgres;

CREATE TABLE plan_detail_data ( 
	plandetaildataid integer NOT NULL  DEFAULT nextval('pdd_sequence'::regclass),
	plandetail text NOT NULL,
	plandetailhash text NOT NULL
)
;

--
-- plan_data
--
CREATE SEQUENCE pd_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE pd_sequence
    OWNER TO postgres;

CREATE TABLE plan_data ( 
	plandataid integer NOT NULL DEFAULT nextval('pd_sequence'::regclass) PRIMARY KEY,
	plandetaildataid integer NOT NULL, 
	interfacename text NOT NULL,
	dataconsumername text NOT NULL,
	planname text NOT NULL,
	planid uuid NOT NULL,
	planactivationdate timestamp with time zone,
	plandeactivationdate timestamp with time zone,
	deactivationreason text,
	description text,
	retentiontype integer NOT NULL
)
;

--
-- hourly_context_instance_data
--
CREATE SEQUENCE cid_sequence_hourly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE cid_sequence_hourly
    OWNER TO postgres;
	
CREATE TABLE hourly_context_instance_data ( 
	contextinstancedataid bigint NOT NULL DEFAULT nextval('cid_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	contexttime timestamp with time zone NOT NULL,
	contextvalues jsonb
)
;

SELECT create_hypertable('hourly_context_instance_data', 'contexttime', chunk_time_interval => interval '1 hour');

--
-- daily_context_instance_data
--
CREATE SEQUENCE cid_sequence_daily
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE cid_sequence_daily
    OWNER TO postgres;
	
CREATE TABLE daily_context_instance_data ( 
	contextinstancedataid bigint NOT NULL DEFAULT nextval('cid_sequence_daily'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	contexttime timestamp with time zone NOT NULL,
	contextvalues jsonb
)
;

SELECT create_hypertable('daily_context_instance_data', 'contexttime', chunk_time_interval => interval '1 day');

--
-- monthly_context_instance_data
--
CREATE SEQUENCE cid_sequence_monthly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE cid_sequence_monthly
    OWNER TO postgres;
	
CREATE TABLE monthly_context_instance_data ( 
	contextinstancedataid bigint NOT NULL DEFAULT nextval('cid_sequence_monthly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	contexttime timestamp with time zone NOT NULL,
	contextvalues jsonb
)
;

SELECT create_hypertable('monthly_context_instance_data', 'contexttime', chunk_time_interval => interval '1 month');

--
-- alarm_identifier_data
--
CREATE SEQUENCE aid_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE aid_sequence
    OWNER TO postgres;

CREATE TABLE alarm_identifier_data ( 
	alarmidentifierdataid integer NOT NULL DEFAULT nextval('aid_sequence'::regclass) PRIMARY KEY,
	rootid text NOT NULL,
	sourceid text NOT NULL,
	exceptionid text NOT NULL,
	requestedsourceid text,
	requestedexceptionid text
)
;

--
-- hourly_alarm_data
--
CREATE SEQUENCE ald_sequence_hourly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE ald_sequence_hourly
    OWNER TO postgres;
	
CREATE TABLE hourly_alarm_data ( 
	alarmdataid bigint NOT NULL DEFAULT nextval('ald_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	alarmidentifierdataid integer NOT NULL REFERENCES alarm_identifier_data(alarmidentifierdataid),
	message text NOT NULL,
	exceptiontime timestamp with time zone NOT NULL,
	severity text NOT NULL,
	state text NOT NULL,
	exceptiontype text NOT NULL,
	requestedexceptiontype text,
	enabled boolean NOT NULL,
	contextinstancedataid bigint,
	alarmvalues jsonb
)
;

SELECT create_hypertable('hourly_alarm_data', 'exceptiontime', chunk_time_interval => interval '1 hour');

--
-- daily_alarm_data
--
CREATE SEQUENCE ald_sequence_daily
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE ald_sequence_daily
    OWNER TO postgres;

CREATE TABLE daily_alarm_data ( 
	alarmdataid bigint NOT NULL DEFAULT nextval('ald_sequence_daily'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	alarmidentifierdataid integer NOT NULL REFERENCES alarm_identifier_data(alarmidentifierdataid),
	message text NOT NULL,
	exceptiontime timestamp with time zone NOT NULL,
	severity text NOT NULL,
	state text NOT NULL,
	exceptiontype text NOT NULL,
	requestedexceptiontype text,
	enabled boolean NOT NULL,
	contextinstancedataid bigint,
	alarmvalues jsonb
)
;

SELECT create_hypertable('daily_alarm_data', 'exceptiontime', chunk_time_interval => interval '1 day');

--
-- monthly_alarm_data
--
CREATE SEQUENCE ald_sequence_monthly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE ald_sequence_monthly
    OWNER TO postgres;

CREATE TABLE monthly_alarm_data ( 
	alarmdataid bigint NOT NULL DEFAULT nextval('ald_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	alarmidentifierdataid integer NOT NULL REFERENCES alarm_identifier_data(alarmidentifierdataid),
	message text NOT NULL,
	exceptiontime timestamp with time zone NOT NULL,
	severity text NOT NULL,
	state text NOT NULL,
	exceptiontype text NOT NULL,
	requestedexceptiontype text,
	enabled boolean NOT NULL,
	contextinstancedataid bigint,
	alarmvalues jsonb
)
;

SELECT create_hypertable('monthly_alarm_data', 'exceptiontime', chunk_time_interval => interval '1 month');

--
-- hourly_attribute_data
--
CREATE SEQUENCE atd_sequence_hourly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE atd_sequence_hourly
    OWNER TO postgres;

CREATE TABLE hourly_attribute_data ( 
	attributedataid bigint NOT NULL DEFAULT nextval('atd_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	attributetime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	oldvalue jsonb,
	newvalue jsonb,
	reportvalues jsonb 
)
;

SELECT create_hypertable('hourly_attribute_data', 'attributetime', chunk_time_interval => interval '1 hour');

--
-- daily_attribute_data
--
CREATE SEQUENCE atd_sequence_daily
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE atd_sequence_daily
    OWNER TO postgres;
	
CREATE TABLE daily_attribute_data ( 
	attributedataid bigint NOT NULL DEFAULT nextval('atd_sequence_daily'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	attributetime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	oldvalue jsonb,
	newvalue jsonb,
	reportvalues jsonb 
)
;

SELECT create_hypertable('daily_attribute_data', 'attributetime', chunk_time_interval => interval '1 day');

--
-- montly_attribute_data
--
CREATE SEQUENCE atd_sequence_monthly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE atd_sequence_monthly
    OWNER TO postgres;


CREATE TABLE monthly_attribute_data ( 
	attributedataid bigint NOT NULL DEFAULT nextval('atd_sequence_monthly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	attributetime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	oldvalue jsonb,
	newvalue jsonb,
	reportvalues jsonb
)
;

SELECT create_hypertable('monthly_attribute_data', 'attributetime', chunk_time_interval => interval '1 month');

--
-- event_identifier_data
--
CREATE SEQUENCE eid_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE eid_sequence
    OWNER TO postgres;

CREATE TABLE event_identifier_data ( 
	eventidentifierdataid integer NOT NULL DEFAULT nextval('eid_sequence'::regclass) PRIMARY KEY,
	rootid text NOT NULL,
	sourceid text NOT NULL,
	eventid text NOT NULL
)
;

--
-- hourly_event_data
--
CREATE SEQUENCE evd_sequence_hourly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE evd_sequence_hourly
    OWNER TO postgres;

CREATE TABLE hourly_event_data ( 
	eventdataid bigint NOT NULL DEFAULT nextval('evd_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	eventidentifierdataid integer NOT NULL REFERENCES event_identifier_data(eventidentifierdataid),
	eventtime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	eventvalues jsonb
)
;

CREATE INDEX hourly_event_data_eventidentifierdataid_idx
    ON hourly_event_data USING btree
    (eventidentifierdataid);

SELECT create_hypertable('hourly_event_data', 'eventtime', chunk_time_interval => interval '1 hour');

--
-- daily_event_data
--
CREATE SEQUENCE evd_sequence_daily
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE evd_sequence_daily
    OWNER TO postgres;

CREATE TABLE daily_event_data ( 
	eventdataid bigint NOT NULL DEFAULT nextval('evd_sequence_daily'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	eventidentifierdataid integer NOT NULL REFERENCES event_identifier_data(eventidentifierdataid),
	eventtime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	eventvalues jsonb
)
;

CREATE INDEX daily_event_data_eventidentifierdataid_idx
    ON daily_event_data USING btree
    (eventidentifierdataid);

SELECT create_hypertable('daily_event_data', 'eventtime', chunk_time_interval => interval '1 day');

--
-- monthly_event_data
--
CREATE SEQUENCE evd_sequence_monthly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE evd_sequence_monthly
    OWNER TO postgres;

CREATE TABLE monthly_event_data ( 
	eventdataid bigint NOT NULL DEFAULT nextval('evd_sequence_monthly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	eventidentifierdataid integer NOT NULL REFERENCES event_identifier_data(eventidentifierdataid),
	eventtime timestamp with time zone NOT NULL,
	contextinstancedataid bigint,
	eventvalues jsonb
)
;

CREATE INDEX monthly_event_data_eventidentifierdataid_idx
    ON monthly_event_data USING btree
    (eventidentifierdataid);

SELECT create_hypertable('monthly_event_data', 'eventtime', chunk_time_interval => interval '1 month');

--
-- hourly_trace_data
--
CREATE SEQUENCE trd_sequence_hourly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE trd_sequence_hourly
    OWNER TO postgres;

CREATE TABLE hourly_trace_data ( 
	tracedataid bigint NOT NULL DEFAULT nextval('trd_sequence_hourly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	traceid integer NOT NULL,
	reporttime timestamp with time zone NOT NULL,
	collectiontime timestamp with time zone NOT NULL,
	tracevalues jsonb
)
;

SELECT create_hypertable('hourly_trace_data', 'reporttime', chunk_time_interval => interval '1 hour');

--
-- daily_trace_data
--
CREATE SEQUENCE trd_sequence_daily
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE trd_sequence_daily
    OWNER TO postgres;

CREATE TABLE daily_trace_data ( 
	tracedataid bigint NOT NULL DEFAULT nextval('trd_sequence_daily'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	traceid integer NOT NULL,
	reporttime timestamp with time zone NOT NULL,
	collectiontime timestamp with time zone NOT NULL,
	tracevalues jsonb
)
;

SELECT create_hypertable('daily_trace_data', 'reporttime', chunk_time_interval => interval '1 day');

--
-- monthly_trace_data
--
CREATE SEQUENCE trd_sequence_monthly
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE trd_sequence_monthly
    OWNER TO postgres;

CREATE TABLE monthly_trace_data ( 
	tracedataid bigint NOT NULL DEFAULT nextval('trd_sequence_monthly'::regclass),
	plandataid integer NOT NULL REFERENCES plan_data(plandataid),
	traceid integer NOT NULL,
	reporttime timestamp with time zone NOT NULL,
	collectiontime timestamp with time zone NOT NULL,
	tracevalues jsonb
)
;

SELECT create_hypertable('monthly_trace_data', 'reporttime', chunk_time_interval => interval '1 month');

--
-- value_data_type
---
CREATE SEQUENCE vdt_sequence
    INCREMENT 1
    START 100    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE vdt_sequence
    OWNER TO postgres;

CREATE TABLE value_data_type ( 
	valuedatatypeid integer NOT NULL DEFAULT nextval('vdt_sequence'::regclass) PRIMARY KEY,
	type text NOT NULL,
	format integer NOT NULL
)
;

INSERT INTO value_data_type(valuedatatypeid, type, format)
VALUES
(1, 'Null', 0),
(2, 'Short', 0),
(3, 'UnsignedShort', 0),
(4, 'Integer', 0),
(5, 'UnsignedInteger', 0),
(6, 'Byte', 0),
(7, 'Float', 0),
(8, 'Double', 0),
(9, 'Boolean', 0),
(10, 'String', 0),
(11, 'DateTimeOffset', 0),
(12, 'DateTime', 0),
(13, 'LocalDate', 0),
(14, 'GMTDate', 0),
(15, 'SignedByte', 0),
(16, 'Long', 0),
(17, 'UnsignedLong', 0)
;

--
-- value_identifier_data
--
CREATE SEQUENCE vid_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE vid_sequence
    OWNER TO postgres;

CREATE TABLE value_identifier_data ( 
	valueidentifierdataid integer NOT NULL DEFAULT nextval('vid_sequence'::regclass) PRIMARY KEY,
	rootid text NOT NULL,
	sourceid text NOT NULL,
	parametername text NOT NULL,
	valuedatatypeid integer NOT NULL REFERENCES value_data_type(valuedatatypeid),
	iscontext bool NOT NULL
)
;

--
-- version_data
--
CREATE SEQUENCE vd_sequence
    INCREMENT 1
    START 1    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE vd_sequence
    OWNER TO postgres;

CREATE TABLE version_data ( 
	versiondataid integer NOT NULL DEFAULT nextval('vd_sequence'::regclass) PRIMARY KEY,
	versionnumber text NOT NULL,
	versionname text NOT NULL,
	previousversionnumber text,
	dateupgraded timestamp with time zone	
)
;

INSERT INTO public.version_data(versionnumber, versionname)
	VALUES ('7.8.0.0', 'EIB 7.8')
;

--
-- Functions
--
CREATE OR REPLACE FUNCTION get_value_identifier_data_id(_rootid text, _sourceid text, _parametername text, _valuedatatype int, _iscontext bool) RETURNS integer AS
$func$
DECLARE _vid integer = 0;
BEGIN	
	LOCK TABLE value_identifier_data IN ACCESS EXCLUSIVE MODE;
  
	SELECT valueidentifierdataid
	FROM   value_identifier_data 
	WHERE  rootid=_rootid AND sourceid=_sourceid AND parametername=_parametername AND valuedatatypeid=_valuedatatype AND iscontext = _iscontext	
	INTO   _vid;

	IF (_vid is null) THEN
		BEGIN
			INSERT INTO value_identifier_data(rootid, sourceid, parametername, valuedatatypeid, iscontext)    
			VALUES (_rootid, _sourceid, _parametername, _valuedatatype, _iscontext)
			RETURNING valueidentifierdataid INTO _vid;			
		END;
	END IF;
	
	RETURN _vid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;

CREATE OR REPLACE FUNCTION get_value_data_type_id(_type text, _format integer) RETURNS integer AS
$func$
DECLARE _vdtid integer = 0;
BEGIN	
	LOCK TABLE value_data_type IN ACCESS EXCLUSIVE MODE;
  
	SELECT valuedatatypeid
	FROM   value_data_type 
	WHERE  type=_type AND format=_format
	INTO   _vdtid;

	IF (_vdtid is null) THEN
		BEGIN
			INSERT INTO value_data_type(type, format)
			VALUES (_type, _format)
			RETURNING valuedatatypeid INTO _vdtid;			
		END;
	END IF;
	
	RETURN _vdtid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;

CREATE OR REPLACE FUNCTION get_alarm_identifier_data_id(_rootid text, _sourceid text, _exceptionid text, _requestedsourceid text, _requestedexceptionid text) RETURNS integer AS
$func$
DECLARE _alid integer = 0;
BEGIN	
	LOCK TABLE alarm_identifier_data IN ACCESS EXCLUSIVE MODE;
  
	SELECT alarmidentifierdataid
	FROM   alarm_identifier_data 
	WHERE  rootid=_rootid AND sourceid=_sourceid AND exceptionid=_exceptionid AND requestedsourceid=_requestedsourceid AND requestedexceptionid=_requestedexceptionid
	INTO   _alid;

	IF (_alid is null) THEN
		BEGIN
			INSERT INTO alarm_identifier_data(rootid, sourceid, exceptionid, requestedsourceid, requestedexceptionid)
			VALUES (_rootid, _sourceid, _exceptionid, _requestedsourceid, _requestedexceptionid)
			RETURNING alarmidentifierdataid INTO _alid;			
		END;
	END IF;
	
	RETURN _alid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;

CREATE OR REPLACE FUNCTION get_event_identifier_data_id(_rootid text, _sourceid text, _eventid text) RETURNS integer AS
$func$
DECLARE _evid integer = 0;
BEGIN	
	LOCK TABLE event_identifier_data IN ACCESS EXCLUSIVE MODE;
  
	SELECT eventidentifierdataid
	FROM   event_identifier_data 
	WHERE  rootid=_rootid AND sourceid=_sourceid AND eventid=_eventid
	INTO   _evid;

	IF (_evid is null) THEN
		BEGIN
			INSERT INTO event_identifier_data(rootid, sourceid, eventid)
			VALUES (_rootid, _sourceid, _eventid)
			RETURNING eventidentifierdataid INTO _evid;			
		END;
	END IF;
	
	RETURN _evid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;

CREATE OR REPLACE FUNCTION get_plan_detail_data_id(_plandetail text, _plandetailhash text) RETURNS integer AS
$func$
DECLARE _pddid integer = 0;
BEGIN	
	LOCK TABLE plan_detail_data IN ACCESS EXCLUSIVE MODE;
  
	SELECT plandetaildataid
	FROM   plan_detail_data 
	WHERE  plandetailhash=_plandetailhash
	INTO   _pddid;

	IF (_pddid is null) THEN
		BEGIN
			INSERT INTO plan_detail_data(plandetail, plandetailhash)
			VALUES (_plandetail, _plandetailhash)
			RETURNING plandetaildataid INTO _pddid;			
		END;
	END IF;
	
	RETURN _pddid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;

CREATE OR REPLACE FUNCTION get_context_instance_data_id(_tablename text, _contextinstancedataid bigint, _plandataid integer, _contexttime timestamp with time zone, _contextvalues jsonb) RETURNS bigint AS
$func$
DECLARE _cidid bigint = 0;

BEGIN	
	EXECUTE format('LOCK TABLE %s IN ACCESS EXCLUSIVE MODE', _tablename);
	
  	EXECUTE format('SELECT contextinstancedataid FROM %s WHERE contextinstancedataid = %L', _tablename, _contextinstancedataid )
   	INTO _cidid;

	IF (_cidid is null) THEN
		BEGIN
			EXECUTE format('INSERT INTO %s(contextinstancedataid, plandataid, contexttime, contextvalues) VALUES (%L, %L, %L, %L) RETURNING contextinstancedataid',
					   _tablename, _contextinstancedataid, _plandataid, _contexttime, _contextvalues)
			INTO  _cidid;
		END;
	END IF;
	
	RETURN _cidid;
END;
$func$ LANGUAGE plpgsql PARALLEL UNSAFE
;