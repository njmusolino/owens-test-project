﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="FB_DblSol_OC" Id="{afdd8cf7-c7d9-4ff8-a1c6-b916b08e213e}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_DblSol_OC	// Double-solenoid, two-state devices--Open-Close labels

//=========== Control section ===========
VAR_INPUT
	Init			: BOOL;	// Match outputs to sensor inputs, Reset errors
	Reset 			: BOOL;	// Reset requests
	ResetOnErr		: BOOL;	// Reset outputs on Error
	SolDelay 		: TIME; // Switching delay
	Go_Open 		: BOOL; // Open conditions
	Go_Close 		: BOOL; // Close conditions
END_VAR

VAR_OUTPUT
	Open_Sol 		: BOOL; // Open request to solenoid output
	Close_Sol 		: BOOL; // Close request to solenoid output
	Open_ResetPls 	: BOOL; // Pulse with Open condition
	Close_ResetPls 	: BOOL; // Pulse with Close condition
END_VAR

VAR
	Open_Req		: BOOL;	// Start the Open sequence
	Close_Req		: BOOL;	// Start the Close sequence
	OpenOffDwell 	: TON;
	CloseOffDwell 	: TON;
	P_OpenResetPls	: R_TRIG;
	P_CloseResetPls	: R_TRIG;
END_VAR

//=========== Status section ===========
VAR_INPUT
	CheckErr 		: BOOL;	// Check for Timeout errors
	LatchErr 		: BOOL;	// Latch Error status outputs 
	Opened_Sens 	: BOOL;	// Opened sensor for status
	Closed_Sens 	: BOOL;	// Closed sensor for status
	ErrDelay 		: TIME;	// Move timeout error delay
END_VAR

VAR_OUTPUT
	Status 			: INT;	// Status word
	IsOpened 		: BOOL;	// Status: Opened position
	IsClosed 		: BOOL;	// Status: Closed position
	Opening 		: BOOL;	// Status: Moving to Opened position
	Closing 		: BOOL;	// Status: Moving to Closed position
	TimeoutOpen 	: BOOL;	// Status: Open move timeout error
	TimeoutClose 	: BOOL;	// Status: Close move timeout error
	OpErr 			: BOOL;	// Status: Operational error (both sens. or sol. on)
	ErrPresent 		: BOOL;	// Status: Any error present
END_VAR

VAR
	Open_Timeout 	: TON;
	Close_Timeout 	: TON;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[//------ Double-solenoid, two-state devices (Up/Down, In/Out, Open/Close, etc.)

//  Double-solenoids--use separate open/close type signals so that movement
//  doesn't happen until requested. (No movement with both solenoids off.) This
//  also ensures that both solenoids won't be on at the same time.
//
//  If the mechanism doesn't properly vent the off-state solenoid when energizing
//  the on-state, use a delay between the operations. To disable the delay, set the
//  delay time to zero, or leave it disconnected.
//
//  The one-shot pulse outputs on open or close conditions can be used to reset
//  input variables to act as a momentary-type input signal.

// Init: Match outputs to inputs (from off state)
IF Init THEN
	Open_Req := FALSE;
	Close_Req := FALSE;
	// Go to the Up position
	IF Opened_Sens AND NOT Closed_Sens THEN
		Open_Req := TRUE;
	END_IF
	// Go to the Down position
	IF NOT Opened_Sens AND Closed_Sens THEN
		Close_Req := TRUE;
	END_IF
END_IF

// External Open/Close requests
IF (Go_Open AND NOT Go_Close) AND NOT ErrPresent THEN
	Open_Req := TRUE;
	Close_Req := FALSE;
END_IF
P_OpenResetPls(CLK := Open_Req);			// Set up a rising-edge pulse for the reset output
Open_ResetPls := P_OpenResetPls.Q;

IF (Go_Close AND NOT Go_Open) AND NOT ErrPresent THEN
	Open_Req := FALSE;
	Close_Req := TRUE;
END_IF
P_CloseResetPls(CLK := Close_Req);			// Set up a rising-edge pulse for the reset output
Close_ResetPls := P_CloseResetPls.Q;

// Dwell timers for moving from one position to another (if needed)
OpenOffDwell(IN := NOT Open_Req, PT := SolDelay);
CloseOffDwell(IN := NOT Close_Req, PT := SolDelay);

// Open condition
IF Open_Req THEN
	Close_Sol := FALSE;
	IF CloseOffDwell.Q THEN
		Open_Sol := TRUE;
	END_IF
END_IF

// Close condition
IF Close_Req THEN
	Open_Sol := FALSE;
	IF OpenOffDwell.Q THEN
		Close_Sol := TRUE;
	END_IF
END_IF

// External (or error) reset of both outputs
IF Reset OR (ResetOnErr AND ErrPresent) THEN
	Open_Req := FALSE;
	Close_Req := FALSE;
	Open_Sol := FALSE;
	Close_Sol := FALSE;
END_IF


//  ------ Status for double-solenoid, two-state mechanisms (Up/Down, Open/Close, In/Out, etc.)
//
//  Set the status based on output and sensor states. If a sensor is on, but
//  the corresponding solenoid is off, show it as undetermined or error, since
//  the mechanism may have drifted from the true in-position while still in the
//  sensor's hysteresis zone.
//
//  Status Word (only one bit on at a time):
//		no bits on (value=0) = Undetermined
//		b0 (value=1) = Opened
//		b1 (value=2) = Closed
//		b2 (value=4) = Moving to Opened position
//		b3 (value=8) = Moving to Closed position
//		b4 (value=16) = Open Move Timeout error
//		b5 (value=32) = Close Move Timeout error
//		b6 (value=64) = Operational Error (both sensors or both solenoids on)
//
//  Either the status word or the output status bits can be used, depending
//  on what information is needed.
//
//  Move Timeout errors:
//  Allow the timeout errors to be latching, or not, via the LatchErr bit. The latched
//  errors can be used so that a slow move that causes a timeout won't have its status
//  eventually change to Opened/Closed, possibly releasing other logic and movement.
//  
//  If used as latching, toggle the CheckErr bit to unlatch/reset the error. The error
//  bits can be used to set alarms directly out of the FB, or used as a condition for
//  standard alarm handling.
//  
//  Error status checking can be disabled by setting the CheckErr bit to false (or leave disconnected).
//

// Status value 0 (no bits on): Undetermined
IF NOT Open_Req AND NOT Close_Req THEN Status := 0; END_IF

// Status bit 0 (value 1): Opened position
IF Open_Req AND Opened_Sens THEN Status := 1; END_IF

// Status bit 1 (value 2): Closed position
IF Close_Req AND Closed_Sens THEN Status := 2; END_IF

// Status bit 2 (value 4): Moving to Opened position
IF Open_Req AND NOT Opened_Sens THEN Status := 4; END_IF

// Status bit 3 (value 8): Moving to Closed position
IF Close_Req AND NOT Closed_Sens THEN Status := 8; END_IF

// Status bit 4 (value 16): Check for Open move timeout
Open_Timeout.IN := Checkerr AND ((Open_Req AND NOT Opened_Sens) OR (LatchErr AND NOT Init AND Open_Timeout.Q));	// Reset latched error on Init
Open_Timeout(PT := ErrDelay);
// Set the status value for Open move timeout error
IF Open_Timeout.Q THEN Status := 16; END_IF

// Status bit 5 (value 32): Check for Close move timeout
Close_Timeout.IN := Checkerr AND ((Close_Req AND NOT Closed_Sens) OR (LatchErr AND NOT Init AND Close_Timeout.Q));	// Reset latched error on Init
Close_Timeout(PT := ErrDelay);
// Set the status value for Close move timeout error
IF Close_Timeout.Q THEN Status := 32; END_IF

// Status bit 6 (value 64): Operational error (both solenoids or both sensors on at the same time)
IF CheckErr AND ((Open_Req AND Close_Req) OR (Opened_Sens AND Closed_Sens)) THEN Status := 64; END_IF

// Set the individual status bits based on the status word
IsOpened := Status.0;
IsClosed := Status.1;
Opening := Status.2;
Closing := Status.3;
TimeoutOpen := Status.4;
TimeoutClose := Status.5;
OpErr := Status.6;

ErrPresent := TimeoutOpen OR TimeoutClose OR OpErr;
]]></ST>
    </Implementation>
    <LineIds Name="FB_DblSol_OC">
      <LineId Id="137" Count="9" />
      <LineId Id="149" Count="1" />
      <LineId Id="443" Count="0" />
      <LineId Id="445" Count="12" />
      <LineId Id="365" Count="0" />
      <LineId Id="442" Count="0" />
      <LineId Id="368" Count="1" />
      <LineId Id="376" Count="0" />
      <LineId Id="373" Count="2" />
      <LineId Id="366" Count="0" />
      <LineId Id="377" Count="5" />
      <LineId Id="386" Count="0" />
      <LineId Id="154" Count="0" />
      <LineId Id="130" Count="0" />
      <LineId Id="156" Count="0" />
      <LineId Id="175" Count="0" />
      <LineId Id="131" Count="1" />
      <LineId Id="157" Count="0" />
      <LineId Id="159" Count="2" />
      <LineId Id="158" Count="0" />
      <LineId Id="134" Count="0" />
      <LineId Id="176" Count="6" />
      <LineId Id="135" Count="0" />
      <LineId Id="185" Count="1" />
      <LineId Id="384" Count="1" />
      <LineId Id="187" Count="0" />
      <LineId Id="189" Count="0" />
      <LineId Id="188" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="136" Count="0" />
      <LineId Id="67" Count="7" />
      <LineId Id="310" Count="0" />
      <LineId Id="75" Count="9" />
      <LineId Id="86" Count="17" />
      <LineId Id="107" Count="2" />
      <LineId Id="235" Count="2" />
      <LineId Id="110" Count="17" />
      <LineId Id="249" Count="0" />
      <LineId Id="128" Count="1" />
      <LineId Id="246" Count="2" />
      <LineId Id="252" Count="0" />
      <LineId Id="251" Count="0" />
      <LineId Id="245" Count="0" />
      <LineId Id="256" Count="0" />
      <LineId Id="9" Count="0" />
      <LineId Id="257" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>