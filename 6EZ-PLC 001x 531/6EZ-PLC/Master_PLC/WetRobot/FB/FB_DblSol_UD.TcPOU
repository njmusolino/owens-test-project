﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="FB_DblSol_UD" Id="{0251c07a-9515-44b2-800d-02549a02d6a4}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_DblSol_UD	// Double-solenoid, two-state devices--Up-Down labels

//=========== Control section ===========
VAR_INPUT
	Init			: BOOL;	// Match outputs to sensor inputs, Reset errors
	Reset 			: BOOL;	// Reset requests
	ResetOnErr		: BOOL;	// Reset outputs on Error
	SolDelay 		: TIME; // Switching delay
	Go_Up 			: BOOL; // Up conditions
	Go_Down 		: BOOL; // Down conditions
END_VAR

VAR_OUTPUT
	Up_Sol 			: BOOL; // Up request to solenoid output
	Down_Sol 		: BOOL; // Down request to solenoid output
	Up_ResetPls 	: BOOL; // Pulse with Up condition
	Down_ResetPls 	: BOOL; // Pulse with Down condition
END_VAR

VAR
	Up_Req			: BOOL;	// Start the Up sequence
	Down_Req		: BOOL;	// Start the Down sequence
	UpOffDwell 		: TON;
	DownOffDwell 	: TON;
	P_UpResetPls	: R_TRIG;
	P_DownResetPls	: R_TRIG;
END_VAR

//=========== Status section ===========
VAR_INPUT
	CheckErr 		: BOOL;	// Check for Timeout errors
	LatchErr 		: BOOL;	// Latch Error status outputs 
	Up_Sens 		: BOOL;	// Up sensor for status
	Down_Sens 		: BOOL;	// Down sensor for status
	ErrDelay 		: TIME;	// Move timeout error delay
END_VAR

VAR_OUTPUT
	Status 			: INT;	// Status word
	IsUp 			: BOOL;	// Status: Up position
	IsDown 			: BOOL;	// Status: Down position
	MovingUp 		: BOOL;	// Status: Moving to Up position
	MovingDown 		: BOOL;	// Status: Moving to Down position
	TimeoutUp 		: BOOL;	// Status: Up move timeout error
	TimeoutDown 	: BOOL;	// Status: Down move timeout error
	OpErr 			: BOOL;	// Status: Operational error (both sens. or sol. on)
	ErrPresent 		: BOOL;	// Status: Any error present
END_VAR

VAR
	Up_Timeout 		: TON;
	Down_Timeout 	: TON;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[//------ Double-solenoid, two-state devices (Up/Down, In/Out, Open/Close, etc.)

//  Double-solenoids--use separate up/down type signals so that movement
//  doesn't happen until requested. (No movement with both solenoids off.) This
//  also ensures that both solenoids won't be on at the same time.
//
//  If the mechanism doesn't properly vent the off-state solenoid when energizing
//  the on-state, use a delay between the operations. To disable the delay, set the
//  delay time to zero, or leave it disconnected.
//
//  The one-shot pulse outputs on up or down conditions can be used to reset
//  input variables to act as a momentary-type input signal.

// Init: Match outputs to inputs (from off state)
IF Init THEN
	Up_Req := FALSE;
	Down_Req := FALSE;
	// Go to the Up position
	IF Up_Sens AND NOT Down_Sens THEN
		Up_Req := TRUE;
	END_IF
	// Go to the Down position
	IF NOT Up_Sens AND Down_Sens THEN
		Down_Req := TRUE;
	END_IF
END_IF

// External Up/Down requests
IF (Go_Up AND NOT Go_Down) AND NOT ErrPresent THEN
	Up_Req := TRUE;
	Down_Req := FALSE;
END_IF
P_UpResetPls(CLK := Up_Req);			// Set up a rising-edge pulse for the reset output
Up_ResetPls := P_UpResetPls.Q;

IF (Go_Down AND NOT Go_Up) AND NOT ErrPresent THEN
	Up_Req := FALSE;
	Down_Req := TRUE;
END_IF
P_DownResetPls(CLK := Down_Req);		// Set up a rising-edge pulse for the reset output
Down_ResetPls := P_DownResetPls.Q;

// Dwell timers for moving from one position to another (if needed)
UpOffDwell(IN := NOT Up_Req, PT := SolDelay);
DownOffDwell(IN := NOT Down_Req, PT := SolDelay);

//  Up condition
IF Up_Req THEN
	Down_Sol := FALSE;
	IF DownOffDwell.Q THEN
		Up_Sol := TRUE;
	END_IF
END_IF

//  Down condition
IF Down_Req THEN
	Up_Sol := FALSE;
	IF UpOffDwell.Q THEN
		Down_Sol := TRUE;
	END_IF
END_IF

// External (or error) reset of both outputs
IF Reset OR (ResetOnErr AND ErrPresent) THEN
	Up_Req := FALSE;
	Down_Req := FALSE;
	Up_Sol := FALSE;
	Down_Sol := FALSE;
END_IF


//  ------ Status for double-solenoid, two-state mechanisms (Up/Down, Open/Close, In/Out, etc.)
//
//  Set the status based on output and sensor states. If a sensor is on, but
//  the corresponding solenoid is off, show it as undetermined or error, since
//  the mechanism may have drifted from the true in-position while still in the
//  sensor's hysteresis zone.
//
//  Status Word (only one bit on at a time):
//		no bits on (value=0) = Undetermined
//		b0 (value=1) = Up
//		b1 (value=2) = Down
//		b2 (value=4) = Moving Up
//		b3 (value=8) = Moving Down
//		b4 (value=16) = Up Move Timeout error
//		b5 (value=32) = Down Move Timeout error
//		b6 (value=64) = Operational Error (both sensors or both solenoids on)
//
//  Either the status word or the output status bits can be used, depending
//  on what information is needed.
//
//  Move Timeout errors:
//  Allow the timeout errors to be latching, or not, via the LatchErr bit. The latched
//  errors can be used so that a slow move that causes a timeout won't have its status
//  eventually change to Up/Down, possibly releasing other logic and movement.
//  
//  If used as latching, toggle the CheckErr bit to unlatch/reset the error. The error
//  bits can be used to set alarms directly out of the FB, or used as a condition for
//  standard alarm handling.
//  
//  Error status checking can be disabled by setting the CheckErr bit to false (or leave disconnected).
//

// Status value 0 (no bits on): Undetermined
IF NOT Up_Req AND NOT Down_Req THEN Status := 0; END_IF

// Status bit 0 (value 1): Up position
IF Up_Req AND Up_Sens THEN Status := 1; END_IF

// Status bit 1 (value 2): Down position
IF Down_Req AND Down_Sens THEN Status := 2; END_IF

// Status bit 2 (value 4): Moving to Up position
IF Up_Req AND NOT Up_Sens THEN Status := 4; END_IF

// Status bit 3 (value 8): Moving to Down position
IF Down_Req AND NOT Down_Sens THEN Status := 8; END_IF

// Status bit 4 (value 16): Check for Up move timeout
Up_Timeout.IN := Checkerr AND ((Up_Req AND NOT Up_Sens) OR (LatchErr AND NOT Init AND Up_Timeout.Q));	// Reset latched error on Init
Up_Timeout(PT := ErrDelay);
// Set the status value for Up move timeout error
IF Up_Timeout.Q THEN Status := 16; END_IF

// Status bit 5 (value 32): Check for Down move timeout
Down_Timeout.IN := Checkerr AND ((Down_Req AND NOT Down_Sens) OR (LatchErr AND NOT Init AND Down_Timeout.Q));	// Reset latched error on Init
Down_Timeout(PT := ErrDelay);
// Set the status value for Down move timeout error
IF Down_Timeout.Q THEN Status := 32; END_IF

// Status bit 6 (value 64): Operational error (both solenoids or both sensors on at the same time)
IF CheckErr AND ((Up_Req AND Down_Req) OR (Up_Sens AND Down_Sens)) THEN Status := 64; END_IF

// Set the individual status bits based on the status word
IsUp := Status.0;
IsDown := Status.1;
MovingUp := Status.2;
MovingDown := Status.3;
TimeoutUp := Status.4;
TimeoutDown := Status.5;
OpErr := Status.6;

ErrPresent := TimeoutUp OR TimeoutDown OR OpErr;
]]></ST>
    </Implementation>
    <LineIds Name="FB_DblSol_UD">
      <LineId Id="137" Count="9" />
      <LineId Id="149" Count="1" />
      <LineId Id="463" Count="0" />
      <LineId Id="465" Count="3" />
      <LineId Id="470" Count="7" />
      <LineId Id="482" Count="0" />
      <LineId Id="464" Count="0" />
      <LineId Id="154" Count="0" />
      <LineId Id="367" Count="12" />
      <LineId Id="365" Count="0" />
      <LineId Id="380" Count="0" />
      <LineId Id="130" Count="0" />
      <LineId Id="156" Count="0" />
      <LineId Id="175" Count="0" />
      <LineId Id="131" Count="1" />
      <LineId Id="157" Count="0" />
      <LineId Id="159" Count="2" />
      <LineId Id="158" Count="0" />
      <LineId Id="134" Count="0" />
      <LineId Id="176" Count="6" />
      <LineId Id="135" Count="0" />
      <LineId Id="381" Count="3" />
      <LineId Id="388" Count="1" />
      <LineId Id="387" Count="0" />
      <LineId Id="190" Count="0" />
      <LineId Id="136" Count="0" />
      <LineId Id="67" Count="7" />
      <LineId Id="310" Count="0" />
      <LineId Id="75" Count="9" />
      <LineId Id="86" Count="17" />
      <LineId Id="107" Count="2" />
      <LineId Id="235" Count="2" />
      <LineId Id="110" Count="17" />
      <LineId Id="249" Count="0" />
      <LineId Id="128" Count="1" />
      <LineId Id="246" Count="2" />
      <LineId Id="252" Count="0" />
      <LineId Id="251" Count="0" />
      <LineId Id="245" Count="0" />
      <LineId Id="256" Count="0" />
      <LineId Id="9" Count="0" />
      <LineId Id="257" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>