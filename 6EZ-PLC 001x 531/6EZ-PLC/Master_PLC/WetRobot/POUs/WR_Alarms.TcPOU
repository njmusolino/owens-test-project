﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="WR_Alarms" Id="{f6a14cb2-b281-4921-b47c-c0a7bb64f669}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM WR_Alarms	// This POU is called every scan from WR_Main

// Note: Set Alarm description text values in AlarmSetup

VAR
	
	AlarmTMR_0 : TON;		// ALARM 0
	AlarmTMR_1 : TON;		// ALARM 1
	AlarmTMR_2 : TON;		// ALARM 2
	AlarmTMR_3 : TON;		// ALARM 3
	AlarmTMR_4 : TON;		// Alarm 4
	AlarmTMR_5 : TON;		// Alarm 5
	AlarmTMR_6 : TON;		// Alarm 6
	AlarmTMR_7 : TON;		// Alarm 7
	AlarmTMR_8 : TON;		// ALARM 8
	AlarmTMR_9 : TON;		// ALARM 9
	AlarmTMR_10 : TON;		// ALARM 10
	AlarmTMR_11 : TON;		// ALARM 11
	AlarmTMR_12 : TON;		// ALARM 12
	AlarmTMR_13 : TON;		// ALARM 13
	AlarmTMR_14 : TON;		// ALARM 14
	AlarmTMR_15 : TON;		// ALARM 15
	AlarmTMR_16 : TON;		// ALARM 16
	AlarmTMR_17 : TON;		// ALARM 17
	AlarmTMR_18 : TON;		// ALARM 18
	AlarmTMR_19 : TON;		// ALARM 19
	AlarmTMR_20 : TON;		// ALARM 20
	AlarmTMR_21 : TON;		// ALARM 21
	AlarmTMR_22 : TON;		// ALARM 22
	AlarmTMR_23 : TON;		// ALARM 23
	AlarmTMR_24 : TON;		// ALARM 24
	AlarmTMR_25 : TON;		// ALARM 25
	AlarmTMR_26 : TON;		// ALARM 26
	AlarmTMR_27 : TON;		// ALARM 27
	AlarmTMR_28 : TON;		// ALARM 28
	AlarmTMR_29 : TON;		// ALARM 29
	AlarmTMR_30 : TON;		// ALARM 30
	AlarmTMR_31 : TON;		// ALARM 31
	AlarmTMR_32 : TON;		// ALARM 32
	AlarmTMR_33 : TON;		// ALARM 33
	AlarmTMR_34 : TON;		// ALARM 34
	AlarmTMR_35 : TON;		// ALARM 35
	AlarmTMR_36 : TON;		// ALARM 36
	AlarmTMR_37 : TON;		// ALARM 37
	AlarmTMR_38 : TON;		// ALARM 38
	AlarmTMR_39 : TON;		// ALARM 39
	AlarmTMR_40 : TON;		// ALARM 40
	AlarmTMR_41 : TON;		// ALARM 41
	AlarmTMR_42 : TON;		// ALARM 42
	AlarmTMR_43 : TON;		// ALARM 43
	AlarmTMR_44 : TON;		// ALARM 44
	AlarmTMR_45 : TON;		// ALARM 45
	AlarmTMR_46 : TON;		// ALARM 46
	AlarmTMR_47 : TON;		// ALARM 47
	AlarmTMR_48 : TON;		// ALARM 48
	AlarmTMR_49 : TON;		// ALARM 49

	F_EStop : F_TRIG;		// Falling-edge trigger
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// ========== Set up Alarm ID and text descriptions
//  First scan or system reset
//
IF WR.bSystemResetPulse THEN AlarmSetup(); END_IF


// ========== ALARM 0 - E-Stop: E-Stop PB pressed during operation
//  When the E-Stop circuit is disabled, set an alarm.
//
// Set a falling-edge trigger for the E-Stop circuit input
F_Estop(CLK := WR_X_Axis.EStopOK);
// Alarm timer conditions
AlarmTMR_0.IN := ((F_Estop.Q OR AlarmTMR_0.Q) AND WR_MainSM.MainDelay.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_0(PT := T#0S);
WR.AlarmList[0].isActive := AlarmTMR_0.Q;


// ========== ALARM 1 - spare
// Alarm timer conditions
AlarmTMR_1.IN := ((FALSE) OR AlarmTMR_1.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_1(PT := T#0S);
WR.AlarmList[1].isActive := AlarmTMR_1.Q;


// ========== ALARM 2 - spare
// Alarm timer conditions
AlarmTMR_2.IN := ((FALSE) OR AlarmTMR_2.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_2(PT := T#0S);
WR.AlarmList[2].isActive := AlarmTMR_2.Q;


// ========== ALARM 3 - spare
// Alarm timer conditions
AlarmTMR_3.IN := ((FALSE) OR AlarmTMR_3.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_3(PT := T#0S);
WR.AlarmList[3].isActive := AlarmTMR_3.Q;


// ========== ALARM 4 - spare
// Alarm timer conditions
AlarmTMR_4.IN := ((FALSE) OR AlarmTMR_4.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_4(PT := T#0S);
WR.AlarmList[4].isActive := AlarmTMR_4.Q;


// ========== ALARM 5 - spare
// Alarm timer conditions
AlarmTMR_5.IN := ((FALSE) OR AlarmTMR_5.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_5(PT := T#0S);
WR.AlarmList[5].isActive := AlarmTMR_5.Q;


// ========== Alarm 6 - spare
// Alarm timer conditions
AlarmTMR_6.IN := ((FALSE) OR AlarmTMR_6.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_6(PT := T#0S);
WR.AlarmList[6].isActive := AlarmTMR_6.Q;


// ========== Alarm 7 - Wafer lost at SRD Spin Fingers
// Alarm timer conditions
AlarmTMR_7.IN := ((WR.bWaferInSRD_Fingers AND NOT WR.di_SRD_FingersAreClosedWaferSens) OR AlarmTMR_7.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_7(PT := T#2S);
WR.AlarmList[7].isActive := AlarmTMR_7.Q;


// ========== ALARM 8 - spare
// Alarm timer conditions
AlarmTMR_8.IN := ((FALSE) OR AlarmTMR_8.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_8(PT := T#0S);
WR.AlarmList[8].isActive := AlarmTMR_8.Q;


// ========== ALARM 9 - spare
// Alarm timer conditions
AlarmTMR_9.IN := ((FALSE) OR AlarmTMR_9.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_9(PT := T#0S);
WR.AlarmList[9].isActive := AlarmTMR_9.Q;


// ========== ALARM 10 - X-Axis Error (See Error Code)
// Alarm timer conditions
AlarmTMR_10.IN := ((WR_MainSM.MainDelay.Q AND WR.X_Axis.Status.Error) OR AlarmTMR_10.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_10(PT := T#0S);
WR.AlarmList[10].isActive := AlarmTMR_10.Q;


// ========== ALARM 11 - X-Axis Home Sequence Timeout
// Alarm timer conditions
AlarmTMR_11.IN := (WR_X_Axis.bHoming OR AlarmTMR_11.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_11(PT := WR.stX_AxisSet.HomeErrTime);
WR.AlarmList[11].isActive := AlarmTMR_11.Q;


// ========== ALARM 12 - X-Axis Absolute Position Move Timeout
// Axis control program state 12 is the Absolute Move (MC_MoveAbsolute)
// Alarm timer conditions
AlarmTMR_12.IN := (WR_X_Axis.fbSM.States[12] OR AlarmTMR_12.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_12(PT := WR.stX_AxisSet.ABSMoveErrTime);
WR.AlarmList[12].isActive := AlarmTMR_12.Q;


// ========== ALARM 13 - X-Axis Absolute Position Move Command Aborted
// Alarm timer conditions
AlarmTMR_13.IN := (WR_X_Axis.fbMC_MoveAbsolute.CommandAborted OR AlarmTMR_13.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_13(PT := T#0S);
WR.AlarmList[13].isActive := AlarmTMR_13.Q;


// ========== ALARM 14 - X-Axis Absolute Position Move stopped by EOT
// Alarm timer conditions
AlarmTMR_14.IN := ((WR_X_Axis.fbMC_MoveAbsolute.Busy AND (NOT WR_X_Axis.NotEOTREVSens OR NOT WR_X_Axis.NotEOTFWDSens)) OR AlarmTMR_14.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_14(PT := T#0S);
WR.AlarmList[14].isActive := AlarmTMR_14.Q;


// ========== ALARM 15 - X-Axis Move Target out of position range
// Axis control program state 11 is the Absolute Move setup state (before ABS move)
// Alarm timer conditions
AlarmTMR_15.IN := ((WR_X_Axis.fbSM.States[11] AND (NOT WR_X_Axis.bMoveTargetValid OR NOT WR_X_Axis.bMoveSpeedValid)) OR AlarmTMR_15.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_15(PT := T#0S);
WR.AlarmList[15].isActive := AlarmTMR_15.Q;


// ========== ALARM 16 - X-Axis Door to Move Target not Open (before or during move)
// Axis control program state 11 is the Absolute Move setup state (before ABS move)
// Alarm timer conditions
AlarmTMR_16.IN := ((WR_X_Axis.fbSM.States[11] AND NOT WR_X_Axis.bMoveTargetDoorOpened) OR (WR_X_Axis.fbMC_Halt.Active AND NOT WR_X_Axis.bMoveTargetDoorOpened) OR AlarmTMR_16.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_16(PT := T#0S);
WR.AlarmList[16].isActive := AlarmTMR_16.Q;


// ========== ALARM 17 - X-Axis Lifter or SRD Spindle lift not in correct position for Move Target (before or during move)
// fbLifter.IsUp signal lost during ABS Move
// Alarm timer conditions
AlarmTMR_17.IN := ((WR_X_Axis.fbSM.States[11] AND (NOT WR_X_Axis.bMoveTargetLifterOK OR NOT WR_X_Axis.bMoveTargetSRDLiftOK)) OR (WR_X_Axis.fbMC_Halt.Active AND NOT WR_X_Axis.bMoveTargetLifterOK) OR AlarmTMR_17.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_17(PT := T#0S);
WR.AlarmList[17].isActive := AlarmTMR_17.Q;


// ========== ALARM 18 - X-Axis Motion stopped: Rotary Actuator is not at 0 or 180
// The X-Axis can only move if the Rotary Actuator is horizontal (at the 0 or 180 degree position)
// Alarm timer conditions
AlarmTMR_18.IN := ((WR_X_Axis.fbMC_Halt.Active AND (NOT WR_Pneu.fbFlipper.Is_At0 AND NOT WR_Pneu.fbFlipper.Is_At180)) OR AlarmTMR_18.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_18(PT := T#0S);
WR.AlarmList[18].isActive := AlarmTMR_18.Q;


// ========== ALARM 19 - spare
// Alarm timer conditions
AlarmTMR_19.IN := ((FALSE) OR AlarmTMR_19.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_19(PT := T#0S);
WR.AlarmList[19].isActive := AlarmTMR_19.Q;


// ========== ALARM 20 - Lifter Error: Failed to reach upper position
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_20.IN := (WR_Pneu.fbLifter.TimeoutUp OR AlarmTMR_20.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_20(PT := T#0S);
WR.AlarmList[20].isActive := AlarmTMR_20.Q;


// ========== ALARM 21 - Lifter Error: Failed to reach lower position
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_21.IN := (WR_Pneu.fbLifter.TimeoutDown OR AlarmTMR_21.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_21(PT := T#0S);
WR.AlarmList[21].isActive := AlarmTMR_21.Q;


// ========== ALARM 22 - Gripper Error: Failed to open
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_22.IN := (WR_Pneu.fbGripper.TimeoutOpen OR AlarmTMR_22.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_22(PT := T#0S);
WR.AlarmList[22].isActive := AlarmTMR_22.Q;


// ========== ALARM 23 - Gripper Error: Failed to close
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_23.IN := (WR_Pneu.fbGripper.TimeoutClose OR AlarmTMR_23.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_23(PT := T#0S);
WR.AlarmList[23].isActive := AlarmTMR_23.Q;


// ========== ALARM 24 - Flipper/Rotary Actuator Error: Failed to reach 0 position
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_24.IN := (WR_Pneu.fbFlipper.Timeout_To0 OR AlarmTMR_24.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_24(PT := T#0S);
WR.AlarmList[24].isActive := AlarmTMR_24.Q;


// ========== ALARM 25 - Flipper/Rotary Actuator Error: Failed to reach 90 position
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_25.IN := (WR_Pneu.fbFlipper.Timeout_To90 OR AlarmTMR_25.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_25(PT := T#0S);
WR.AlarmList[25].isActive := AlarmTMR_25.Q;


// ========== ALARM 26 - Flipper/Rotary Actuator Error: Failed to reach 180 position
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_26.IN := (WR_Pneu.fbFlipper.Timeout_To180 OR AlarmTMR_26.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_26(PT := T#0S);
WR.AlarmList[26].isActive := AlarmTMR_26.Q;


// ========== ALARM 27 - Inner Shield Door Error: Failed to open
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_27.IN := (WR_Pneu.fbShieldDoor.TimeoutOpen OR AlarmTMR_27.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_27(PT := T#0S);
WR.AlarmList[27].isActive := AlarmTMR_27.Q;


// ========== ALARM 28 - Inner Shield Door (at Polisher) Error: Failed to close
// Timeout error is set in the solenoid control function block in the WR_Pneu program.
// Alarm timer conditions
AlarmTMR_28.IN := (WR_Pneu.fbShieldDoor.TimeoutClose OR AlarmTMR_28.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_28(PT := T#0S);
WR.AlarmList[28].isActive := AlarmTMR_28.Q;


// ========== ALARM 29 - spare
// Alarm timer conditions
AlarmTMR_29.IN := ((FALSE) OR AlarmTMR_29.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_29(PT := T#0S);
WR.AlarmList[29].isActive := AlarmTMR_29.Q;


// ========== ALARM 30 - spare
// Alarm timer conditions
AlarmTMR_30.IN := ((FALSE) OR AlarmTMR_30.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_30(PT := T#0S);
WR.AlarmList[30].isActive := AlarmTMR_30.Q;


// ========== ALARM 31 - spare
// Alarm timer conditions
AlarmTMR_31.IN := ((FALSE) OR AlarmTMR_31.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_31(PT := T#0S);
WR.AlarmList[31].isActive := AlarmTMR_31.Q;


// ========== ALARM 32 - spare
// Alarm timer conditions
AlarmTMR_32.IN := ((FALSE) OR AlarmTMR_32.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_32(PT := T#0S);
WR.AlarmList[32].isActive := AlarmTMR_32.Q;


// ========== ALARM 33 - spare
// Alarm timer conditions
AlarmTMR_33.IN := ((FALSE) OR AlarmTMR_33.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_33(PT := T#0S);
WR.AlarmList[33].isActive := AlarmTMR_33.Q;


// ========== ALARM 34 - spare
// Alarm timer conditions
AlarmTMR_34.IN := ((FALSE) OR AlarmTMR_34.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_34(PT := T#0S);
WR.AlarmList[34].isActive := AlarmTMR_34.Q;


// ========== ALARM 35 - spare
// Alarm timer conditions
AlarmTMR_35.IN := ((FALSE) OR AlarmTMR_35.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_35(PT := T#0S);
WR.AlarmList[35].isActive := AlarmTMR_35.Q;


// ========== ALARM 36 - spare
// Alarm timer conditions
AlarmTMR_36.IN := ((FALSE) OR AlarmTMR_36.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_36(PT := T#0S);
WR.AlarmList[36].isActive := AlarmTMR_36.Q;


// ========== ALARM 37 - spare
// Alarm timer conditions
AlarmTMR_37.IN := ((FALSE) OR AlarmTMR_37.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_37(PT := T#0S);
WR.AlarmList[37].isActive := AlarmTMR_37.Q;


// ========== ALARM 38 - spare
// Alarm timer conditions
AlarmTMR_38.IN := ((FALSE) OR AlarmTMR_38.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_38(PT := T#0S);
WR.AlarmList[38].isActive := AlarmTMR_38.Q;


// ========== ALARM 39 - spare
// Alarm timer conditions
AlarmTMR_39.IN := ((FALSE) OR AlarmTMR_39.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_39(PT := T#0S);
WR.AlarmList[39].isActive := AlarmTMR_39.Q;


// ========== ALARM 40 - spare
// Alarm timer conditions
AlarmTMR_40.IN := ((FALSE) OR AlarmTMR_40.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_40(PT := T#0S);
WR.AlarmList[40].isActive := AlarmTMR_40.Q;


// ========== ALARM 41 - spare
// Alarm timer conditions
AlarmTMR_41.IN := ((FALSE) OR AlarmTMR_41.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_41(PT := T#0S);
WR.AlarmList[41].isActive := AlarmTMR_41.Q;


// ========== ALARM 42 - spare
// Alarm timer conditions
AlarmTMR_42.IN := ((FALSE) OR AlarmTMR_42.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_42(PT := T#0S);
WR.AlarmList[42].isActive := AlarmTMR_42.Q;


// ========== ALARM 43 - spare
// Alarm timer conditions
AlarmTMR_43.IN := ((FALSE) OR AlarmTMR_43.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_43(PT := T#0S);
WR.AlarmList[43].isActive := AlarmTMR_43.Q;


// ========== ALARM 44 - spare
// Alarm timer conditions
AlarmTMR_44.IN := ((FALSE) OR AlarmTMR_44.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_44(PT := T#0S);
WR.AlarmList[44].isActive := AlarmTMR_44.Q;


// ========== ALARM 45 - spare
// Alarm timer conditions
AlarmTMR_45.IN := ((FALSE) OR AlarmTMR_45.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_45(PT := T#0S);
WR.AlarmList[45].isActive := AlarmTMR_45.Q;


// ========== ALARM 46 - spare
// Alarm timer conditions
AlarmTMR_46.IN := ((FALSE) OR AlarmTMR_46.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_46(PT := T#0S);
WR.AlarmList[46].isActive := AlarmTMR_46.Q;


// ========== ALARM 47 - spare
// Alarm timer conditions
AlarmTMR_47.IN := ((FALSE) OR AlarmTMR_47.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_47(PT := T#0S);
WR.AlarmList[47].isActive := AlarmTMR_47.Q;


// ========== ALARM 48 - spare
// Alarm timer conditions
AlarmTMR_48.IN := ((FALSE) OR AlarmTMR_48.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_48(PT := T#0S);
WR.AlarmList[48].isActive := AlarmTMR_48.Q;


// ========== ALARM 49 - spare
// Alarm timer conditions
AlarmTMR_49.IN := ((FALSE) OR AlarmTMR_49.Q) AND NOT WR.bAlarmResetPulse;
AlarmTMR_49(PT := T#0S);
WR.AlarmList[49].isActive := AlarmTMR_49.Q;


// ========== Check for active alarms
//
AlarmStatus();
]]></ST>
    </Implementation>
    <Method Name="AlarmSetup" Id="{cf19e00b-d667-4968-b56a-f2b5b355458f}">
      <Declaration><![CDATA[METHOD AlarmSetup : BOOL	//  Set up Alarm ID and text descriptions on the first scan/reset
VAR_INPUT
END_VAR

VAR	
	alarmSet0 : ARRAY [0..WR.Alarm_Qty] OF STRING := [
		'ALARM 0 - E-Stop: E-Stop PB pressed during operation',
		'ALARM 1 - ',
		'ALARM 2 - ',
		'ALARM 3 - ',
		'ALARM 4 - ',
		'ALARM 5 - ',
		'Alarm 6 - ',
		'Alarm 7 - Wafer lost at SRD Spin Fingers',
		'ALARM 8 - ',
		'ALARM 9 - ',
		'ALARM 10 - X-Axis Error (See Error Code)',
		'ALARM 11 - X-Axis Home Sequence Timeout',
		'ALARM 12 - X-Axis Absolute Position Move Timeout',
		'ALARM 13 - X-Axis Absolute Position Move Command Aborted',
		'ALARM 14 - X-Axis Absolute Position Move Halted by EOT',
		'ALARM 15 - X-Axis Move Target out of position range',
		'ALARM 16 - X-Axis Door to Move Target not Open',
		'ALARM 17 - X-Axis Lifter or SRD spindle not in correct position for Move Target',
		'ALARM 18 - X-Axis Motion stopped: Flipper is not at 0 or 180',
		'ALARM 19 - ',
		'ALARM 20 - Lifter Error: Failed to reach upper position',
		'ALARM 21 - Lifter Error: Failed to reach lower position',
		'ALARM 22 - Gripper Error: Failed to open',
		'ALARM 23 - Gripper Error: Failed to close',
		'ALARM 24 - Flipper/Rotary Actuator Error: Failed to reach 0 position',
		'ALARM 25 - Flipper/Rotary Actuator Error: Failed to reach 90 position',
		'ALARM 26 - Flipper/Rotary Actuator Error: Failed to reach 180 position',
		'ALARM 27 - Inner Shield Door Error: Failed to open (up)',
		'ALARM 28 - Inner Shield Door Error: Failed to close (down)',
		'ALARM 29 - ',
		'ALARM 30 - ',
		'ALARM 31 - ',
		'ALARM 32 - ',
		'ALARM 33 - ',
		'ALARM 34 - ',
		'ALARM 35 - ',
		'ALARM 36 - ',
		'ALARM 37 - ',
		'ALARM 38 - ',
		'ALARM 39 - ',
		'ALARM 40 - ',
		'ALARM 41 - ',
		'ALARM 42 - ',
		'ALARM 43 - ',
		'ALARM 44 - ',
		'ALARM 45 - ',
		'ALARM 46 - ',
		'ALARM 47 - ',
		'ALARM 48 - ',
		'ALARM 49 - '];
	
	nCounter: INT;
	ID: INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
//  Assign values for ID and alarm text 
FOR nCounter :=0 TO WR.Alarm_Qty BY 1 DO
	WR.AlarmList[nCounter].ID := nCounter;
	WR.AlarmList[nCounter].Description := alarmSet0[nCounter];
END_FOR
]]></ST>
      </Implementation>
    </Method>
    <Method Name="AlarmStatus" Id="{c91e996d-76b8-4daf-91fa-badd6f4c9a1e}">
      <Declaration><![CDATA[METHOD AlarmStatus : BOOL
VAR_INPUT
END_VAR

VAR
	nCounter: INT;
END_VAR

]]></Declaration>
      <Implementation>
        <ST><![CDATA[
//  Reset the active alarm bit, which will be set in the following FOR loop, if necessary.
WR.bAlarmActive := FALSE;

// Go through the Alarm list and check for active alarm 
FOR nCounter :=0 TO WR.Alarm_Qty BY 1 DO	
	IF WR.AlarmList[nCounter].isActive THEN
		WR.bAlarmActive := TRUE;
	END_IF
END_FOR
]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="WR_Alarms">
      <LineId Id="5" Count="0" />
      <LineId Id="73" Count="0" />
      <LineId Id="97" Count="0" />
      <LineId Id="74" Count="0" />
      <LineId Id="121" Count="0" />
      <LineId Id="75" Count="1" />
      <LineId Id="81" Count="0" />
      <LineId Id="72" Count="0" />
      <LineId Id="104" Count="0" />
      <LineId Id="106" Count="1" />
      <LineId Id="83" Count="1" />
      <LineId Id="108" Count="0" />
      <LineId Id="283" Count="1" />
      <LineId Id="632" Count="6" />
      <LineId Id="363" Count="4" />
      <LineId Id="285" Count="1" />
      <LineId Id="368" Count="18" />
      <LineId Id="120" Count="0" />
      <LineId Id="387" Count="0" />
      <LineId Id="389" Count="1" />
      <LineId Id="912" Count="2" />
      <LineId Id="394" Count="20" />
      <LineId Id="388" Count="0" />
      <LineId Id="110" Count="1" />
      <LineId Id="116" Count="3" />
      <LineId Id="109" Count="0" />
      <LineId Id="85" Count="0" />
      <LineId Id="509" Count="28" />
      <LineId Id="544" Count="7" />
      <LineId Id="538" Count="0" />
      <LineId Id="627" Count="4" />
      <LineId Id="427" Count="1" />
      <LineId Id="560" Count="7" />
      <LineId Id="552" Count="7" />
      <LineId Id="429" Count="4" />
      <LineId Id="281" Count="0" />
      <LineId Id="185" Count="1" />
      <LineId Id="234" Count="0" />
      <LineId Id="230" Count="3" />
      <LineId Id="187" Count="2" />
      <LineId Id="244" Count="1" />
      <LineId Id="235" Count="2" />
      <LineId Id="190" Count="2" />
      <LineId Id="246" Count="1" />
      <LineId Id="238" Count="2" />
      <LineId Id="193" Count="2" />
      <LineId Id="248" Count="1" />
      <LineId Id="241" Count="2" />
      <LineId Id="196" Count="2" />
      <LineId Id="250" Count="4" />
      <LineId Id="202" Count="0" />
      <LineId Id="204" Count="1" />
      <LineId Id="255" Count="4" />
      <LineId Id="206" Count="2" />
      <LineId Id="260" Count="4" />
      <LineId Id="209" Count="0" />
      <LineId Id="200" Count="0" />
      <LineId Id="493" Count="15" />
      <LineId Id="1009" Count="4" />
      <LineId Id="645" Count="1" />
      <LineId Id="1004" Count="4" />
      <LineId Id="659" Count="1" />
      <LineId Id="999" Count="4" />
      <LineId Id="725" Count="1" />
      <LineId Id="994" Count="4" />
      <LineId Id="732" Count="1" />
      <LineId Id="989" Count="4" />
      <LineId Id="739" Count="1" />
      <LineId Id="984" Count="4" />
      <LineId Id="746" Count="1" />
      <LineId Id="979" Count="4" />
      <LineId Id="753" Count="1" />
      <LineId Id="974" Count="4" />
      <LineId Id="760" Count="92" />
      <LineId Id="98" Count="0" />
      <LineId Id="100" Count="1" />
      <LineId Id="82" Count="0" />
    </LineIds>
    <LineIds Name="WR_Alarms.AlarmSetup">
      <LineId Id="6" Count="2" />
      <LineId Id="71" Count="0" />
      <LineId Id="9" Count="1" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="WR_Alarms.AlarmStatus">
      <LineId Id="14" Count="9" />
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>