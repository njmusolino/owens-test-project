﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="WR_Flip" Id="{f5ece085-b78d-4cc2-b4a7-5b8f9878e10e}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM WR_Flip		// Wet Robot Flip Wafer logic (used for FUN_WR_Flip function)

VAR
	
	fbSM : FB_stStateMachineCtrl;	// State machine control function block	
	R_DoingTask : R_TRIG;			// Rising-edge trigger
	Task_Timeout : TON;				// Task timeout (not responding)
	ErrState : INT; 
	
	MoveTo0 : BOOL;
	MoveTo180 : BOOL;
	
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[//
// =========================================================================================================
// The Flip function will leave the WR in the X-Axis Flip position with the
// Lifter down, and the Rotary Actuator at 90. (This function isn't necessary.)
//
// =========================================================================================================
//


// On first starting the logic, reset to state zero.
R_DoingTask(CLK := (WR.Task = E_WR_Task.Flip) OR WR.FlipFromPnP);	// Rising-edge trigger
IF R_DoingTask.Q OR WR.AbortFun THEN
	fbSM.State := 0;
	WR.Resp_Flip := 0;
	ErrState := 0;
END_IF

// ============================================================
// ============================================================
// ========	Wet Robot Flip Wafer State Machine
//
fbSM.Pause := FALSE;
fbSM.SnglStep := FALSE;
fbSM();


// ============================================================
// ========	State 0
//			Wait for a move request
//			(Set up the initial move to the Flip position)
//
IF fbSM.States[0] AND NOT WR.AbortFun THEN
	IF (WR.Task = E_WR_Task.Flip) OR WR.FlipFromPnP THEN
		fbSM.State := 2;	// Next State
		// Reset internal SRD pneumatics errors
		WR.SRD_SpindleLift_Err := FALSE;
		WR.SRD_Fingers_Err := FALSE;
		WR.SRD_SpindleLock_Err := FALSE;
		WR.SRD_WetDoor_Err := FALSE;
		// Set the move target position
		WR.MoveToPosnTarget := 2;	// X-Axis Flip position
		// Clear old response
		WR.Resp_MoveToPosn := 0;
	END_IF
END_IF


// ============================================================
// ========	State 2
//			Set up the flipped position
//
IF fbSM.States[2] THEN
	// Set the target for the rotary actuator movement
	IF WR_Pneu.fbFlipper.Is_At0 THEN
		MoveTo0 := FALSE;
		MoveTo180 := TRUE;
	ELSIF WR_Pneu.fbFlipper.Is_At180 THEN
		MoveTo0 := TRUE;
		MoveTo180 := FALSE;
	ELSE	// Started at 90--leave it where the MoveToPosn logic puts it
		MoveTo0 := FALSE;
		MoveTo180 := FALSE;	
	END_IF
	fbSM.State := 4;	// Next state
END_IF


// Turn off external requests--if being used, the request will
// be set to true in the state that is using it.
// 
WR.MoveToPosnFromFlip := FALSE;


// ============================================================
// ========	State 4
//			Move the X-Axis with the MoveToPosn logic
//			(MoveToPosn will also raise the lifter, etc.)
//			When the Response is other than 0, MoveToPosn is done.
//
IF fbSM.States[4] THEN
	IF WR.Resp_MoveToPosn = 0 THEN
		WR.MoveToPosnFromFlip := TRUE;	// Run the Move logic
	ELSE	// Move logic done
		fbSM.State := 6;	// Next state
	END_IF
END_IF


// ============================================================
// ========	State 6
//			Check the MoveToPosn response for success or failure
//			(1 = success, <0 = failure)
//
IF fbSM.States[6] THEN
	IF WR.Resp_MoveToPosn = 1 THEN
		fbSM.State := 8;	// Next state
	ELSE
		WR.Resp_Flip := WR.Resp_MoveToPosn;
		ErrState := fbSM.State;
		fbSM.State := 31;	// Error state
	END_IF
END_IF


// ============================================================
// ========	State 8
//			Rotate to the opposite horizontal position
//
IF fbSM.States[8] THEN
	IF (WR_Pneu.fbFlipper.Is_At0 AND MoveTo0) OR (WR_Pneu.fbFlipper.Is_At180 AND MoveTo180) OR (NOT MoveTo0 AND NOT MoveTo180) THEN
		WR.Resp_Flip := 1;
		fbSM.State := 30;	// Done
	ELSIF WR_Pneu.fbFlipper.ErrPresent THEN
		WR.Resp_Flip := -26;
		ErrState := fbSM.State;
		fbSM.State := 31;	// Error state
	ELSE	// Try to move the actuator to a position (if the X-Axis is in the Flip pos.)
		IF WR_X_Axis.bInFlipPosn THEN	// Rotate to 0 or 180 degrees
			WR.RotGoto0Auto := MoveTo0;
			WR.RotGoto90Auto := FALSE;
			WR.RotGoto180Auto := MoveTo180;
		ELSE
			WR.Resp_Flip := -5;
			ErrState := fbSM.State;
			fbSM.State := 31;	// Error state
		END_IF
		// If the Wet Door isn't in position, the Flipper can't be moved.
		IF NOT WR.di_SRD_WetDoorIsClosedSens THEN
			WR.Resp_Flip := -46;
			ErrState := fbSM.State;
			fbSM.State := 31;	// Error state
		END_IF
	END_IF
END_IF


// ========== Task Timeout
Task_Timeout.IN := (fbSM.State < 30) AND (fbSM.State > 0);
Task_Timeout(PT := WR.TT_Flip_ErrDelay);
IF Task_Timeout.Q THEN
	WR.Resp_Flip := -98;	// Task Timeout
	ErrState := fbSM.State;
	fbSM.State := 31;	// Error state
END_IF


// ============================================================
// ========	State 30
//			Done (State will reset to zero at the next start)
//
IF fbSM.States[30] THEN
	IF WR.bSystemResetPulse THEN
		fbSM.State := 0;	// Reset to state 0 on reset
		WR.Resp_Flip := 0;
	END_IF
END_IF


// ============================================================
// ========	State 31
//			Error
//
IF fbSM.States[31] THEN
	IF WR.bAlarmResetPulse OR WR.bSystemResetPulse THEN
		fbSM.State := 0;	// Reset to state 0 on reset
		WR.Resp_Flip := 0;
		ErrState := 0;
	END_IF
END_IF


// ========	Release the task, if running from a function and finished
IF (WR.Task = E_WR_Task.Flip) AND (fbSM.States[30] OR fbSM.States[31]) THEN
	WR.Resp_Last := WR.Resp_Flip;	// Set the last active task response value
	WR.Task := E_WR_Task.Inactive;
END_IF

]]></ST>
    </Implementation>
    <LineIds Name="WR_Flip">
      <LineId Id="883" Count="176" />
      <LineId Id="49" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>