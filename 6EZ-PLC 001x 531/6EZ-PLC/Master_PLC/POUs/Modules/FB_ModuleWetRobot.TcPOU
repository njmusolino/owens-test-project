﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.12">
  <POU Name="FB_ModuleWetRobot" Id="{b450680c-3ff4-451b-afaf-06211fa24bc7}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_ModuleWetRobot EXTENDS FB_ModuleBaseClass
VAR_INPUT
	stModuleIO : ST_ModuleIOWetRobot;
	stModuleData : ST_ModuleDataWetRobot;
	stModuleConfig : ST_ModuleConfigWetRobot;
END_VAR
VAR_OUTPUT
END_VAR
VAR
	pstSRDModuleIO : POINTER TO ST_ModuleIOSRD;
	
// declare Subsystems
	fbOwensWetRobot : FB_SubsystemOwensWetRobot;
	
// declare module variables
	iSafetyMask : UDINT;

	fbMonitorSrdFingerOpenRequest : R_TRIG;
	fbMonitorSrdFingerCloseRequest : R_TRIG;
	fbMonitorSrdSpindleLockRequest : R_TRIG;
	fbMonitorSrdSpindleUnlockRequest : R_TRIG;
	
	fbMonitorSrdSpindleUpRequest : R_TRIG;
	fbMonitorSrdSpindleDownRequest : R_TRIG;
	fbMonitorSrdWetDoorOpenRequest : R_TRIG;
	fbMonitorSrdWetDoorCloseRequest : R_TRIG;
	
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[]]></ST>
    </Implementation>
    <Method Name="Abort" Id="{7c53ebfd-dcdb-4534-9029-4366c421a681}">
      <Declaration><![CDATA[
METHOD PUBLIC Abort
VAR_INPUT
	eObjectId	: E_ObjectIDs;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[]]></ST>
      </Implementation>
    </Method>
    <Method Name="CheckPersistentVariables" Id="{82ae8b44-4a88-47be-8591-efc94378e348}">
      <Declaration><![CDATA[METHOD CheckPersistentVariables
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[]]></ST>
      </Implementation>
    </Method>
    <Method Name="ExecuteOnModule" Id="{1931fd2d-07fd-4a90-bb57-b0452631526f}">
      <Declaration><![CDATA[METHOD PROTECTED ExecuteOnModule
VAR_INPUT
	eModuleObject	: E_ObjectIDs;
	eModuleCommand	: E_CommandIDs;
	sValue : STRING[COMMAND_PARAMETER_SIZE];
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[]]></ST>
      </Implementation>
    </Method>
    <Method Name="Initialize" Id="{7cabe29d-408b-45fd-aab6-d5ab5fd2b7e8}">
      <Declaration><![CDATA[
METHOD PUBLIC Initialize
VAR_INPUT
	eObjectId	: E_ObjectIDs;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[
SUPER^.Initialize( eObjectId );

// subsystem overhead setup
THIS^.stModuleIO.StructureVersion := cStructModuleWetRobotIOVersion;
THIS^.stModuleData.StructureSize := SIZEOF( THIS^.stModuleData );
THIS^.stModuleIO.StructureSize := SIZEOF( THIS^.stModuleIO );
THIS^.stModuleConfig.StructureSize := SIZEOF( THIS^.stModuleConfig );

pstSRDModuleIO := ADR(.ModuleSupervisor.pfbModuleSRD^.stModuleIO);
// update Config/Persistent variables

// BasicAddSubsystem () and SetupIO()
BasicAddSubsystem(ADR(fbOwensWetRobot), E_ObjectIDs.eWetRobotOwensRobot);
fbOwensWetRobot.SetupIO();
// SetupAxis ()

// Endpoints

// Safety Checks	
]]></ST>
      </Implementation>
    </Method>
    <Method Name="Service" Id="{72c7993a-1d93-4195-9d91-aa1ce7b598f6}">
      <Declaration><![CDATA[METHOD PUBLIC Service
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[SUPER^.Service();

UpdateOwensIO();


IF NOT(WR_MainSM.SimulateSensors) OR NOT(WR_Pneu.SimSRD_SpindleFingers) THEN
	fbMonitorSrdFingerOpenRequest(CLK := WR.SRD_FingersOpenReq);			// arm tests for I/O change from Owens code
	fbMonitorSrdFingerCloseRequest(CLK := WR.SRD_FingersCloseReq);
	IF fbMonitorSrdFingerOpenRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdFingers, E_CommandIDs.eCommand_Open, '');
	END_IF
	IF fbMonitorSrdFingerCloseRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdFingers, E_CommandIDs.eCommand_Close, '');		
	END_IF		

	//WR.di_SRD_FingersAreOpenedSens := stModuleData.diSpinFingerIsAtOpen;
	//WR.di_SRD_FingersAreClosedWaferSens := stModuleData.diSpinFingerHasWafer;
END_IF

IF NOT(WR_MainSM.SimulateSensors) OR NOT(WR_Pneu.SimSRD_SpindleLock) THEN
	fbMonitorSrdSpindleLockRequest(CLK := WR.SRD_SpindleLockReq);
	fbMonitorSrdSpindleUnlockRequest(CLK := WR.SRD_SpindleUnlockReq);
	IF fbMonitorSrdSpindleLockRequest.Q THEN
		//fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdSpindleUnlock, E_CommandIDs.eCommand_Off, '');
		WR.di_SRD_SpindleIsLockedSens := TRUE; //simulating lock sense for WR
	END_IF
	IF fbMonitorSrdSpindleUnlockRequest.Q THEN
		//fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdSpindleUnlock, E_CommandIDs.eCommand_On, '');
		WR.di_SRD_SpindleIsLockedSens := FALSE; //simulating lock sense for WR
	END_IF		

	//WR.di_SRD_SprayArmIsRetractedSens := stModuleData.diJetArmSprayAtRetract;
	//WR.di_SRD_SpindleIsLockedSens := stModuleData.diSpindleLock;
END_IF

IF NOT(WR_MainSM.SimulateSensors) OR NOT(WR_Pneu.SimSRD_SpindleLift) THEN
	fbMonitorSrdSpindleUpRequest(CLK := WR.SRD_SpindleUpReq);
	fbMonitorSrdSpindleDownRequest(CLK := WR.SRD_SpindleDownReq);
	IF fbMonitorSrdSpindleUpRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdSpindleUpDown, E_CommandIDs.eCommand_Up, '');			
	END_IF
	IF fbMonitorSrdSpindleDownRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdSpindleUpDown, E_CommandIDs.eCommand_Down, '');			
	END_IF		

	//WR.di_SRD_SpindleIsUpSens := stModuleData.diSpindleIsAtUp;
	//WR.di_SRD_SpindleIsDownSens := stModuleData.diSpindleIsAtDown;
END_IF

IF NOT(WR_MainSM.SimulateSensors) OR NOT(WR_Pneu.SimSRD_WetDoor) THEN		
	fbMonitorSrdWetDoorOpenRequest(CLK := WR.SRD_WetDoorOpenReq);
	fbMonitorSrdWetDoorCloseRequest(CLK := WR.SRD_WetDoorCloseReq);
																			// comply with any I/O changes
	IF fbMonitorSrdWetDoorOpenRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdWetDoor, E_CommandIDs.eCommand_Open, '');
	END_IF
	IF fbMonitorSrdWetDoorCloseRequest.Q THEN
		fbSequencerA.ExecuteCommandOnSubsystem(E_ObjectIDs.eSrdWetDoor, E_CommandIDs.eCommand_Close, '');
	END_IF

	//WR.di_SRD_WetDoorIsOpenedSens := stModuleData.diWetDoorIsAtOpen;	// update WR.di_SRD_* data from stModuleData
	//WR.di_SRD_WetDoorIsClosedSens := stModuleData.diWetDoorIsAtClosed;
END_IF
	
	
// update the SafetyCheck data
]]></ST>
      </Implementation>
    </Method>
    <Method Name="UpdateConfigVariables" Id="{2b39db5f-5863-4905-b138-c4d0565f3f1e}">
      <Declaration><![CDATA[METHOD UpdateConfigVariables
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[;]]></ST>
      </Implementation>
    </Method>
    <Action Name="UpdateOwensIO" Id="{40a2ef7f-fecd-4aac-aecc-80c7449c6f02}">
      <Implementation>
        <ST><![CDATA[
// copy the OwensWetRobot controlled digital outputs (WR.do_*) to the 6EZ StModuleIO and stModuleData variables 
stModuleData.do_LifterUpSol := stModuleIO.do_LifterUpSol := BOOL_TO_BYTE(WR.do_LifterUpSol);								// Lifter
stModuleData.do_LifterDownSol := stModuleIO.do_LifterDownSol := BOOL_TO_BYTE(WR.do_LifterDownSol);
stModuleData.do_GripperOpenSol := stModuleIO.do_GripperOpenSol :=  BOOL_TO_BYTE(WR.do_GripperOpenSol);						// Gripper
stModuleData.do_GripperCloseSol := stModuleIO.do_GripperCloseSol :=  BOOL_TO_BYTE(WR.do_GripperCloseSol);
stModuleData.do_Flipper_MSZ_1A_Sol := stModuleIO.do_Flipper_MSZ_1A_Sol :=  BOOL_TO_BYTE(WR.do_Flipper_MSZ_1A_Sol);				// Rotary actuator SMC MSZ) port A
stModuleData.do_Flipper_MSZ_1B_Sol := stModuleIO.do_Flipper_MSZ_1B_Sol :=  BOOL_TO_BYTE(WR.do_Flipper_MSZ_1B_Sol);				// Rotary actuator SMC MSZ) port C
stModuleData.do_Flipper_MSZ_2A_Sol := stModuleIO.do_Flipper_MSZ_2A_Sol :=  BOOL_TO_BYTE(WR.do_Flipper_MSZ_2A_Sol);				// Rotary actuator SMC MSZ) port B
stModuleData.do_Flipper_MSZ_2B_Sol := stModuleIO.do_Flipper_MSZ_2B_Sol :=  BOOL_TO_BYTE(WR.do_Flipper_MSZ_2B_Sol);				// Rotary actuator SMC MSZ) port D
stModuleData.do_ShieldDoorOpenSol := stModuleIO.do_ShieldDoorOpenSol :=  BOOL_TO_BYTE(WR.do_ShieldDoorOpenSol);				// Inner Shield Door (at Polisher) Open/Up - drives two air cylinders
stModuleData.do_ShieldDoorCloseSol := stModuleIO.do_ShieldDoorCloseSol :=  BOOL_TO_BYTE(WR.do_ShieldDoorCloseSol);			// Inner Shield Door (at Polisher) Close/Down - drives two air cylinders


//If Owen's simulation is running, pump their variables into our I/O structure
//Else, pump our I/O into Owen's variables
IF WR_MainSM.SimulateSensors THEN
	stModuleData.di_HomeSens := stModuleIO.di_HomeSens := BOOL_TO_BYTE(WR.di_HomeSens);			// Home Sensor
	stModuleData.di_EOTFWDSens := stModuleIO.di_EOTFWDSens := BOOL_TO_BYTE(WR.di_EOTFWDSens);		// Forward End of Travel sensor
	stModuleData.di_EOTREVSens := 	stModuleIO.di_EOTREVSens := BOOL_TO_BYTE(WR.di_EOTREVSens); 	// Reverse End of Travel sensor
	stModuleData.di_WaferAtFlipSens := stModuleIO.di_WaferAtFlipSens := BOOL_TO_BYTE(WR.di_WaferAtFlipSens);		// Wafer present sensor at Flip position
	stModuleData.di_EStopOK := stModuleIO.di_EStopOK := BOOL_TO_BYTE(WR.di_EStopOK);		// E-Stop circuit is reset/OK

	stModuleData.di_LifterIsUpSens := stModuleIO.di_LifterIsUpSens := BOOL_TO_BYTE(WR.di_LifterIsUpSens);
	stModuleData.di_LifterIsDownSens := stModuleIO.di_LifterIsDownSens := BOOL_TO_BYTE(WR.di_LifterIsDownSens);

	stModuleData.di_GripperIsOpenedSens := stModuleIO.di_GripperIsOpenedSens := BOOL_TO_BYTE(WR.di_GripperIsOpenedSens);	// Gripper is Opened
	stModuleData.di_GripperIsClosedSens := stModuleIO.di_GripperIsClosedSens := BOOL_TO_BYTE(WR.di_GripperIsClosedSens); // Gripper is Closed

	stModuleData.di_Flipper_At0_Sens :=	stModuleIO.di_Flipper_At0_Sens :=	BOOL_TO_BYTE(WR.di_Flipper_At0_Sens);	// Rotary Actuator at 0 degree position (horizontal for Polisher station)
	stModuleData.di_Flipper_At90_Sens := stModuleIO.di_Flipper_At90_Sens := BOOL_TO_BYTE(WR.di_Flipper_At90_Sens);	// Rotary Actuator at 90 degree position (vertical)
	stModuleData.di_Flipper_At180_Sens := stModuleIO.di_Flipper_At180_Sens := BOOL_TO_BYTE(WR.di_Flipper_At180_Sens);	// Rotary Actuator at 180 degree position (horizontal for Cleaner/SRD station)

	stModuleData.di_ShieldDoorIsOpened1Sens := stModuleIO.di_ShieldDoorIsOpened1Sens := BOOL_TO_BYTE(WR.di_ShieldDoorIsOpened1Sens);	// Inner Shield Door (at Polisher) Open/Up - air cyl. 1
	stModuleData.di_ShieldDoorIsOpened2Sens := stModuleIO.di_ShieldDoorIsOpened2Sens := BOOL_TO_BYTE(WR.di_ShieldDoorIsOpened2Sens);	// Inner Shield Door (at Polisher) Open/Up - air cyl. 2
	stModuleData.di_ShieldDoorIsClosed1Sens := stModuleIO.di_ShieldDoorIsClosed1Sens := BOOL_TO_BYTE(WR.di_ShieldDoorIsClosed1Sens);	// Inner Shield Door (at Polisher) Closed/Down - air cyl. 1
	stModuleData.di_ShieldDoorIsClosed2Sens := stModuleIO.di_ShieldDoorIsClosed2Sens := BOOL_TO_BYTE(WR.di_ShieldDoorIsClosed2Sens);	// Inner Shield Door (at Polisher) Closed/Down - air cyl. 2

	pstSRDModuleIO^.diWetDoorIsAtOpen := WR.di_SRD_WetDoorIsOpenedSens;
	stModuleData.di_SRD_WetDoorIsOpenedSens := BOOL_TO_BYTE(pstSRDModuleIO^.diWetDoorIsAtOpen);	// Wet Door (at Cleaner/SRD) Open/Up
	
	pstSRDModuleIO^.diWetDoorIsAtClosed  := (WR.di_SRD_WetDoorIsClosedSens);
	stModuleData.di_SRD_WetDoorIsClosedSens  := BOOL_TO_BYTE(pstSRDModuleIO^.diWetDoorIsAtClosed);	// Wet Door (at Cleaner/SRD) Closed/Down	
	
	pstSRDModuleIO^.diJetArmSprayAtRetract := (WR.di_SRD_SprayArmIsRetractedSens);
	stModuleData.di_SRD_SprayArmIsRetractedSens := BOOL_TO_BYTE(pstSRDModuleIO^.diJetArmSprayAtRetract);	// Spray arm retracted (clear for Pick/Place, Spindle Up/Down)
	
	pstSRDModuleIO^.diSpindleLock := (WR.di_SRD_SpindleIsLockedSens);
	stModuleData.di_SRD_SpindleIsLockedSens := BOOL_TO_BYTE(pstSRDModuleIO^.diSpindleLock);
	
	pstSRDModuleIO^.diSpindleIsAtUp := (WR.di_SRD_SpindleIsUpSens);
	stModuleData.di_SRD_SpindleIsUpSens := BOOL_TO_BYTE(pstSRDModuleIO^.diSpindleIsAtUp);
	
	pstSRDModuleIO^.diSpindleIsAtDown := (WR.di_SRD_SpindleIsDownSens);
	stModuleData.di_SRD_SpindleIsDownSens := BOOL_TO_BYTE(pstSRDModuleIO^.diSpindleIsAtDown);
	
	pstSRDModuleIO^.diSpinFingerIsAtOpen := (WR.di_SRD_FingersAreOpenedSens);
	stModuleData.di_SRD_FingersAreOpenedSens := BOOL_TO_BYTE(pstSRDModuleIO^.diSpinFingerIsAtOpen);	// Spin Chuck Fingers are Opened
	
	pstSRDModuleIO^.diSpinFingerHasWafer := (WR.di_SRD_FingersAreClosedWaferSens);	
	stModuleData.di_SRD_FingersAreClosedWaferSens := BOOL_TO_BYTE(pstSRDModuleIO^.diSpinFingerHasWafer);	// Spin Chuck Fingers are Closed on wafer (off with sol. on = no wafer)
	
ELSE
	//WR.di_HomeSens := BYTE_TO_BOOL(stModuleIO.di_HomeSens) ;
	stModuleData.di_HomeSens 						:= BOOL_TO_BYTE(WR.di_HomeSens);			// Home Sensor
	//WR.di_EOTFWDSens := BYTE_TO_BOOL(stModuleIO.di_EOTFWDSens );
	stModuleData.di_EOTFWDSens 						:= BOOL_TO_BYTE(WR.di_EOTFWDSens) ;		// Forward End of Travel sensor
	//WR.di_EOTREVSens := BYTE_TO_BOOL(stModuleIO.di_EOTREVSens) ;
	stModuleData.di_EOTREVSens 						:= BOOL_TO_BYTE(WR.di_EOTREVSens) ;	// Reverse End of Travel sensor
	//WR.di_WaferAtFlipSens := BYTE_TO_BOOL(stModuleIO.di_WaferAtFlipSens) ;
	stModuleData.di_WaferAtFlipSens 				:= BOOL_TO_BYTE(WR.di_WaferAtFlipSens) ;		// Wafer present sensor at Flip position
	//WR.di_EStopOK := BYTE_TO_BOOL(stModuleIO.di_EStopOK );
	stModuleData.di_EStopOK 						:= BOOL_TO_BYTE(WR.di_EStopOK) ;		// E-Stop circuit is reset/OK
	
	//IF NOT WR_Pneu.SimLifter THEN
	//	WR.di_LifterIsUpSens := BYTE_TO_BOOL(stModuleIO.di_LifterIsUpSens );
	//	WR.di_LifterIsDownSens := BYTE_TO_BOOL(stModuleIO.di_LifterIsDownSens) ;
	//END_IF
	stModuleData.di_LifterIsUpSens 					:= BOOL_TO_BYTE(WR.di_LifterIsUpSens) ;
	stModuleData.di_LifterIsDownSens 				:= BOOL_TO_BYTE(WR.di_LifterIsDownSens) ;
	
	//IF NOT WR_Pneu.SimGripper THEN
	//	WR.di_GripperIsOpenedSens := BYTE_TO_BOOL(stModuleIO.di_GripperIsOpenedSens) ;
	//	WR.di_GripperIsClosedSens := BYTE_TO_BOOL(stModuleIO.di_GripperIsClosedSens) ;
	//END_IF
	stModuleData.di_GripperIsOpenedSens 			:= BOOL_TO_BYTE(WR.di_GripperIsOpenedSens) ;	// Gripper is Opened
	stModuleData.di_GripperIsClosedSens 			:= BOOL_TO_BYTE(WR.di_GripperIsClosedSens) ; // Gripper is Closed
	
	//IF NOT WR_Pneu.SimFlipper THEN
	//	WR.di_Flipper_At0_Sens := BYTE_TO_BOOL(stModuleIO.di_Flipper_At0_Sens) ;
	//	WR.di_Flipper_At90_Sens := BYTE_TO_BOOL(stModuleIO.di_Flipper_At90_Sens) ;
	//	WR.di_Flipper_At180_Sens:= BYTE_TO_BOOL(stModuleIO.di_Flipper_At180_Sens) ;
	//END_IF
	stModuleData.di_Flipper_At0_Sens 				:= BOOL_TO_BYTE(WR.di_Flipper_At0_Sens) ;	// Rotary Actuator at 0 degree position (horizontal for Polisher station)
	stModuleData.di_Flipper_At90_Sens 				:= BOOL_TO_BYTE(WR.di_Flipper_At90_Sens) ;	// Rotary Actuator at 90 degree position (vertical)
	stModuleData.di_Flipper_At180_Sens 				:= BOOL_TO_BYTE(WR.di_Flipper_At180_Sens);// Rotary Actuator at 180 degree position (horizontal for Cleaner/SRD station)
	
	//IF NOT WR_Pneu.SimShieldDoor THEN
	//	WR.di_ShieldDoorIsOpened1Sens := BYTE_TO_BOOL(stModuleIO.di_ShieldDoorIsOpened1Sens );
	//	WR.di_ShieldDoorIsOpened2Sens := BYTE_TO_BOOL(stModuleIO.di_ShieldDoorIsOpened2Sens );	
	//	WR.di_ShieldDoorIsClosed1Sens := BYTE_TO_BOOL(stModuleIO.di_ShieldDoorIsClosed1Sens) ;	
	//	WR.di_ShieldDoorIsClosed2Sens := BYTE_TO_BOOL(stModuleIO.di_ShieldDoorIsClosed2Sens) ;
	//END_IF
	stModuleData.di_ShieldDoorIsOpened1Sens 		:= BOOL_TO_BYTE(WR.di_ShieldDoorIsOpened1Sens);	// Inner Shield Door (at Polisher) Open/Up - air cyl. 1
	stModuleData.di_ShieldDoorIsOpened2Sens 		:= BOOL_TO_BYTE(WR.di_ShieldDoorIsOpened2Sens);	// Inner Shield Door (at Polisher) Open/Up - air cyl. 2
	stModuleData.di_ShieldDoorIsClosed1Sens 		:= BOOL_TO_BYTE(WR.di_ShieldDoorIsClosed1Sens);	// Inner Shield Door (at Polisher) Closed/Down - air cyl. 1
	stModuleData.di_ShieldDoorIsClosed2Sens 		:= BOOL_TO_BYTE(WR.di_ShieldDoorIsClosed2Sens);	// Inner Shield Door (at Polisher) Closed/Down - air cyl. 2
	
	IF NOT WR_Pneu.SimSRD_WetDoor THEN
		WR.di_SRD_WetDoorIsOpenedSens := pstSRDModuleIO^.diWetDoorIsAtOpen ;
		WR.di_SRD_WetDoorIsClosedSens := pstSRDModuleIO^.diWetDoorIsAtClosed  ;
	END_IF
	stModuleData.di_SRD_WetDoorIsOpenedSens 		:= BOOL_TO_BYTE(WR.di_SRD_WetDoorIsOpenedSens);	// Wet Door (at Cleaner/SRD) Open/Up
	stModuleData.di_SRD_WetDoorIsClosedSens  		:= BOOL_TO_BYTE(WR.di_SRD_WetDoorIsClosedSens);	// Wet Door (at Cleaner/SRD) Closed/Down
	
	WR.di_SRD_SprayArmIsRetractedSens := pstSRDModuleIO^.diJetArmSprayAtRetract ;
	stModuleData.di_SRD_SprayArmIsRetractedSens 	:= BOOL_TO_BYTE(WR.di_SRD_SprayArmIsRetractedSens);	// Spray arm retracted (clear for Pick/Place, Spindle Up/Down)
	
	(* Removed 4/24/19. Simulating lock status for WR 
	IF NOT WR_Pneu.SimSRD_SpindleLock THEN
		WR.di_SRD_SpindleIsLockedSens := pstSRDModuleIO^.diSpindleLock ;
	END_IF*)
	stModuleData.di_SRD_SpindleIsLockedSens 		:= BOOL_TO_BYTE(WR.di_SRD_SpindleIsLockedSens);
	
	
	IF NOT WR_Pneu.SimSRD_SpindleLift THEN
		WR.di_SRD_SpindleIsUpSens := pstSRDModuleIO^.diSpindleIsAtUp ;
		WR.di_SRD_SpindleIsDownSens := pstSRDModuleIO^.diSpindleIsAtDown ;
	END_IF
	stModuleData.di_SRD_SpindleIsUpSens 			:= BOOL_TO_BYTE(WR.di_SRD_SpindleIsUpSens);
	stModuleData.di_SRD_SpindleIsDownSens 			:= BOOL_TO_BYTE(WR.di_SRD_SpindleIsDownSens);
	
	IF NOT WR_Pneu.SimSRD_SpindleFingers THEN
		WR.di_SRD_FingersAreOpenedSens := pstSRDModuleIO^.diSpinFingerIsAtOpen ;
		WR.di_SRD_FingersAreClosedWaferSens := pstSRDModuleIO^.diSpinFingerHasWafer ;
	END_IF
	stModuleData.di_SRD_FingersAreOpenedSens 		:= BOOL_TO_BYTE(WR.di_SRD_FingersAreOpenedSens);	// Spin Chuck Fingers are Opened
	stModuleData.di_SRD_FingersAreClosedWaferSens 	:= BOOL_TO_BYTE(WR.di_SRD_FingersAreClosedWaferSens);	// Spin Chuck Fingers are Closed on wafer (off with sol. on = no wafer)
END_IF
	]]></ST>
      </Implementation>
    </Action>
    <Method Name="UpdatePersistentVariables" Id="{a3dcc3c8-68c8-4c20-bcb1-a564ce9f10f9}">
      <Declaration><![CDATA[METHOD UpdatePersistentVariables
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[;]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="FB_ModuleWetRobot">
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.Abort">
      <LineId Id="7" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.CheckPersistentVariables">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.ExecuteOnModule">
      <LineId Id="8" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.Initialize">
      <LineId Id="37" Count="19" />
      <LineId Id="17" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.Service">
      <LineId Id="154" Count="23" />
      <LineId Id="221" Count="0" />
      <LineId Id="178" Count="2" />
      <LineId Id="222" Count="0" />
      <LineId Id="181" Count="36" />
      <LineId Id="4" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.UpdateConfigVariables">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.UpdateOwensIO">
      <LineId Id="2" Count="11" />
      <LineId Id="20" Count="60" />
      <LineId Id="132" Count="0" />
      <LineId Id="138" Count="0" />
      <LineId Id="81" Count="0" />
      <LineId Id="83" Count="0" />
      <LineId Id="134" Count="0" />
      <LineId Id="174" Count="1" />
      <LineId Id="140" Count="0" />
      <LineId Id="139" Count="0" />
      <LineId Id="85" Count="0" />
      <LineId Id="87" Count="0" />
      <LineId Id="135" Count="0" />
      <LineId Id="172" Count="1" />
      <LineId Id="141" Count="1" />
      <LineId Id="89" Count="0" />
      <LineId Id="91" Count="0" />
      <LineId Id="93" Count="0" />
      <LineId Id="143" Count="0" />
      <LineId Id="169" Count="2" />
      <LineId Id="137" Count="0" />
      <LineId Id="144" Count="0" />
      <LineId Id="95" Count="0" />
      <LineId Id="97" Count="0" />
      <LineId Id="99" Count="0" />
      <LineId Id="101" Count="0" />
      <LineId Id="103" Count="0" />
      <LineId Id="165" Count="3" />
      <LineId Id="145" Count="1" />
      <LineId Id="104" Count="0" />
      <LineId Id="176" Count="0" />
      <LineId Id="106" Count="0" />
      <LineId Id="109" Count="0" />
      <LineId Id="163" Count="0" />
      <LineId Id="149" Count="0" />
      <LineId Id="110" Count="0" />
      <LineId Id="162" Count="0" />
      <LineId Id="151" Count="0" />
      <LineId Id="178" Count="0" />
      <LineId Id="152" Count="0" />
      <LineId Id="113" Count="0" />
      <LineId Id="115" Count="0" />
      <LineId Id="161" Count="0" />
      <LineId Id="153" Count="0" />
      <LineId Id="179" Count="0" />
      <LineId Id="154" Count="0" />
      <LineId Id="116" Count="0" />
      <LineId Id="177" Count="0" />
      <LineId Id="118" Count="0" />
      <LineId Id="121" Count="0" />
      <LineId Id="159" Count="0" />
      <LineId Id="157" Count="0" />
      <LineId Id="128" Count="0" />
      <LineId Id="122" Count="0" />
      <LineId Id="158" Count="0" />
      <LineId Id="129" Count="0" />
      <LineId Id="123" Count="0" />
      <LineId Id="126" Count="1" />
      <LineId Id="1" Count="0" />
    </LineIds>
    <LineIds Name="FB_ModuleWetRobot.UpdatePersistentVariables">
      <LineId Id="5" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>